-----------------------------------------------------------------------
-----------------------------------------------------------------------
-------------------Baza je napravljena na account-u:-------------------
-------------------UN:S14536-----------PW:beban93----------------------
-----------------------------------------------------------------------
-----------------------------------------------------------------------

CREATE TABLE IGRAC(
Starost         number,
Pol         	varchar(1),
Prezime         varchar(15),
Ime        		varchar(15),
Nadimak         varchar(15) not null unique,
ID_IGRAC   		number,
Lozinka         varchar(15) not null,
ID_LIKA        	number, 
ID_ALIJANSE    	number
);

ALTER TABLE IGRAC ADD PRIMARY KEY(ID_IGRAC);

ALTER TABLE IGRAC ADD CONSTRAINT VEZA_KONTROLISE FOREIGN KEY(ID_LIKA) REFERENCES LIK(ID_LIK);

ALTER TABLE IGRAC ADD CONSTRAINT VEZA_PRIPADA FOREIGN KEY(ID_ALIJANSE) REFERENCES ALIJANSA(ID_ALIJANSA);

CREATE SEQUENCE  IGRAC_ID_SEQ  MINVALUE 1 MAXVALUE 1000000 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;
  
CREATE OR REPLACE TRIGGER ON_INSERT_IGRAC
BEFORE INSERT ON IGRAC
REFERENCING NEW AS noviIgrac
FOR EACH ROW 
BEGIN
  SELECT IGRAC_ID_SEQ.NEXTVAl INTO :noviIgrac.ID_IGRAC FROM DUAL;
END;

CREATE TABLE ALIJANSA(
ID_ALIJANSA		number,
Min         	number,
Max         	number,
Naziv         	varchar(15) not null,
Bonus_iskustvo 	number,
Bonus_zdravlje	number
);

ALTER TABLE ALIJANSA ADD PRIMARY KEY(ID_ALIJANSA);

CREATE SEQUENCE  ALIJANSA_ID_SEQ  MINVALUE 1 MAXVALUE 1000000 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;
  
CREATE OR REPLACE TRIGGER ON_INSERT_ALIJANSA
BEFORE INSERT ON ALIJANSA
REFERENCING NEW AS novaAlijansa
FOR EACH ROW 
BEGIN
  SELECT ALIJANSA_ID_SEQ.NEXTVAl INTO :novaAlijansa.ID_ALIJANSA FROM DUAL;
END;


CREATE TABLE LIK(
Iskustvo        				number,
Zlato         					number,
Zamor         					number,
Zdravlje        				number,
Ime        						varchar(15),
Segrtov_Bonus_u_Skrivanju      	number,
ID_LIK         					number,
ID_GOSPODARA      				number,
Rasa							varchar(15),
Energija						number,
Specijalizacija_oruzja 			varchar(15),
Bonus_u_skrivanju 				number
);

ALTER TABLE LIK ADD PRIMARY KEY(ID_LIK);

ALTER TABLE LIK ADD CONSTRAINT VEZA_JE_SEGRT FOREIGN KEY(ID_GOSPODARA) REFERENCES LIK(ID_LIK);

ALTER TABLE LIK ADD CONSTRAINT TIP_RASE CHECK (Rasa in ('Demon','Vilenjak','Ork','Covek','Patuljak'));

ALTER TABLE LIK ADD CONSTRAINT "ORUZJE_ILI_ENERGIJA" CHECK 
((Rasa in ('Demon','Vilenjak') AND Energija is not null and Specijalizacija_oruzja is null and Bonus_u_skrivanju is null)
 OR (Rasa in ('Ork','Patuljak')and Energija is null and Bonus_u_skrivanju is null and Specijalizacija_oruzja is not null)
 OR (Rasa in ('Covek')and Energija is null and Specijalizacija_oruzja is null and Bonus_u_skrivanju is not null));

--PREPRAVITI--ALTER TABLE LIK ADD CONSTRAINT "SEGRT_I_GOSPODAR" CHECK ((Segrtov_Bonus_u_Skrivanju is not null AND ID_GOSPODARA is not null AND ime is not null) OR 
--(Segrtov_Bonus_u_Skrivanju is null AND ID_GOSPODARA is null AND ime is null)); --samo segrti imaju ime

CREATE SEQUENCE  LIK_ID_SEQ  MINVALUE 1 MAXVALUE 1000000 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;
  
CREATE OR REPLACE TRIGGER ON_INSERT_LIK
BEFORE INSERT ON LIK
REFERENCING NEW AS noviLik
FOR EACH ROW 
BEGIN
  SELECT LIK_ID_SEQ.NEXTVAl INTO :noviLik.ID_LIK FROM DUAL;
END;


CREATE TABLE SESIJA(
ID_SESIJA 		number,
Vreme_pocetka   date not null,
Vreme_kraja 	date,
Zlato         	number,
Iskustvo        number,
ID_IGRACA  		number --not null
);

ALTER TABLE SESIJA ADD PRIMARY KEY(ID_SESIJA);

ALTER TABLE SESIJA ADD CONSTRAINT VEZA_POVEZAN FOREIGN KEY(ID_IGRACA) REFERENCES IGRAC(ID_IGRAC);

CREATE SEQUENCE  SESIJA_ID_SEQ  MINVALUE 1 MAXVALUE 1000000 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;
  
CREATE OR REPLACE TRIGGER ON_INSERT_SESIJA
BEFORE INSERT ON SESIJA
REFERENCING NEW AS novaSesija
FOR EACH ROW 
BEGIN
  SELECT SESIJA_ID_SEQ.NEXTVAl INTO :novaSesija.ID_SESIJA FROM DUAL;
END;


CREATE TABLE ZADATAK(
ID_ZADATAK      number,
Bonus     	    number,
Vreme_pocetka   date not null,
Vreme_kraja 	date,
Brojnost		varchar(15)
);

ALTER TABLE ZADATAK ADD PRIMARY KEY(ID_ZADATAK);

ALTER TABLE ZADATAK ADD CONSTRAINT BROJNOST_ZADATKA check (Brojnost in ('Solo','Grupni'));

CREATE SEQUENCE  ZADATAK_ID_SEQ  MINVALUE 1 MAXVALUE 1000000 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;
  
CREATE OR REPLACE TRIGGER ON_INSERT_ZADATAK
BEFORE INSERT ON ZADATAK
REFERENCING NEW AS noviZadatak
FOR EACH ROW 
BEGIN
  SELECT ZADATAK_ID_SEQ.NEXTVAl INTO :noviZadatak.ID_ZADATAK FROM DUAL;
END;


CREATE TABLE PREVOZNO_SREDSTVO( --svako ima svog mounta
Brzina         			number,
ID_PREVOZNO_SREDSTVO	number,
Naziv         			varchar(15) not null,
Tip_vozila				varchar(15),
ID_IGRACA				number
);

ALTER TABLE PREVOZNO_SREDSTVO ADD PRIMARY KEY(ID_PREVOZNO_SREDSTVO);

ALTER TABLE PREVOZNO_SREDSTVO ADD CONSTRAINT VEZA_VOZI FOREIGN KEY(ID_IGRACA) REFERENCES IGRAC(ID_IGRAC);

ALTER TABLE PREVOZNO_SREDSTVO ADD CONSTRAINT TIP_PREVOZNOG_SREDSTVA check (Tip_vozila in ('Zmaj','Mehanicki','Zivotinja'));

CREATE SEQUENCE  PREVOZNO_SREDSTVO_ID_SEQ  MINVALUE 1 MAXVALUE 1000000 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;
  
CREATE OR REPLACE TRIGGER ON_INSERT_PREVOZNO_SREDSTVO
BEFORE INSERT ON PREVOZNO_SREDSTVO
REFERENCING NEW AS novoPrevoznoSredstvo
FOR EACH ROW 
BEGIN
  SELECT PREVOZNO_SREDSTVO_ID_SEQ.NEXTVAl INTO :novoPrevoznoSredstvo.ID_PREVOZNO_SREDSTVO FROM DUAL;
END;


CREATE TABLE ITEM(
ID_ITEM			number,
Tip		 		varchar(15) not null,
--PBonus			number(1),
Iskustvo		number,
--PKljucni		number(1),
Ime				varchar(15),
Opis			varchar(15)

);

ALTER TABLE ITEM ADD PRIMARY KEY(ID_ITEM);

--ALTER TABLE ITEM ADD CONSTRAINT BONUS_FLAG check (PBonus in (0,1));

--ALTER TABLE ITEM ADD CONSTRAINT KLJUCNI_FLAG check (PKljucni in (0,1));

--PREPRAVITI--ALTER TABLE ITEM ADD CONSTRAINT BONUS_ILI_ISKUSTVO check ((PBonus = 1 and Iskustvo is not null and PKljucni = 0 and Ime is null and Opis is null)
-- OR (PKljucni = 1 and Ime is not null and opis is not null and PBonus = 0 and Iskustvo is null));

ALTER TABLE ITEM ADD CONSTRAINT TIP_ITEMA check (Tip in ('OruzjeKljucni','PredmetKljucni','OruzjeBonus','PredmetBonus'));

CREATE SEQUENCE  ITEM_ID_SEQ  MINVALUE 1 MAXVALUE 1000000 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;
  
CREATE OR REPLACE TRIGGER ON_INSERT_ITEM
BEFORE INSERT ON ITEM
REFERENCING NEW AS noviItem
FOR EACH ROW 
BEGIN
  SELECT ITEM_ID_SEQ.NEXTVAl INTO :noviItem.ID_ITEM FROM DUAL;
END;


CREATE TABLE RASA(
ID_RASA			number,
RASA         	varchar(15),
ID_ITEMA  		number
);

ALTER TABLE RASA ADD PRIMARY KEY(ID_RASA);

ALTER TABLE RASA ADD CONSTRAINT STRANI_KLJUC_ITEMA_U_RASI FOREIGN KEY(ID_ITEMA) REFERENCES ITEM(ID_ITEM);

ALTER TABLE RASA ADD CONSTRAINT TIP_RASE_ITEM CHECK (RASA in ('Demon','Vilenjak','Ork','Covek','Patuljak'));

CREATE SEQUENCE  RASA_ID_SEQ  MINVALUE 1 MAXVALUE 1000000 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE ;
  
CREATE OR REPLACE TRIGGER ON_INSERT_RASA
BEFORE INSERT ON RASA
REFERENCING NEW AS novaRasa
FOR EACH ROW 
BEGIN
  SELECT RASA_ID_SEQ.NEXTVAl INTO :novaRasa.ID_RASA FROM DUAL;
END;


CREATE TABLE POSEDUJE(
ID_IGRACA		number,
ID_ITEMA	   	number
);

ALTER TABLE POSEDUJE ADD PRIMARY KEY(ID_IGRACA, ID_ITEMA);

ALTER TABLE POSEDUJE ADD CONSTRAINT STEANI_KLJUC_IGRACA FOREIGN KEY(ID_IGRACA) REFERENCES IGRAC(ID_IGRAC);

ALTER TABLE POSEDUJE ADD CONSTRAINT STRANI_KLJUC_ITEMA FOREIGN KEY(ID_ITEMA) REFERENCES ITEM(ID_ITEM);


CREATE TABLE SAVEZ(
ID_ALIJANSE1	number,
ID_ALIJANSE2   	number
);

ALTER TABLE SAVEZ ADD PRIMARY KEY(ID_ALIJANSE1, ID_ALIJANSE2);

ALTER TABLE SAVEZ ADD CONSTRAINT STRANI_KLJUC_PRVE_ALIJANSE FOREIGN KEY(ID_ALIJANSE1) REFERENCES ALIJANSA(ID_ALIJANSA);

ALTER TABLE SAVEZ ADD CONSTRAINT STRANI_KLJUC_DRUGE_ALIJANSE FOREIGN KEY(ID_ALIJANSE2) REFERENCES ALIJANSA(ID_ALIJANSA);



CREATE TABLE SOLO_ZADATAK(
ID_IGRACA      		number,
ID_ZADATKA			number
);

ALTER TABLE SOLO_ZADATAK ADD PRIMARY KEY(ID_IGRACA, ID_ZADATKA);

ALTER TABLE SOLO_ZADATAK ADD CONSTRAINT STRANI_KLJUC_IGRACA FOREIGN KEY(ID_IGRACA) REFERENCES IGRAC(ID_IGRAC);

ALTER TABLE SOLO_ZADATAK ADD CONSTRAINT STRANI_KLJUC_ZADATKA FOREIGN KEY(ID_ZADATKA) REFERENCES ZADATAK(ID_ZADATAK);


CREATE TABLE GRUPNI_ZADATAK(
ID_ALIJANSE    		number,
ID_ZADATKA			number
);

ALTER TABLE GRUPNI_ZADATAK ADD PRIMARY KEY(ID_ALIJANSE, ID_ZADATKA);

ALTER TABLE GRUPNI_ZADATAK ADD CONSTRAINT STRANI_KLJUC_ALIJANSE FOREIGN KEY(ID_ALIJANSE) REFERENCES ALIJANSA(ID_ALIJANSA);

ALTER TABLE GRUPNI_ZADATAK ADD CONSTRAINT STRANI_KLJUC_ZADATKA_GRUPNI FOREIGN KEY(ID_ZADATKA) REFERENCES ZADATAK(ID_ZADATAK);



-----------------------------------------------------------------
--------------------------SAMPLE PODACI--------------------------
-----------------------------------------------------------------

INSERT INTO "S14289"."IGRAC" (STAROST, POL, PREZIME, IME, NADIMAK, ID_IGRAC, LOZINKA) VALUES ('21', 'M', 'Mihajlovic', 'Stevan', 'Stevica', null, '123')
INSERT INTO "S14289"."IGRAC" (STAROST, POL, PREZIME, IME, NADIMAK, ID_IGRAC, LOZINKA) VALUES ('21', 'M', 'Djordjevic', 'Dusan', 'Dusanbe', null, '456')
INSERT INTO "S14289"."IGRAC" (STAROST, POL, PREZIME, IME, NADIMAK, ID_IGRAC, LOZINKA) VALUES ('21', 'M', 'Stojiljkovic', 'Stefan', 'Beban', null, '789')
INSERT INTO "S14289"."IGRAC" (STAROST, POL, PREZIME, IME, NADIMAK, ID_IGRAC, LOZINKA) VALUES ('21', 'M', 'Ravnihar', 'Aleksa', 'Ramax', null, '0')

INSERT INTO "S14289"."LIK" (ISKUSTVO, ZLATO, ZAMOR, ZDRAVLJE, IME, ID_LIK, RASA, BONUS_U_SKRIVANJU) VALUES ('9999', '999999', '10', '100', 'Ashbringer', null, 'Covek', '10')
INSERT INTO "S14289"."LIK" (ISKUSTVO, ZLATO, ZAMOR, ZDRAVLJE, IME, ID_LIK, RASA, ENERGIJA) VALUES ('999', '1000', '1', '99', 'Cherrykiss', null, 'Vilenjak', '115')
INSERT INTO "S14289"."LIK" (ISKUSTVO, ZLATO, ZAMOR, ZDRAVLJE, IME, ID_LIK, RASA, ENERGIJA) VALUES ('500', '0', '0', '99', 'Illidan', null, 'Demon', '1000')
INSERT INTO "S14289"."LIK" (ISKUSTVO, ZLATO, ZAMOR, ZDRAVLJE, IME, RASA, BONUS_U_SKRIVANJU) VALUES ('15000', '2000', '20', '101', 'Tolens', 'Covek', '15')


INSERT INTO "S14289"."ALIJANSA" (ID_ALIJANSA, MIN, MAX, NAZIV, BONUS) VALUES (null, '1', '10', 'LeeSeeCe', '220')
INSERT INTO "S14289"."ALIJANSA" (ID_ALIJANSA, MIN, MAX, NAZIV, BONUS) VALUES (null, '1', '5', 'Demonhunters', '100')

INSERT INTO "S14289"."SAVEZ" (ID_ALIJANSE1, ID_ALIJANSE2) VALUES ('1', '2')

INSERT INTO "S14289"."PREVOZNO_SREDSTVO" (BRZINA, ID_PREVOZNO_SREDSTVO, NAZIV, TIP_VOZILA, ID_IGRACA) VALUES ('310', null, 'Invincible', 'Zmaj', '2')
INSERT INTO "S14289"."PREVOZNO_SREDSTVO" (BRZINA, ID_PREVOZNO_SREDSTVO, NAZIV, TIP_VOZILA, ID_IGRACA) VALUES ('100', null, 'Spectral tiger', 'Zivotinja', '2')
INSERT INTO "S14289"."PREVOZNO_SREDSTVO" (BRZINA, ID_PREVOZNO_SREDSTVO, NAZIV, TIP_VOZILA, ID_IGRACA) VALUES ('100', null, 'Sabretooth', 'Zivotinja', '3')
INSERT INTO "S14289"."PREVOZNO_SREDSTVO" (BRZINA, ID_PREVOZNO_SREDSTVO, NAZIV, TIP_VOZILA, ID_IGRACA) VALUES ('100', null, 'Mimiron''s head', 'Mehanicki', '4')
INSERT INTO "S14289"."PREVOZNO_SREDSTVO" (BRZINA, ID_PREVOZNO_SREDSTVO, NAZIV, TIP_VOZILA, ID_IGRACA) VALUES ('100', null, 'Deathcharger', 'Zivotinja', '5')
INSERT INTO "S14289"."PREVOZNO_SREDSTVO" (BRZINA, ID_PREVOZNO_SREDSTVO, NAZIV, TIP_VOZILA, ID_IGRACA) VALUES ('310', null, 'X6', 'Mehanicki', '5')

INSERT INTO "S14289"."ITEM" (ID_ITEM, TIP, PBONUS, ISKUSTVO) VALUES (null, 'ORUZJE', '1', '100')
INSERT INTO "S14289"."ITEM" (ID_ITEM, TIP, PBONUS, ISKUSTVO) VALUES (null, 'PREDMET', '1', '299')
INSERT INTO "S14289"."ITEM" (ID_ITEM, TIP, PKLJUCNI, IME, OPIS) VALUES (null, 'ORUZJE', '1', 'Frostmourne', 'Arthasov mac')
INSERT INTO "S14289"."ITEM" (ID_ITEM, TIP, PKLJUCNI, IME, OPIS) VALUES (null, 'ORUZJE', '1', 'Atiesh', 'Staff')
INSERT INTO "S14289"."ITEM" (ID_ITEM, TIP, PKLJUCNI, IME, OPIS) VALUES (null, 'PREDMET', '1', 'Eye of Kilrog', 'Artefakt')

INSERT INTO "S14289"."POSEDUJE" (ID_IGRACA, ID_ITEMA) VALUES ('2', '1')
INSERT INTO "S14289"."POSEDUJE" (ID_IGRACA, ID_ITEMA) VALUES ('2', '2')
INSERT INTO "S14289"."POSEDUJE" (ID_IGRACA, ID_ITEMA) VALUES ('3', '4')
INSERT INTO "S14289"."POSEDUJE" (ID_IGRACA, ID_ITEMA) VALUES ('3', '5')
INSERT INTO "S14289"."POSEDUJE" (ID_IGRACA, ID_ITEMA) VALUES ('5', '1')
INSERT INTO "S14289"."POSEDUJE" (ID_IGRACA, ID_ITEMA) VALUES ('5', '2')
INSERT INTO "S14289"."POSEDUJE" (ID_IGRACA, ID_ITEMA) VALUES ('5', '3')
INSERT INTO "S14289"."POSEDUJE" (ID_IGRACA, ID_ITEMA) VALUES ('5', '4')
INSERT INTO "S14289"."POSEDUJE" (ID_IGRACA, ID_ITEMA) VALUES ('5', '5')
INSERT INTO "S14289"."POSEDUJE" (ID_IGRACA, ID_ITEMA) VALUES ('4', '5')

INSERT INTO "S14289"."RASA" (RASA, ID_ITEMA) VALUES ('Covek', '1')
INSERT INTO "S14289"."RASA" (RASA, ID_ITEMA) VALUES ('Patuljak', '1')
INSERT INTO "S14289"."RASA" (RASA, ID_ITEMA) VALUES ('Vilenjak', '1')
INSERT INTO "S14289"."RASA" (RASA, ID_ITEMA) VALUES ('Demon', '1')
INSERT INTO "S14289"."RASA" (RASA, ID_ITEMA) VALUES ('Ork', '1')
INSERT INTO "S14289"."RASA" (RASA, ID_ITEMA) VALUES ('Ork', '2')
INSERT INTO "S14289"."RASA" (RASA, ID_ITEMA) VALUES ('Demon', '2')
INSERT INTO "S14289"."RASA" (RASA, ID_ITEMA) VALUES ('Vilenjak', '2')


INSERT INTO "S14289"."ZADATAK" (ID_ZADATAK, BONUS, VREME_POCETKA, VREME_KRAJA, BROJNOST) VALUES ('1', '200', TO_DATE('2015-04-01 18:31:57', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2015-04-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'Solo')
INSERT INTO "S14289"."ZADATAK" (ID_ZADATAK, BONUS, VREME_POCETKA, BROJNOST) VALUES ('2', '500', TO_DATE('2015-04-11 18:32:23', 'YYYY-MM-DD HH24:MI:SS'), 'Grupni')

INSERT INTO "S14289"."SOLO_ZADATAK" (ID_IGRACA, ID_ZADATKA) VALUES ('3', '2')

INSERT INTO "S14289"."GRUPNI_ZADATAK" (ID_ALIJANSE, ID_ZADATKA) VALUES ('1', '3')








