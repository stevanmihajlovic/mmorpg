﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class InsertItemForm : Form
    {
        public int iditemazaupdate=0;

        public Entiteti.Item i;

        public InsertItemForm()
        {
            InitializeComponent();
        }
        public InsertItemForm(int a)
        {
            //this.Name = "UpdateItemForm";
            InitializeComponent();
            iditemazaupdate = a;
            try
            {
                ISession s = DataLayer.GetSession();


                
                    i = s.Get<Item>(iditemazaupdate); //ZBOG PROXYA!

                    if (i.GetType().BaseType == typeof(BonusItem))  //moramo da ocistimo sve rase prvo
                    {
                        IList<Rasa> rase = (from r in s.Query<Entiteti.Rasa>()
                                            where (r.IdItema==i)
                                            select r).ToList<Rasa>();

                        foreach (Rasa r in rase)
                        {
                            s.Delete(r);
                        }
                    }
                    if (i.GetType() == typeof(PredmetKljucniItem))
                    {
                        radioButtonPredmet.Checked = true;
                        radioButtonOruzje.Checked = false;
                        radioButtonKljucni.Checked = true;
                        radioButtonBonus.Checked = false;
                    }
                    if (i.GetType() == typeof(PredmetBonusItem))
                    {
                        radioButtonPredmet.Checked = true;
                        radioButtonOruzje.Checked = false;
                        radioButtonKljucni.Checked = false;
                        radioButtonBonus.Checked = true;
                    }
                    if (i.GetType() == typeof(OruzjeKljucniItem))
                    {
                        radioButtonPredmet.Checked = false;
                        radioButtonOruzje.Checked = true;
                        radioButtonKljucni.Checked = true;
                        radioButtonBonus.Checked = false;
                    }
                    if (i.GetType() == typeof(OruzjeBonusItem))
                    {
                        radioButtonPredmet.Checked = false;
                        radioButtonOruzje.Checked = true;
                        radioButtonKljucni.Checked = false;
                        radioButtonBonus.Checked = true;
                    }
                    groupBox1.Visible = false;
                    label2.Visible = false;
                    label3.Visible = false;
                    groupBoxKljucni.Visible = radioButtonKljucni.Checked;
                    groupBoxBonus.Visible = radioButtonBonus.Checked;
                    radioButtonPredmet.Visible = false;
                    radioButtonKljucni.Visible = false;
                    radioButtonOruzje.Visible = false;
                    radioButtonBonus.Visible = false;
                    s.Flush();
                    s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void radioButtonKljucni_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxKljucni.Visible = radioButtonKljucni.Checked;
            groupBoxBonus.Visible = radioButtonBonus.Checked;
        }

        private void buttonUbaci_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                if (iditemazaupdate != 0)
                s.Lock(i, LockMode.None);
                
                int greska = 0;

                if (radioButtonPredmet.Checked)
                {
                    if (radioButtonKljucni.Checked)
                    {
                        if (iditemazaupdate == 0)
                            i = new Entiteti.PredmetKljucniItem();
                        ((Entiteti.PredmetKljucniItem)i).Ime = textBoxIme.Text;
                        ((Entiteti.PredmetKljucniItem)i).Opis = textBoxOpis.Text;
                    }
                    else
                    {
                        if (iditemazaupdate == 0)
                            i = new Entiteti.PredmetBonusItem();
                        ((Entiteti.PredmetBonusItem)i).Iskustvo = Convert.ToInt32(textBoxIskustvo.Text);
                        if (checkBoxCovek.Checked || checkBoxDemon.Checked || checkBoxOrk.Checked || checkBoxPatuljak.Checked || checkBoxVilenjak.Checked)
                        {
                            if (checkBoxVilenjak.Checked)
                            {
                                Entiteti.Rasa r = new Entiteti.Rasa();
                                r.IdItema = ((Entiteti.PredmetBonusItem)i);
                                r.TipRase = ("Vilenjak");
                                ((Entiteti.PredmetBonusItem)i).Rase.Add(r);
                                s.SaveOrUpdate(r);
                            }
                            if (checkBoxDemon.Checked)
                            {
                                Entiteti.Rasa r = new Entiteti.Rasa();
                                r.IdItema = ((Entiteti.PredmetBonusItem)i);
                                r.TipRase = ("Demon");
                                ((Entiteti.PredmetBonusItem)i).Rase.Add(r);
                                s.SaveOrUpdate(r);
                            }
                            if (checkBoxCovek.Checked)
                            {
                                Entiteti.Rasa r = new Entiteti.Rasa();
                                r.IdItema = ((Entiteti.PredmetBonusItem)i);
                                r.TipRase = ("Covek");
                                ((Entiteti.PredmetBonusItem)i).Rase.Add(r);
                                s.SaveOrUpdate(r);
                            }
                            if (checkBoxPatuljak.Checked)
                            {
                                Entiteti.Rasa r = new Entiteti.Rasa();
                                r.IdItema = ((Entiteti.PredmetBonusItem)i);
                                r.TipRase = ("Patuljak");
                                ((Entiteti.PredmetBonusItem)i).Rase.Add(r);
                                s.SaveOrUpdate(r);
                            }
                            if (checkBoxOrk.Checked)
                            {
                                Entiteti.Rasa r = new Entiteti.Rasa();
                                r.IdItema = ((Entiteti.PredmetBonusItem)i);
                                r.TipRase = ("Ork");
                                ((Entiteti.PredmetBonusItem)i).Rase.Add(r);
                                s.SaveOrUpdate(r);
                            }
                        }
                        else
                            greska=1;
                    }
                }
                else
                {
                    if (radioButtonKljucni.Checked)
                    {
                        if (iditemazaupdate == 0)
                            i = new Entiteti.OruzjeKljucniItem();
                        ((Entiteti.OruzjeKljucniItem)i).Ime = textBoxIme.Text;
                        ((Entiteti.OruzjeKljucniItem)i).Opis = textBoxOpis.Text;
                    }
                    else
                    {
                        if (iditemazaupdate == 0)
                            i = new Entiteti.OruzjeBonusItem();
                        ((Entiteti.OruzjeBonusItem)i).Iskustvo = Convert.ToInt32(textBoxIskustvo.Text);
                        if (checkBoxCovek.Checked || checkBoxDemon.Checked || checkBoxOrk.Checked || checkBoxPatuljak.Checked || checkBoxVilenjak.Checked)
                        {
                            if (checkBoxVilenjak.Checked)
                            {
                                Entiteti.Rasa r = new Entiteti.Rasa();
                                r.IdItema = ((Entiteti.OruzjeBonusItem)i);
                                r.TipRase = ("Vilenjak");
                                ((Entiteti.OruzjeBonusItem)i).Rase.Add(r);
                                s.SaveOrUpdate(r);
                            }
                            if (checkBoxDemon.Checked)
                            {
                                Entiteti.Rasa r = new Entiteti.Rasa();
                                r.IdItema = ((Entiteti.OruzjeBonusItem)i);
                                r.TipRase = ("Demon");
                                ((Entiteti.OruzjeBonusItem)i).Rase.Add(r);
                                s.SaveOrUpdate(r);
                            }
                            if (checkBoxCovek.Checked)
                            {
                                Entiteti.Rasa r = new Entiteti.Rasa();
                                r.IdItema = ((Entiteti.OruzjeBonusItem)i);
                                r.TipRase = ("Covek");
                                ((Entiteti.OruzjeBonusItem)i).Rase.Add(r);
                                s.SaveOrUpdate(r);
                            }
                            if (checkBoxPatuljak.Checked)
                            {
                                Entiteti.Rasa r = new Entiteti.Rasa();
                                r.IdItema = ((Entiteti.OruzjeBonusItem)i);
                                r.TipRase = ("Patuljak");
                                ((Entiteti.OruzjeBonusItem)i).Rase.Add(r);
                                s.SaveOrUpdate(r);
                            }
                            if (checkBoxOrk.Checked)
                            {
                                Entiteti.Rasa r = new Entiteti.Rasa();
                                r.IdItema = ((Entiteti.OruzjeBonusItem)i);
                                r.TipRase = ("Ork");
                                ((Entiteti.OruzjeBonusItem)i).Rase.Add(r);
                                s.SaveOrUpdate(r);
                            }
                        }
                        else
                            greska = 1;
                    }

                }
                if (greska == 1)
                    MessageBox.Show("Čekirajte bar jednu rasu");
                else
                {
                    
                    s.SaveOrUpdate(i);  //ovo radi zato sto se svaki put napravi novi item
                    //ranije smo imali da je on atribut klase i tamo ga inicirali i kad jednom dobije sesiju
                    //a mi ne stikliramo rasu, sesija se ne zatvori i sledeci put on pukne

                    s.Flush();
                    s.Close();
                    if (iditemazaupdate==0)
                        MessageBox.Show("Uspešno ste ubacili novi item u bazu");
                    else
                        MessageBox.Show("Uspešno ste updatovali item");
                    this.Close();
                }
            }
            catch (System.FormatException ec)
            {
                MessageBox.Show("Unesite za iskustvo broj od 0 do 1000");
                textBoxIskustvo.BackColor = Color.IndianRed;
            }
            catch (Exception ec)
            {             
                MessageBox.Show("Neobradjeni prekid, presli ste nas");
                this.Close();
            }
        }

        private void radioButtonBonus_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxKljucni.Visible = radioButtonKljucni.Checked;
            groupBoxBonus.Visible = radioButtonBonus.Checked;
        }
    }
}
