﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class DeleteItemForm : Form
    {
        public IList<Item> itemi;

        public DeleteItemForm()
        {
            InitializeComponent();

            try
            {
                ISession s = DataLayer.GetSession();

                itemi = (from a in s.Query<Entiteti.Item>()
                         select a).ToList<Item>();

                foreach (Item a in itemi)
                    listBox1.Items.Add(a.IdItem);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Selektujte item");
            else
            {
                try
                {
                    ISession s = DataLayer.GetSession();

                    Item selektovan = s.Load<Item>(itemi[listBox1.SelectedIndex].IdItem);
                    //s.Lock(selektovan, LockMode.None);
                    //Item selektovan = itemi[listBox1.SelectedIndex];
                    //s.Delete(Convert.ToInt32(listBox1.Items[listBox1.SelectedIndex].ToString()));
                    
                    foreach (Igrac i in selektovan.Igraci)
                    {
                        i.Itemi.Remove(selektovan);
                        s.SaveOrUpdate(i);
                    }

                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                    s.Delete(selektovan);
                   
                    s.Flush();
                    s.Close();

                    MessageBox.Show("Uspesno ste izbacili item iz baze");
                    this.Close();
                }
                catch (Exception ec)
                {
                    // if (ec.Message.Contains("already") || ec.InnerException.Message.Contains("insert"))
                    //   MessageBox.Show("Igrac vec poseduje item");
                    // else
                    MessageBox.Show(ec.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Selektujte item");
            else
            {
                InsertItemForm i = new InsertItemForm(itemi[listBox1.SelectedIndex].IdItem);
                i.ShowDialog();
            }
        }
    }
}
