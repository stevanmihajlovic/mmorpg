﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;


namespace MMORPG_ver1
{
    public partial class CreateAlianceForm : Form
    {
        Igrac logovaniIgrac;
        Entiteti.Alijansa alijansa;
        GameForm g;


        public CreateAlianceForm()
        {
            InitializeComponent();
        }
        public CreateAlianceForm(Igrac l,GameForm g1) 
        {
            g = g1;
            InitializeComponent();
            logovaniIgrac = l;
            alijansa = new Alijansa();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            textBoxMax.BackColor = Color.White;
            textBoxMin.BackColor = Color.White;
            
            if (textBoxNaziv.TextLength > 0)
            {
                try
                {
                    ISession s = DataLayer.GetSession();

                    alijansa.BonusIskustvo = 10;
                    alijansa.BonusZdravlje = 50;
                    alijansa.Naziv = textBoxNaziv.Text;
                   
                    alijansa.Igraci.Add(logovaniIgrac);     //sa obe strane uvek dodajemo 
                    logovaniIgrac.PripadaAlijansi = alijansa;

                    if (Convert.ToInt32(textBoxMin.Text) <= 1 || Convert.ToInt32(textBoxMax.Text) < Convert.ToInt32(textBoxMin.Text))
                    {
                        textBoxMax.BackColor = Color.IndianRed;
                        textBoxMin.BackColor = Color.IndianRed;
                        MessageBox.Show("Unesite pravilan broj minimalnih i maksimalnih igrača u alijansi");
                    }
                    else
                    {
                        alijansa.Max = Convert.ToInt32(textBoxMax.Text);
                        alijansa.Min = Convert.ToInt32(textBoxMin.Text);

                        s.SaveOrUpdate(alijansa);

                        s.Flush();
                        s.Close();

                        MessageBox.Show("Uspešno ste kreirali alijansu");
                        this.Close();
                        if (g.LogovanIgrac.PripadaAlijansi == null)
                        {
                            g.buttonPridruziteSeAlijansi.Visible = true;
                            g.KreairajteAlijansu.Visible = true;
                            g.buttonNapustiteAlijansu.Visible = false;
                        }
                        else
                        {
                            g.buttonPridruziteSeAlijansi.Visible = false;
                            g.KreairajteAlijansu.Visible = false;
                            g.buttonNapustiteAlijansu.Visible = true;
                        }
                    }
                    
                }
                catch (System.FormatException ec)
                {
                    MessageBox.Show("Za minimalni i miaksimalni broj igrača unesite brojeve od 2 do 20");
                    textBoxMin.BackColor = Color.IndianRed;
                }
                catch (Exception ec)
                {
                    MessageBox.Show("Neobradjeni prekid, presli ste nas "+ec);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Unesite naziv alijanse");
                textBoxNaziv.BackColor = Color.IndianRed;
            }

        }
    }
}
