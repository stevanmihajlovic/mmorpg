﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class BuyVehicle : Form
    {
        public Igrac LogovanIgrac;
        public Lik LogovanLik;
        public IList<PrevoznoSredstvo> prevoznasredstva;
        public GameForm g;
        public BuyVehicle()
        {
            InitializeComponent();
        }

        public BuyVehicle(Igrac i, Lik l,GameForm game)
        {
            
            InitializeComponent();
            g = game;
            LogovanIgrac = i;
            LogovanLik = l;
            try
            {
                ISession s = DataLayer.GetSession();

                prevoznasredstva = (from a in s.Query<Entiteti.PrevoznoSredstvo>()
                            where (a.Cena < LogovanLik.KolicinaZlata && a.PripadaIgracu==null)
                            //where (a.Cena < LogovanLik.KolicinaZlata && (!LogovanIgrac.PrevoznaSredstva.Contains(a)))
                            select a).ToList<PrevoznoSredstvo>();
                if (prevoznasredstva.Count > 0)
                {
                    foreach (PrevoznoSredstvo a in prevoznasredstva)
                    {
                        if (a.GetType() == typeof(PrevoznoSredstvoZmaj))
                            listBox1.Items.Add(a.Naziv + "\t \t Zmaj");
                        if (a.GetType() == typeof(PrevoznoSredstvoMehanicki))
                            listBox1.Items.Add(a.Naziv + "\t \t Mehanicki");
                        if (a.GetType() == typeof(PrevoznoSredstvoZivotinja))
                            listBox1.Items.Add(a.Naziv + "\t \t Zivotinja");
                    }
                    s.Flush();
                    s.Close();
                }
                else
                    MessageBox.Show("Nemate novca za neko prevozno sredstvo");
                    
            }
            catch (Exception ec)
            {
                MessageBox.Show("Neobradjena greska");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (listBox1.SelectedIndex == -1)
                    MessageBox.Show("Izaberite prevozno sredstvo za kupovinu");
                else
                {
                    ISession s = DataLayer.GetSession();

                    s.Lock(prevoznasredstva[listBox1.SelectedIndex], LockMode.None);
                    s.Lock(LogovanIgrac, LockMode.None);
                    s.Lock(LogovanLik, LockMode.None);

                    PrevoznoSredstvo selektovana = prevoznasredstva[listBox1.SelectedIndex];
                    LogovanIgrac.PrevoznaSredstva.Add(selektovana);
                    selektovana.PripadaIgracu = LogovanIgrac;
                    LogovanLik.KolicinaZlata -= selektovana.Cena;
                   
                    s.SaveOrUpdate(LogovanIgrac);   //igrac ce da povuce save za lika i prevozno sredstvo
                    
                    s.Flush();
                    s.Close();

                    g.listBoxPrevoznaSredstva.Items.Add(listBox1.Items[listBox1.SelectedIndex]);
                    MessageBox.Show("Uspešno ste kupili prevozno sredstvo");
                    this.Close();
                }
            }
            catch (Exception ec)
            {
                    MessageBox.Show("Neobradjena greska");
            }
        }
    }
}
