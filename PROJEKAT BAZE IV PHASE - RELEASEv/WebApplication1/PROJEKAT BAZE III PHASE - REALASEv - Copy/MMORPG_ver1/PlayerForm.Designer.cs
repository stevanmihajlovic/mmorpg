﻿namespace MMORPG_ver1
{
    partial class PlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PrijaviteSe = new System.Windows.Forms.Button();
            this.RegistrujteSe = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PrijaviteSe
            // 
            this.PrijaviteSe.Location = new System.Drawing.Point(128, 55);
            this.PrijaviteSe.Name = "PrijaviteSe";
            this.PrijaviteSe.Size = new System.Drawing.Size(99, 23);
            this.PrijaviteSe.TabIndex = 0;
            this.PrijaviteSe.Text = "Prijavite se";
            this.PrijaviteSe.UseVisualStyleBackColor = true;
            this.PrijaviteSe.Click += new System.EventHandler(this.PrijaviteSe_Click);
            // 
            // RegistrujteSe
            // 
            this.RegistrujteSe.Location = new System.Drawing.Point(128, 97);
            this.RegistrujteSe.Name = "RegistrujteSe";
            this.RegistrujteSe.Size = new System.Drawing.Size(99, 23);
            this.RegistrujteSe.TabIndex = 1;
            this.RegistrujteSe.Text = "Registrujte se";
            this.RegistrujteSe.UseVisualStyleBackColor = true;
            this.RegistrujteSe.Click += new System.EventHandler(this.RegistrujteSe_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Imate nalog";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nemate nalog";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(96, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "DOBRODOŠLI";
            // 
            // PlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 149);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RegistrujteSe);
            this.Controls.Add(this.PrijaviteSe);
            this.Name = "PlayerForm";
            this.Text = "PlayerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button PrijaviteSe;
        private System.Windows.Forms.Button RegistrujteSe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}