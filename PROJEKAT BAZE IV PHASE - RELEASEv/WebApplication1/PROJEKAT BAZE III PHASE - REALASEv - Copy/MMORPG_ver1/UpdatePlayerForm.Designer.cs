﻿namespace MMORPG_ver1
{
    partial class UpdatePlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonZenski = new System.Windows.Forms.RadioButton();
            this.radioButtonMuski = new System.Windows.Forms.RadioButton();
            this.textBoxStarost = new System.Windows.Forms.TextBox();
            this.textBoxPrezime = new System.Windows.Forms.TextBox();
            this.textBoxIme = new System.Windows.Forms.TextBox();
            this.textBoxLozinka = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.RegistrujSe = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // radioButtonZenski
            // 
            this.radioButtonZenski.AutoSize = true;
            this.radioButtonZenski.Location = new System.Drawing.Point(217, 114);
            this.radioButtonZenski.Name = "radioButtonZenski";
            this.radioButtonZenski.Size = new System.Drawing.Size(57, 17);
            this.radioButtonZenski.TabIndex = 33;
            this.radioButtonZenski.Text = "Ženski";
            this.radioButtonZenski.UseVisualStyleBackColor = true;
            // 
            // radioButtonMuski
            // 
            this.radioButtonMuski.AutoSize = true;
            this.radioButtonMuski.Checked = true;
            this.radioButtonMuski.Location = new System.Drawing.Point(154, 114);
            this.radioButtonMuski.Name = "radioButtonMuski";
            this.radioButtonMuski.Size = new System.Drawing.Size(53, 17);
            this.radioButtonMuski.TabIndex = 32;
            this.radioButtonMuski.TabStop = true;
            this.radioButtonMuski.Text = "Muški";
            this.radioButtonMuski.UseVisualStyleBackColor = true;
            // 
            // textBoxStarost
            // 
            this.textBoxStarost.Location = new System.Drawing.Point(151, 134);
            this.textBoxStarost.Name = "textBoxStarost";
            this.textBoxStarost.Size = new System.Drawing.Size(121, 20);
            this.textBoxStarost.TabIndex = 31;
            // 
            // textBoxPrezime
            // 
            this.textBoxPrezime.Location = new System.Drawing.Point(151, 91);
            this.textBoxPrezime.Name = "textBoxPrezime";
            this.textBoxPrezime.Size = new System.Drawing.Size(121, 20);
            this.textBoxPrezime.TabIndex = 30;
            // 
            // textBoxIme
            // 
            this.textBoxIme.Location = new System.Drawing.Point(151, 68);
            this.textBoxIme.Name = "textBoxIme";
            this.textBoxIme.Size = new System.Drawing.Size(121, 20);
            this.textBoxIme.TabIndex = 29;
            // 
            // textBoxLozinka
            // 
            this.textBoxLozinka.Location = new System.Drawing.Point(151, 45);
            this.textBoxLozinka.Name = "textBoxLozinka";
            this.textBoxLozinka.Size = new System.Drawing.Size(121, 20);
            this.textBoxLozinka.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 134);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(5);
            this.label7.Size = new System.Drawing.Size(95, 23);
            this.label7.TabIndex = 25;
            this.label7.Text = "Unesite Starost: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 111);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(5);
            this.label6.Size = new System.Drawing.Size(107, 23);
            this.label6.TabIndex = 24;
            this.label6.Text = "Unesite Pol (M/Ž): ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 88);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5);
            this.label5.Size = new System.Drawing.Size(126, 23);
            this.label5.TabIndex = 23;
            this.label5.Text = "Unesite Vaše Prezime: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 65);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5);
            this.label4.Size = new System.Drawing.Size(106, 23);
            this.label4.TabIndex = 22;
            this.label4.Text = "Unesite Vaše Ime: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 42);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(126, 23);
            this.label2.TabIndex = 21;
            this.label2.Text = "Unesite novu Lozinku: ";
            // 
            // RegistrujSe
            // 
            this.RegistrujSe.Location = new System.Drawing.Point(197, 195);
            this.RegistrujSe.Name = "RegistrujSe";
            this.RegistrujSe.Size = new System.Drawing.Size(75, 23);
            this.RegistrujSe.TabIndex = 20;
            this.RegistrujSe.Text = "Izmenite";
            this.RegistrujSe.UseVisualStyleBackColor = true;
            this.RegistrujSe.Click += new System.EventHandler(this.RegistrujSe_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(103, 25);
            this.label1.TabIndex = 34;
            this.label1.Text = "Izmenite podatke:";
            // 
            // UpdatePlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 235);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButtonZenski);
            this.Controls.Add(this.radioButtonMuski);
            this.Controls.Add(this.textBoxStarost);
            this.Controls.Add(this.textBoxPrezime);
            this.Controls.Add(this.textBoxIme);
            this.Controls.Add(this.textBoxLozinka);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RegistrujSe);
            this.Name = "UpdatePlayerForm";
            this.Text = "UpdatePlayerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonZenski;
        private System.Windows.Forms.RadioButton radioButtonMuski;
        private System.Windows.Forms.TextBox textBoxStarost;
        private System.Windows.Forms.TextBox textBoxPrezime;
        private System.Windows.Forms.TextBox textBoxIme;
        private System.Windows.Forms.TextBox textBoxLozinka;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button RegistrujSe;
        private System.Windows.Forms.Label label1;
    }
}