﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;


namespace MMORPG_ver1
{
    public partial class CreateUnion : Form
    {
        public IList<Alijansa> alijanse;
        public IList<Alijansa> listazadatakanakojimaNIJEalijansa;
        
        public CreateUnion()
        {
            InitializeComponent();
            try
            {
                ISession s = DataLayer.GetSession();

                alijanse = (from a in s.Query<Entiteti.Alijansa>()
                                select a).ToList<Alijansa>();
                if (alijanse.Count > 0)
                {
                    foreach (Alijansa a in alijanse)
                    {
                        listBox1.Items.Add(a.Naziv);
                    }
                }
                else
                    MessageBox.Show("Ne postoje alijanse");
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show("Neobradjena greska");
            }
        }

        private void buttonKreirajSavez_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex==-1)
                MessageBox.Show("Izaberite alijansu");
            else
            {
                try
                {
                    ISession s = DataLayer.GetSession();
                    s.Lock(alijanse[listBox1.SelectedIndex], LockMode.None);
                    s.Lock(listazadatakanakojimaNIJEalijansa[listBox2.SelectedIndex], LockMode.None);
                    alijanse[listBox1.SelectedIndex].USavezuSa.Add(listazadatakanakojimaNIJEalijansa[listBox2.SelectedIndex]);
                    listazadatakanakojimaNIJEalijansa[listBox2.SelectedIndex].USavezuSa.Add(alijanse[listBox1.SelectedIndex]);
                    
                    //s.SaveOrUpdate(alijanse[listBox1.SelectedIndex]); //odlucili smo se za po jedan niz u alijansama
                                                                      //za saveze
                    s.SaveOrUpdate(listazadatakanakojimaNIJEalijansa[listBox2.SelectedIndex]);
                    //ne znam zasto sacuva i drugu stranu kada joj kazemo da sacuva samo jednu a pritom nema cascade
                    

                    s.Flush();
                    s.Close();
                    MessageBox.Show("Uspešno ste kreirali savez");
                    this.Close();
                }
                catch (Exception ec)
                {
                    if (ec.InnerException.Message.Contains("unique")) //ako savez vec postoji
                        MessageBox.Show("Uneti savez postoji");
                    else
                        MessageBox.Show(ec.Message);
                }
            }
        }

        private void buttonIzadji_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            try
            {
                ISession s = DataLayer.GetSession();

                Alijansa selektovana = alijanse[listBox1.SelectedIndex];
                s.Lock(selektovana, LockMode.None);

                foreach (Alijansa a in selektovana.USavezuSa)
                {
                    listBox3.Items.Add(a.Naziv);
                }

                IList<int> IdalijansisakojimaJEusavezuselektovana = selektovana.USavezuSa.Select(x => x.IdAlijansa).ToList();
                IdalijansisakojimaJEusavezuselektovana.Add(selektovana.IdAlijansa);

                listazadatakanakojimaNIJEalijansa = alijanse.Where(x => !IdalijansisakojimaJEusavezuselektovana.Contains(x.IdAlijansa)).ToList();

                if (listazadatakanakojimaNIJEalijansa.Count > 0)
                {
                    foreach (Alijansa a in listazadatakanakojimaNIJEalijansa)   //kako proveriti da vec nije na tom zadatku?
                    {
                        listBox2.Items.Add(a.Naziv);
                    }
                }
                else
                    MessageBox.Show("Nema alijansi sa kojima ova alijansa moze da sklopi savez");

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void buttonRaspustiSavez_Click(object sender, EventArgs e)
        {
            if (listBox3.SelectedIndex == -1)
                MessageBox.Show("Izaberite alijansu sa kojom zelite da raskinete savez");
            else
            {
                try
                {
                    ISession s = DataLayer.GetSession();
                    Alijansa selektovana = alijanse[listBox1.SelectedIndex];
                    Alijansa drugaselektovana = selektovana.USavezuSa[listBox3.SelectedIndex];
                    s.Lock(selektovana, LockMode.None);
                    s.Lock(drugaselektovana, LockMode.None);

                    selektovana.USavezuSa.RemoveAt(listBox3.SelectedIndex); //moze i samo remove drugaselektovana
                    drugaselektovana.USavezuSa.Remove(selektovana);

                    s.SaveOrUpdate(selektovana);
                    
                    s.Flush();
                    s.Close();
                    MessageBox.Show("Uspesno ste raskinuli savez");
                    this.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }
            }
        }
    }
}
