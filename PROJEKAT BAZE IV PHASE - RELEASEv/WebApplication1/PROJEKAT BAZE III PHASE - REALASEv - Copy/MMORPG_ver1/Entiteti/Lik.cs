﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMORPG_ver1.Entiteti
{
    public abstract class Lik
    {
        public virtual int IdLik { get; protected set; }
        public virtual int StepenZamora { get; set; }
        public virtual int Iskustvo { get; set; }
        public virtual int Zdravlje { get; set; }
        public virtual int KolicinaZlata { get; set; }

        public virtual int BonusSkrivanjaSegrta { get; set; }   //atribut veze prelazi u tabelu lika, imaju ga samo segrti
        public virtual string Ime { get; set; }                 //segrti imaju ime, likovi ne
        public virtual Igrac IdIgraca { get; set; }             //referenca na Igraca za 1:1 vezu igrac-lik
        public virtual Lik GospodarSegrta { get; set; }         //referenca na Lika za 1:N rekurzivnu vezu
        public virtual IList<Lik> Segrti { get; set; }          //referenca na lika za 1:N rekurzivnu vezu
    }
    public class LikVilenjak :Lik
    {
        public virtual int Energija { get; set; }
    }
    public class LikDemon : Lik
    {
        public virtual int Energija { get; set; }
    }
    public class LikOrk : Lik
    {
        public virtual int SpecijalizacijaOruzja { get; set; }
    }
    public class LikPatuljak : Lik
    {
        public virtual int SpecijalizacijaOruzja { get; set; }
    }
    public class LikCovek : Lik
    {
        public virtual int BonusSkrivanja { get; set; }
    }


}
