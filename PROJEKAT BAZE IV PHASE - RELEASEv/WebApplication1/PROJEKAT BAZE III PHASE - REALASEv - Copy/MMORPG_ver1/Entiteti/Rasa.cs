﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMORPG_ver1.Entiteti
{
    public class Rasa
    {
        public virtual int IdRasa {get; protected set;}
        public virtual string TipRase { get; set; }
        public virtual BonusItem IdItema { get; set; }

    }
}
