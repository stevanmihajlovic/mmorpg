﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMORPG_ver1.Entiteti
{
    public abstract class Item      //abstraktno
    {
        public virtual int IdItem { get; protected set; }

        public virtual IList<Igrac> Igraci { get; set; }

        public Item()
        {
            Igraci = new List<Igrac>();
        }
    }
    public abstract class BonusItem : Item  //isto abstraktno
    {
        public virtual int Iskustvo { get; set; }
        public virtual IList<Rasa> Rase { get; set; }    //visevrednosti 1:N veza sa klasom Rasa
        public BonusItem()
        {
            Rase = new List<Rasa>();
        }
        
    }

    public abstract class KljucniItem : Item //isto abstraktno
    {
        public virtual string Ime { get; set; } //ime itema, ne treba nadimak igraca jer je vec u vezi sa njim...
        public virtual string Opis { get; set; }
    }

    public class OruzjeBonusItem : BonusItem
    {

    }
    public class PredmetBonusItem : BonusItem
    {

    }

    public class OruzjeKljucniItem : KljucniItem
    {

    }
    public class PredmetKljucniItem : KljucniItem
    {

    }
}
