﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMORPG_ver1.Entiteti
{
    public abstract class Zadatak
    {
        public virtual int IdZadatak { get; protected set; }
        public virtual DateTime VremePocetka { get; set; }
        public virtual DateTime VremeKraja { get; set; }
        public virtual int BonusIskustvo { get; set; }
    }

    public class SoloZadatak : Zadatak
    {
        public virtual IList<Igrac> Solo { get; set; }
        public SoloZadatak()
        {
            Solo = new List<Igrac>(); 
        }
    }
    public class GrupniZadatak : Zadatak
    { 
        public virtual IList<Alijansa> Grupni { get; set; }
        public GrupniZadatak()
        {
            Grupni = new List<Alijansa>();
        }
    }
}
