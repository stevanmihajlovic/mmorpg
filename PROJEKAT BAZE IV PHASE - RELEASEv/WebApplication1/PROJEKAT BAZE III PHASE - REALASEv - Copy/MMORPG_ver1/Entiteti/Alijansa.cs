﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMORPG_ver1.Entiteti
{
    public class Alijansa
    {
        public virtual int IdAlijansa { get; protected set; }
        public virtual string Naziv { get; set; }
        public virtual int Min { get; set; }
        public virtual int Max { get; set; }
        public virtual int BonusIskustvo { get; set; }                      //treba da imamo dva bonusa a ne jedan
        public virtual int BonusZdravlje { get; set; }

        public virtual IList<Igrac> Igraci { get; set; }            //niz igraca za 1:N vezu alijansa-igraci

        public virtual IList<GrupniZadatak> Zadaci { get; set; }    //niz grupnih zadataka za M:N vezu alijanse-zadaci

        public virtual IList<Alijansa> USavezuSa { get; set; }  //dva niza za M:N rekurzivnu vezu SAVEZ
        
        public Alijansa()
        {
            Igraci = new List<Igrac>();
            Zadaci = new List<GrupniZadatak>();
            USavezuSa = new List<Alijansa>();
        }
    }
}
