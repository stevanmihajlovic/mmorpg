﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMORPG_ver1.Entiteti
{
    public class Igrac
    {
        public virtual int IdIgrac { get; protected set; }
        public virtual string Nadimak { get; set; }
        public virtual string Lozinka { get; set; }
        public virtual string Ime { get; set; }
        public virtual string Prezime { get; set; }
        public virtual char Pol { get; set; }
        public virtual int Starost { get; set; }

        public virtual IList<PrevoznoSredstvo> PrevoznaSredstva { get; set; }       //Niz prevoznih sredstava za 1:N vezu igrac-prevozna sredstva
        public virtual IList<Sesija> Sesije { get; set; }                           //Niz sesija za 1:N vezu igrac-sesije
        public virtual IList<SoloZadatak> Zadaci { get; set; }                      //Niz zadataka za M:N vezu igraci-zadaci
        public virtual IList<Item> Itemi { get; set; }                              //Niz itema za M:N vezu igraci-itemi
        public virtual Lik IdLika { get; set; }                                     //Referencira ID_LIKA za 1:1 sa likom
        public virtual Alijansa PripadaAlijansi { get; set; }                       //Referencira ID_ALIJANSE za N:1 sa alijansom 

        
        public Igrac()
        {
            PrevoznaSredstva = new List<PrevoznoSredstvo>();
            Sesije = new List<Sesija>();
            Zadaci = new List<SoloZadatak>();
            Itemi = new List<Item>();
        }

    }
}
