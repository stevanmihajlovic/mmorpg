﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMORPG_ver1.Entiteti
{
    public abstract class PrevoznoSredstvo
    {
        public virtual int IdPrevoznoSredstvo { get; protected set; }
        public virtual string Naziv { get; set; }
        public virtual int Brzina { get; set; }
        public virtual int Cena { get; set; }

        public virtual Igrac PripadaIgracu { get; set; }    //referenca na ID_IGRACA za 1:N vezu igrac-prevozna sredstva
        public PrevoznoSredstvo()
        {
        }
    }

    public class PrevoznoSredstvoZmaj : PrevoznoSredstvo
    {
    }
    public class PrevoznoSredstvoMehanicki : PrevoznoSredstvo
    {
    }
    public class PrevoznoSredstvoZivotinja : PrevoznoSredstvo
    {
    }

}
