﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMORPG_ver1.Entiteti
{
    public class Sesija
    {
        public virtual int IdSesija { get; protected set; }
        public virtual DateTime VremePocetka { get; set; }
        public virtual DateTime VremeKraja { get; set; }
        public virtual int Iskustvo { get; set; }
        public virtual int Zlato { get; set; }
        
        
        public virtual Igrac VezanaZaIgraca { get; set; }   //Referenca na ID_IGRACA za 1:N vezu igrac-sesije
                                                            
                                                            //TREBA DA SOFTVEROM REALIZUJEMO SLABI TIP
        public Sesija()
        {
        }
    }
}
