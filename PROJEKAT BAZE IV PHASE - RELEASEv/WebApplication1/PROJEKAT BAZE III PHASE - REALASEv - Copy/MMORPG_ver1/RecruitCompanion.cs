﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;

namespace MMORPG_ver1
{
    public partial class RecruitCompanion : Form
    {
        public Lik LogovanLik;
        public GameForm g;
        public RecruitCompanion()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 4;
        }

        public RecruitCompanion(Lik l,GameForm game)
        {
            g = game;
            LogovanLik = l;
            InitializeComponent();
            comboBox1.SelectedIndex = 4;
        }
        private void buttonRecruit_Click(object sender, EventArgs e)
        {
            if (textBoxName.TextLength > 0)
            {
                try
                {
                    //dodati prepravke male za naziv i ostalo
                    Entiteti.Lik c;
                    ISession s = DataLayer.GetSession();
                    s.Lock(LogovanLik, LockMode.None);

                    switch (comboBox1.SelectedIndex)
                    {
                        case 0:
                            {
                                c = new Entiteti.LikVilenjak();
                                ((Entiteti.LikVilenjak)c).Energija = 100;
                                break;
                            }
                        case 1:
                            {
                                c = new Entiteti.LikDemon();
                                ((Entiteti.LikDemon)c).Energija = 100;
                                break;
                            }
                        case 2:
                            {
                                c = new Entiteti.LikOrk();
                                ((Entiteti.LikOrk)c).SpecijalizacijaOruzja = 10;
                                break;
                            }
                        case 3:
                            {
                                c = new Entiteti.LikPatuljak();
                                ((Entiteti.LikPatuljak)c).SpecijalizacijaOruzja = 10;
                                break;
                            }
                        default:
                            {
                                c = new Entiteti.LikCovek();
                                ((Entiteti.LikCovek)c).BonusSkrivanja = 5;
                                break;
                            }
                    }

                    //nemamo ID igraca

                    c.Iskustvo = 0;
                    c.Zdravlje = 100;
                    c.StepenZamora = 0;
                    c.KolicinaZlata = 0;

                    c.Ime = textBoxName.Text;
                    c.GospodarSegrta = LogovanLik;
                    c.BonusSkrivanjaSegrta = 20;

                    LogovanLik.Segrti.Add(c);

                    
                    s.SaveOrUpdate(LogovanLik); //lik ce da povuce save za segrta
                    s.Flush();
                    s.Close();
                    g.listBoxSegrti.Items.Add(c.Ime);
                    
                    MessageBox.Show("Uspesno ste regrutovali šegrta");
                    this.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show("greska");
                }
            }
            else
            {
                MessageBox.Show("Unesite naziv šegrta");
                textBoxName.BackColor = Color.IndianRed;
            }

        }
    }
}
