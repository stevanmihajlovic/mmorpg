﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;

namespace MMORPG_ver1
{
    public partial class UpdatePlayerForm : Form
    {
        public Igrac LogovanIgrac;
        public UpdatePlayerForm()
        {
            InitializeComponent();
        }
        public UpdatePlayerForm(Igrac l)
        {
            InitializeComponent();
            LogovanIgrac = l;
        }

        private void RegistrujSe_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                if (textBoxIme.TextLength > 0) LogovanIgrac.Ime = textBoxIme.Text;
                if (textBoxPrezime.TextLength > 0) LogovanIgrac.Prezime = textBoxPrezime.Text;
                if (radioButtonMuski.Checked)
                    LogovanIgrac.Pol = 'M';
                else
                    LogovanIgrac.Pol = 'Z';
                if (textBoxStarost.TextLength != 0)
                    LogovanIgrac.Starost = Convert.ToInt32(textBoxStarost.Text);
                else
                    LogovanIgrac.Starost = 0;
                if (textBoxLozinka.TextLength > 0) LogovanIgrac.Lozinka = textBoxLozinka.Text;
                s.SaveOrUpdate(LogovanIgrac);
                s.Flush();
                s.Close();
                this.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show("Neobradjena greska");
            }
        }
    }
}
