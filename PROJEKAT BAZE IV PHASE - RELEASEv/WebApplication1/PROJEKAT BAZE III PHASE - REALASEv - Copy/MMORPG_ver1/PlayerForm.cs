﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMORPG_ver1
{
    public partial class PlayerForm : Form
    {
        public PlayerForm()
        {
            InitializeComponent();
        }

        private void PrijaviteSe_Click(object sender, EventArgs e)
        {
            LoginForm nova = new LoginForm();
            nova.ShowDialog();
        }

        private void RegistrujteSe_Click(object sender, EventArgs e)
        {
            RegisterForm nova = new RegisterForm();
            nova.ShowDialog();
        }
    }
}
