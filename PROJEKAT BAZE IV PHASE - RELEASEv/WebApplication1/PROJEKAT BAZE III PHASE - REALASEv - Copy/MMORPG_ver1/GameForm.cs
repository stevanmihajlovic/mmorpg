﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;

namespace MMORPG_ver1
{
    public partial class GameForm : Form
    {
        public Igrac LogovanIgrac;
        public Lik LogovanLik;
        public Sesija sesija;
        public GameForm()
        {
            InitializeComponent();
        }

        public GameForm(Igrac i,Lik l)
        {
            
            InitializeComponent();
            LogovanIgrac = i;
            LogovanLik = l;

            try
            {
                ISession s1 = DataLayer.GetSession();
                s1.Lock(LogovanIgrac, LockMode.None);
                s1.Lock(LogovanLik, LockMode.None);
                label1.Text="Podaci o Igraču  "+LogovanIgrac.Nadimak;
                labelIme.Text = LogovanIgrac.Ime;
                labelPrezime.Text = LogovanIgrac.Prezime;
                labelStarost.Text = LogovanIgrac.Starost.ToString();
                labelPol.Text = LogovanIgrac.Pol.ToString();

                if (LogovanLik.GetType() == typeof(Entiteti.LikDemon)) 
                {
                    labelRasa.Text= "Demon";
                    labelSpecijalnost.Text = "Energija";
                    labelBonusSpecijalnosti.Text = ((LikDemon)LogovanLik).Energija.ToString();
                }
                if (LogovanLik.GetType() == typeof(LikVilenjak))
                {
                    labelRasa.Text = "Vilenjak";
                    labelSpecijalnost.Text = "Energija";
                    labelBonusSpecijalnosti.Text = ((LikVilenjak)LogovanLik).Energija.ToString();
                }
                if (LogovanLik.GetType() == typeof(LikOrk))
                {
                    labelRasa.Text = "Ork";
                    labelSpecijalnost.Text = "Specijalizacija oružja";
                    labelBonusSpecijalnosti.Text = ((LikOrk)LogovanLik).SpecijalizacijaOruzja.ToString();
                }
                if (LogovanLik.GetType() == typeof(LikPatuljak))
                {
                    labelRasa.Text = "Patuljak";
                    labelSpecijalnost.Text = "Specijalizacija oružja";
                    labelBonusSpecijalnosti.Text = ((LikPatuljak)LogovanLik).SpecijalizacijaOruzja.ToString();
                }
                if (LogovanLik.GetType() == typeof(LikCovek))
                {
                    labelRasa.Text = "Čovek";
                    labelSpecijalnost.Text = "Bonus u skrivanju";
                    labelBonusSpecijalnosti.Text = ((LikCovek)LogovanLik).BonusSkrivanja.ToString();
                }

                labelIskustvo.Text = LogovanLik.Iskustvo.ToString();
                labelZlato.Text = LogovanLik.KolicinaZlata.ToString();
                labelZamor.Text = LogovanLik.StepenZamora.ToString();
                labelZdravlje.Text = LogovanLik.Zdravlje.ToString();

                foreach (Lik l1 in LogovanLik.Segrti)
                {
                    listBoxSegrti.Items.Add(l1.Ime);
                }

                foreach (SoloZadatak z1 in LogovanIgrac.Zadaci)
                {
                    listBoxZadaci.Items.Add(z1.IdZadatak);
                }

                foreach (Item a1 in LogovanIgrac.Itemi)
                {
                    listBoxItemi.Items.Add(a1.IdItem);
                }
                foreach (PrevoznoSredstvo p1 in LogovanIgrac.PrevoznaSredstva)
                {
                    if (p1.GetType() == typeof(PrevoznoSredstvoZmaj))
                        listBoxPrevoznaSredstva.Items.Add(p1.Naziv + "\t \t Zmaj");
                    if (p1.GetType() == typeof(PrevoznoSredstvoMehanicki))
                        listBoxPrevoznaSredstva.Items.Add(p1.Naziv + "\t \t Mehanicki");
                    if (p1.GetType() == typeof(PrevoznoSredstvoZivotinja))
                        listBoxPrevoznaSredstva.Items.Add(p1.Naziv + "\t \t Zivotinja");
                }

                if (LogovanIgrac.PripadaAlijansi == null)
                {
                    buttonPridruziteSeAlijansi.Visible=true;
                    KreairajteAlijansu.Visible=true;
                    buttonNapustiteAlijansu.Visible=false;
                    labelAlijansa.Visible = false;
                }
                else
                {
                    buttonPridruziteSeAlijansi.Visible = false;
                    KreairajteAlijansu.Visible = false;
                    buttonNapustiteAlijansu.Visible = true;
                    labelAlijansa.Visible = true;
                    labelAlijansa.Text="Pripadate Alijansi "+LogovanIgrac.PripadaAlijansi.Naziv;
                }
                                        

                sesija = new Sesija();

                sesija.Iskustvo = 0; //pri zatvaranju primeni razliku neku
                sesija.VremePocetka = DateTime.Now;
                sesija.Zlato = 0;//isto pri zatvaranju, a i vreme kraja
                sesija.VezanaZaIgraca = LogovanIgrac;

                LogovanIgrac.Sesije.Add(sesija);

                s1.SaveOrUpdate(LogovanIgrac);  //igrac ce da povuce i sesiju
                
                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show("Neuspelo zapoceta sesija");
            }
        }

        private void button1_Click(object sender, EventArgs e)      //odakle ovo ovde?
        {
            GameForm nova = new GameForm();
            nova.ShowDialog();
        }

        private void KreairajteAlijansu_Click(object sender, EventArgs e)
        {
            CreateAlianceForm nova = new CreateAlianceForm(LogovanIgrac,this);
            nova.ShowDialog();
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s1 = DataLayer.GetSession();
                sesija.VremeKraja = DateTime.Now;
                s1.SaveOrUpdate(sesija);
                s1.Flush();
                s1.Close();
                //vidi za zlato i iskustvo
            }
            catch (Exception ec)
            {
                MessageBox.Show("Neuspelo zatvaranje sesije");
            }

            this.Close();
        }

        private void buttonPridruziteSeAlijansi_Click(object sender, EventArgs e)
        {
            JoinAlianceForm nova = new JoinAlianceForm(LogovanIgrac,this);
            nova.ShowDialog();
              //resiti da ne moze da se prebaci focus pre gasenja novootvorene
            
        }

        private void buttonNapustiteAlijansu_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                LogovanIgrac.PripadaAlijansi = null;
                s.SaveOrUpdate(LogovanIgrac);       //nema potrebe ista da menjamo u ALijansi jer taj niz prakticno i ne postoji
                s.Flush();                          //proveriti da li ima potrebe da proveravamo kada alijansa nema igraca
                s.Close();

                buttonPridruziteSeAlijansi.Visible = true;
                KreairajteAlijansu.Visible = true;
                buttonNapustiteAlijansu.Visible = false;
                labelAlijansa.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonRegrutujSegrta_Click(object sender, EventArgs e)
        {
            RecruitCompanion nova = new RecruitCompanion(LogovanLik,this);
            nova.ShowDialog();
        }

        private void buttonKupiPrevoznoSredstvo_Click(object sender, EventArgs e)
        {
            BuyVehicle nova = new BuyVehicle(LogovanIgrac, LogovanLik,this);
            nova.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PlayerChooseQuestForm nova = new PlayerChooseQuestForm(LogovanIgrac,this);
            nova.ShowDialog();
        }

        private void buttonPokupiteItem_Click(object sender, EventArgs e)
        {
            LootItemForm nova = new LootItemForm(LogovanIgrac,this);
            nova.ShowDialog();
        }

        private void buttonIzmeniPodatke_Click(object sender, EventArgs e)
        {
            UpdatePlayerForm nova = new UpdatePlayerForm(LogovanIgrac);
            nova.ShowDialog();
        }

        private void buttonOtpustiSegrta_Click(object sender, EventArgs e)
        {
                try
                {
                    if (listBoxSegrti.SelectedIndex == -1)
                        MessageBox.Show("Izaberite segrta za otpustanje");
                    else
                    {
                        ISession s = DataLayer.GetSession();
                        s.Lock(LogovanLik, LockMode.None);
                        Lik brisi = s.Load<Lik>(LogovanLik.Segrti[listBoxSegrti.SelectedIndex].IdLik);
                        LogovanLik.Segrti.RemoveAt(listBoxSegrti.SelectedIndex);
                        listBoxSegrti.Items.RemoveAt(listBoxSegrti.SelectedIndex);
                        s.SaveOrUpdate(LogovanLik);
                        s.Delete(brisi);
                        s.Flush();
                        s.Close();
                    }
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (listBoxPrevoznaSredstva.SelectedIndex == -1)
                    MessageBox.Show("Izaberite prevozno sredstvo za otpustanje");
                else
                {
                    ISession s = DataLayer.GetSession();
                    s.Lock(LogovanIgrac, LockMode.None);
                    LogovanIgrac.PrevoznaSredstva[listBoxPrevoznaSredstva.SelectedIndex].PripadaIgracu = null;
                    LogovanIgrac.PrevoznaSredstva.RemoveAt(listBoxPrevoznaSredstva.SelectedIndex);
                    listBoxPrevoznaSredstva.Items.RemoveAt(listBoxPrevoznaSredstva.SelectedIndex);
                    //cak i da imamo visak saveorupdate ima da prodje... cascadeall je samo olaksanje
                    s.SaveOrUpdate(LogovanIgrac);
                    s.Flush();
                    s.Close();
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void buttonZavrsiteZadatak_Click(object sender, EventArgs e)
        {
            try
            {
                if (listBoxZadaci.SelectedIndex == -1)
                    MessageBox.Show("Izaberite zadatak koji želite da završite");
                else
                {
                    ISession s = DataLayer.GetSession();
                    s.Lock(LogovanIgrac, LockMode.None);
                    LogovanIgrac.Zadaci[listBoxZadaci.SelectedIndex].VremeKraja = DateTime.Now;
                    sesija.Iskustvo += LogovanIgrac.Zadaci[listBoxZadaci.SelectedIndex].BonusIskustvo;
                    LogovanLik.Iskustvo += LogovanIgrac.Zadaci[listBoxZadaci.SelectedIndex].BonusIskustvo;
                    sesija.Zlato += 100;    //ne znamo kako se zamislilo dodavanje zlata pa smo ubacili linearno 100 po zadatku
                    LogovanLik.KolicinaZlata += 100;
                    LogovanIgrac.Zadaci.RemoveAt(listBoxZadaci.SelectedIndex);
                    listBoxZadaci.Items.RemoveAt(listBoxZadaci.SelectedIndex);
                    //zbog izbacivanja inverse u mapiranju ove veze ovo ce uspeti da prodje

                    s.SaveOrUpdate(LogovanIgrac);
                    s.Flush();
                    s.Close();
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void buttonBaciteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (listBoxItemi.SelectedIndex == -1)
                    MessageBox.Show("Izaberite item za bacanje");
                else
                {
                    ISession s = DataLayer.GetSession();
                    s.Lock(LogovanIgrac, LockMode.None);

                    LogovanIgrac.Itemi.RemoveAt(listBoxItemi.SelectedIndex);
                    listBoxItemi.Items.RemoveAt(listBoxItemi.SelectedIndex);
                    //zbog izbacivanja inverse u mapiranju ove veze ovo ce uspeti da prodje

                    s.SaveOrUpdate(LogovanIgrac);   //interesantno, svugde smo imali logovanlik ovde i na 3 mesta
                    s.Flush();                      //gore i sve je to uspevalo da prodje...
                    s.Close();
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }
       

       

    }
}
