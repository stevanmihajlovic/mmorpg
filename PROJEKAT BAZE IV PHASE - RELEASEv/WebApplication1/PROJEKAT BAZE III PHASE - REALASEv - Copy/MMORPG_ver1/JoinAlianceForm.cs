﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class JoinAlianceForm : Form
    {
        public Igrac LogovaniIgrac;

        public IList<Alijansa> slobodne;

        public GameForm g;

        public JoinAlianceForm()
        {
            InitializeComponent();
        }

        public JoinAlianceForm(Igrac l,GameForm g1)
        {

            g = g1;
            LogovaniIgrac = l;
            InitializeComponent();
            try
            {
                ISession s = DataLayer.GetSession();

                slobodne = (from a in s.Query<Entiteti.Alijansa>()
                                           where (a.Igraci.Count < a.Max)
                                           select a).ToList<Alijansa>();
                if (slobodne.Count > 0)
                {
                    foreach (Alijansa a in slobodne)
                    {
                        listBox1.Items.Add(a.Naziv);
                    }
                }
                else
                    MessageBox.Show("Nema slobodnih alijansi");
                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                ISession s = DataLayer.GetSession();
                s.Lock(slobodne[listBox1.SelectedIndex], LockMode.None);
                
                Alijansa selektovana = slobodne[listBox1.SelectedIndex];
                LogovaniIgrac.PripadaAlijansi = selektovana;
                selektovana.Igraci.Add(LogovaniIgrac);
                
                s.SaveOrUpdate(selektovana);    //alijansa ce da povuce i save za igraca
                s.Flush();
                s.Close();

                MessageBox.Show("Uspešno ste se pridružili alijansi");
                this.Close();
                if (g.LogovanIgrac.PripadaAlijansi == null)
                {
                    g.buttonPridruziteSeAlijansi.Visible = true;
                    g.KreairajteAlijansu.Visible = true;
                    g.buttonNapustiteAlijansu.Visible = false;
                    g.labelAlijansa.Visible = false;
                    
                }
                else
                {
                    g.buttonPridruziteSeAlijansi.Visible = false;
                    g.KreairajteAlijansu.Visible = false;
                    g.buttonNapustiteAlijansu.Visible = true;
                    g.labelAlijansa.Visible = true;
                    g.labelAlijansa.Text = "Pripadate Alijansi: "+selektovana.Naziv;
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show("Neobradjena greska");
            }
        }
    }
}
