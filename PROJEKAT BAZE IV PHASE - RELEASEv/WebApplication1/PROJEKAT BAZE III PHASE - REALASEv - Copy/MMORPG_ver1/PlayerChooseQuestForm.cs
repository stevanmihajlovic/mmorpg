﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class PlayerChooseQuestForm : Form
    {
        public IList<SoloZadatak> zadaci;
        public Igrac LogovanIgrac;
        public GameForm g;
        public IList<SoloZadatak> listazadatakanakojimaNIJEigrac;
        public PlayerChooseQuestForm()
        {
            
            InitializeComponent();
            
        }
        public PlayerChooseQuestForm(Igrac l,GameForm game)
        {
            InitializeComponent();
            LogovanIgrac = l;
            g = game;
            try
            {
                ISession s = DataLayer.GetSession();
                zadaci = (from a in s.Query<Entiteti.SoloZadatak>()
                          select a).ToList<SoloZadatak>();

                IList<int> IdzadatakanakojimaJEigrac = LogovanIgrac.Zadaci.Select(x => x.IdZadatak).ToList();

                listazadatakanakojimaNIJEigrac = zadaci.Where(x => !IdzadatakanakojimaJEigrac.Contains(x.IdZadatak)).ToList();

                if (listazadatakanakojimaNIJEigrac.Count > 0)
                {
                    foreach (SoloZadatak a in listazadatakanakojimaNIJEigrac)
                    {
                        listBox1.Items.Add(a.IdZadatak);
                    }
                }
                else
                    MessageBox.Show("Ne postoje zadaci");
                   
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }

        }

        private void buttonIzaberite_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Selektujte zadatak");
            else
            {
                try
                {

                    ISession s = DataLayer.GetSession();

                    
                    s.Lock(LogovanIgrac, LockMode.None);
                    s.Lock(listazadatakanakojimaNIJEigrac[listBox1.SelectedIndex], LockMode.None);

                    LogovanIgrac.Zadaci.Add(listazadatakanakojimaNIJEigrac[listBox1.SelectedIndex]);
                    listazadatakanakojimaNIJEigrac[listBox1.SelectedIndex].Solo.Add(LogovanIgrac);

                    g.listBoxZadaci.Items.Add(listazadatakanakojimaNIJEigrac[listBox1.SelectedIndex].IdZadatak);

                    s.SaveOrUpdate(LogovanIgrac);   //igrac cuva i zadatak
                    s.Flush();
                    s.Close();
                    MessageBox.Show("Bravo, izabrali ste zadatak");
                    this.Close();
                }
                catch (Exception ec)
                {
                    if (ec.Message.Contains("already") || ec.InnerException.Message.Contains("insert"))
                        MessageBox.Show("Igrac je vec na zadatku");
                    else
                        MessageBox.Show(ec.Message);
                }
            }
        }
    }
}
