﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;

namespace MMORPG_ver1
{
    public partial class InsertQuestForm : Form
    {
        public int idzadatkazaupdate = 0;

        public Entiteti.Zadatak q;

        public InsertQuestForm()
        {
            InitializeComponent();
        }
        public InsertQuestForm(int a)
        {
            InitializeComponent();
            idzadatkazaupdate = a;
            try
            {
                ISession s = DataLayer.GetSession();
                q = s.Get<Zadatak>(idzadatkazaupdate); //ZBOG PROXYA!
                if (q.GetType() == typeof(GrupniZadatak))
                {
                    radioButtonGrupni.Checked = true;
                    radioButtonSolo.Checked = false;
                }
                else
                {
                    radioButtonGrupni.Checked = false;
                    radioButtonSolo.Checked = true;
                }
                radioButtonGrupni.Visible = false;
                radioButtonSolo.Visible = false;
                label1.Visible = false;
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void buttonUbaciZadatak_Click(object sender, EventArgs e)
        {
            textBoxIskustvo.BackColor = Color.White;
            try
            {
                ISession s = DataLayer.GetSession();
                if (idzadatkazaupdate != 0)
                    s.Lock(q, LockMode.None);
                if (radioButtonSolo.Checked)
                {
                    if (idzadatkazaupdate == 0)
                        q = new Entiteti.SoloZadatak();
                    q.BonusIskustvo = Convert.ToInt32(textBoxIskustvo.Text);
                    q.VremePocetka = DateTime.Now;
                    s.SaveOrUpdate(q);
                }
                else
                {
                    if (idzadatkazaupdate == 0)
                        q = new Entiteti.GrupniZadatak();
                    q.BonusIskustvo = Convert.ToInt32(textBoxIskustvo.Text);
                    q.VremePocetka = DateTime.Now;
                    s.SaveOrUpdate(q);
                }
                s.Flush();
                s.Close();
                if (idzadatkazaupdate == 0)
                    MessageBox.Show("Uspešno ste ubacili zadatak u bazu");
                else
                    MessageBox.Show("Uspešno ste updatovali zadatak");
                this.Close();
            }
            catch (System.FormatException ec)
            {
                MessageBox.Show("Unesite za iskustvo broj od 0 do 100");
                textBoxIskustvo.BackColor = Color.IndianRed;
            }
            catch (Exception ec)
            {
                MessageBox.Show("Neobradjena greška");
            }
        }
    }
}
