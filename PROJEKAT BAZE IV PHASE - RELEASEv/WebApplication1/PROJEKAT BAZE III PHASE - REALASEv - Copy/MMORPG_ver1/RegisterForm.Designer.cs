﻿namespace MMORPG_ver1
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RegistrujSe = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxNick = new System.Windows.Forms.TextBox();
            this.textBoxLozinka = new System.Windows.Forms.TextBox();
            this.textBoxIme = new System.Windows.Forms.TextBox();
            this.textBoxPrezime = new System.Windows.Forms.TextBox();
            this.textBoxStarost = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.radioButtonMuski = new System.Windows.Forms.RadioButton();
            this.radioButtonZenski = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // RegistrujSe
            // 
            this.RegistrujSe.Location = new System.Drawing.Point(200, 225);
            this.RegistrujSe.Name = "RegistrujSe";
            this.RegistrujSe.Size = new System.Drawing.Size(75, 23);
            this.RegistrujSe.TabIndex = 0;
            this.RegistrujSe.Text = "RegistrujSe";
            this.RegistrujSe.UseVisualStyleBackColor = true;
            this.RegistrujSe.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 49);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(120, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Unesite željeni Nick:* ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 72);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(136, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Unesite željenu Lozinku:*";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(13, 9);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(94, 25);
            this.label3.TabIndex = 3;
            this.label3.Text = "Podaci o Igraču";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 95);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5);
            this.label4.Size = new System.Drawing.Size(106, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "Unesite Vaše Ime: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 118);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5);
            this.label5.Size = new System.Drawing.Size(126, 23);
            this.label5.TabIndex = 5;
            this.label5.Text = "Unesite Vaše Prezime: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 141);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(5);
            this.label6.Size = new System.Drawing.Size(107, 23);
            this.label6.TabIndex = 6;
            this.label6.Text = "Unesite Pol (M/Ž): ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 164);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(5);
            this.label7.Size = new System.Drawing.Size(95, 23);
            this.label7.TabIndex = 7;
            this.label7.Text = "Unesite Starost: ";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Vilenjak",
            "Demon",
            "Ork",
            "Patuljak",
            "Čovek"});
            this.comboBox1.Location = new System.Drawing.Point(154, 187);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 187);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(5);
            this.label8.Size = new System.Drawing.Size(117, 23);
            this.label8.TabIndex = 9;
            this.label8.Text = "Izaberite Rasu Lika:  ";
            // 
            // textBoxNick
            // 
            this.textBoxNick.Location = new System.Drawing.Point(154, 52);
            this.textBoxNick.Name = "textBoxNick";
            this.textBoxNick.Size = new System.Drawing.Size(121, 20);
            this.textBoxNick.TabIndex = 10;
            this.textBoxNick.TextChanged += new System.EventHandler(this.textBoxNick_TextChanged);
            // 
            // textBoxLozinka
            // 
            this.textBoxLozinka.Location = new System.Drawing.Point(154, 75);
            this.textBoxLozinka.Name = "textBoxLozinka";
            this.textBoxLozinka.Size = new System.Drawing.Size(121, 20);
            this.textBoxLozinka.TabIndex = 11;
            // 
            // textBoxIme
            // 
            this.textBoxIme.Location = new System.Drawing.Point(154, 98);
            this.textBoxIme.Name = "textBoxIme";
            this.textBoxIme.Size = new System.Drawing.Size(121, 20);
            this.textBoxIme.TabIndex = 12;
            // 
            // textBoxPrezime
            // 
            this.textBoxPrezime.Location = new System.Drawing.Point(154, 121);
            this.textBoxPrezime.Name = "textBoxPrezime";
            this.textBoxPrezime.Size = new System.Drawing.Size(121, 20);
            this.textBoxPrezime.TabIndex = 13;
            // 
            // textBoxStarost
            // 
            this.textBoxStarost.Location = new System.Drawing.Point(154, 164);
            this.textBoxStarost.Name = "textBoxStarost";
            this.textBoxStarost.Size = new System.Drawing.Size(121, 20);
            this.textBoxStarost.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 248);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(5);
            this.label9.Size = new System.Drawing.Size(214, 23);
            this.label9.TabIndex = 16;
            this.label9.Text = "Polja označena sa * moraju biti popunjena";
            // 
            // radioButtonMuski
            // 
            this.radioButtonMuski.AutoSize = true;
            this.radioButtonMuski.Checked = true;
            this.radioButtonMuski.Location = new System.Drawing.Point(157, 144);
            this.radioButtonMuski.Name = "radioButtonMuski";
            this.radioButtonMuski.Size = new System.Drawing.Size(53, 17);
            this.radioButtonMuski.TabIndex = 18;
            this.radioButtonMuski.TabStop = true;
            this.radioButtonMuski.Text = "Muški";
            this.radioButtonMuski.UseVisualStyleBackColor = true;
            // 
            // radioButtonZenski
            // 
            this.radioButtonZenski.AutoSize = true;
            this.radioButtonZenski.Location = new System.Drawing.Point(220, 144);
            this.radioButtonZenski.Name = "radioButtonZenski";
            this.radioButtonZenski.Size = new System.Drawing.Size(57, 17);
            this.radioButtonZenski.TabIndex = 19;
            this.radioButtonZenski.Text = "Ženski";
            this.radioButtonZenski.UseVisualStyleBackColor = true;
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(298, 280);
            this.Controls.Add(this.radioButtonZenski);
            this.Controls.Add(this.radioButtonMuski);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxStarost);
            this.Controls.Add(this.textBoxPrezime);
            this.Controls.Add(this.textBoxIme);
            this.Controls.Add(this.textBoxLozinka);
            this.Controls.Add(this.textBoxNick);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RegistrujSe);
            this.Name = "RegisterForm";
            this.Text = "RegisterForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button RegistrujSe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxNick;
        private System.Windows.Forms.TextBox textBoxLozinka;
        private System.Windows.Forms.TextBox textBoxIme;
        private System.Windows.Forms.TextBox textBoxPrezime;
        private System.Windows.Forms.TextBox textBoxStarost;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton radioButtonMuski;
        private System.Windows.Forms.RadioButton radioButtonZenski;
    }
}