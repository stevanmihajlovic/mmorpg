﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class LootItemForm : Form
    {
        public Igrac LogovanIgrac;
        public GameForm g;
        public IList<Item> listaitemanakojeNEMAigrac;
        public IList<Item> sviitemi;
        public LootItemForm()
        {
            InitializeComponent();
        }
        public LootItemForm(Igrac l,GameForm game)
        {
            InitializeComponent();
            LogovanIgrac = l;
            g = game;
            
            try
            {
                ISession s = DataLayer.GetSession();

                String rasaigraca="";

                IList<int> dostupnerase;

                sviitemi = (from a in s.Query<Entiteti.Item>()
                            select a).ToList<Item>();

                Lik LogovanLik = s.Get<Lik>(LogovanIgrac.IdLika.IdLik);

                if (LogovanLik.GetType() == typeof(LikDemon))
                    rasaigraca = "Demon";
                if (LogovanLik.GetType() == typeof(LikVilenjak))
                    rasaigraca = "Vilenjak";
                if (LogovanLik.GetType() == typeof(LikOrk))
                    rasaigraca = "Ork";
                if (LogovanLik.GetType() == typeof(LikPatuljak))
                    rasaigraca = "Patuljak";
                if (LogovanLik.GetType() == typeof(LikCovek))
                    rasaigraca = "Covek";

                dostupnerase = (from r in s.Query<Entiteti.Rasa>()
                                where r.TipRase.Equals(rasaigraca)
                                select r.IdItema.IdItem).ToList();

                IList<int> IdzitemakojeIMAigrac = LogovanIgrac.Itemi.Select(x => x.IdItem).ToList();
                //niz posedovanih itema + nedostupni itemi cine niz IDa nedostupnih itema

                foreach (Item i in sviitemi)
                    if (i.GetType().BaseType == typeof(BonusItem))
                    {
                        if (!dostupnerase.Contains(i.IdItem))
                            IdzitemakojeIMAigrac.Add(i.IdItem);
                    }
      
                listaitemanakojeNEMAigrac = sviitemi.Where(x => !IdzitemakojeIMAigrac.Contains(x.IdItem)).ToList();

                if (listaitemanakojeNEMAigrac.Count > 0)
                {
                    foreach (Item a in listaitemanakojeNEMAigrac)
                    {
                        listBox1.Items.Add(a.IdItem);
                    }
                }
                else
                    MessageBox.Show("Nema itema");
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void buttonIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonPokupiteItem_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Selektujte item");
            else
            {
                try
                {
                    ISession s = DataLayer.GetSession();

                    s.Lock(listaitemanakojeNEMAigrac[listBox1.SelectedIndex], LockMode.None);
                    s.Lock(LogovanIgrac, LockMode.None);

                    listaitemanakojeNEMAigrac[listBox1.SelectedIndex].Igraci.Add(LogovanIgrac);
                    LogovanIgrac.Itemi.Add(listaitemanakojeNEMAigrac[listBox1.SelectedIndex]);

                    g.listBoxItemi.Items.Add(listaitemanakojeNEMAigrac[listBox1.SelectedIndex].IdItem);

                    
                    s.SaveOrUpdate(LogovanIgrac);   //igrac ce da povuce i item zbog inverse

                    s.Flush();
                    s.Close();
                    MessageBox.Show("Igrac je pokupio item");
                    this.Close();
                }
                catch (Exception ec)
                {
                    if (ec.Message.Contains("already") || ec.InnerException.Message.Contains("insert"))
                        MessageBox.Show("Igrac vec poseduje item");
                    else
                        MessageBox.Show(ec.Message);
                }
            }
        }

    }
}
