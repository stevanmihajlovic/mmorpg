﻿namespace MMORPG_ver1
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelIme = new System.Windows.Forms.Label();
            this.labelPrezime = new System.Windows.Forms.Label();
            this.labelStarost = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelPol = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelZdravlje = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelZlato = new System.Windows.Forms.Label();
            this.labelIskustvo = new System.Windows.Forms.Label();
            this.labelRasa = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelZamor = new System.Windows.Forms.Label();
            this.labelSpecijalnost = new System.Windows.Forms.Label();
            this.labelBonusSpecijalnosti = new System.Windows.Forms.Label();
            this.KreairajteAlijansu = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonPridruziteSeAlijansi = new System.Windows.Forms.Button();
            this.buttonNapustiteAlijansu = new System.Windows.Forms.Button();
            this.buttonRegrutujSegrta = new System.Windows.Forms.Button();
            this.buttonKupiPrevoznoSredstvo = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonPokupiteItem = new System.Windows.Forms.Button();
            this.buttonIzmeniPodatke = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.labelAlijansa = new System.Windows.Forms.Label();
            this.listBoxSegrti = new System.Windows.Forms.ListBox();
            this.buttonOtpustiSegrta = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.listBoxPrevoznaSredstva = new System.Windows.Forms.ListBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.listBoxZadaci = new System.Windows.Forms.ListBox();
            this.label11 = new System.Windows.Forms.Label();
            this.buttonZavrsiteZadatak = new System.Windows.Forms.Button();
            this.listBoxItemi = new System.Windows.Forms.ListBox();
            this.label16 = new System.Windows.Forms.Label();
            this.buttonBaciteItem = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(97, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Podaci o Igraču:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(37, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ime:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 71);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(57, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Prezime:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 94);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5);
            this.label5.Size = new System.Drawing.Size(53, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Starost:";
            // 
            // labelIme
            // 
            this.labelIme.AutoSize = true;
            this.labelIme.Location = new System.Drawing.Point(107, 48);
            this.labelIme.Name = "labelIme";
            this.labelIme.Padding = new System.Windows.Forms.Padding(5);
            this.labelIme.Size = new System.Drawing.Size(45, 23);
            this.labelIme.TabIndex = 5;
            this.labelIme.Text = "label4";
            // 
            // labelPrezime
            // 
            this.labelPrezime.AutoSize = true;
            this.labelPrezime.Location = new System.Drawing.Point(107, 71);
            this.labelPrezime.Name = "labelPrezime";
            this.labelPrezime.Padding = new System.Windows.Forms.Padding(5);
            this.labelPrezime.Size = new System.Drawing.Size(45, 23);
            this.labelPrezime.TabIndex = 6;
            this.labelPrezime.Text = "label6";
            // 
            // labelStarost
            // 
            this.labelStarost.AutoSize = true;
            this.labelStarost.Location = new System.Drawing.Point(107, 94);
            this.labelStarost.Name = "labelStarost";
            this.labelStarost.Padding = new System.Windows.Forms.Padding(5);
            this.labelStarost.Size = new System.Drawing.Size(45, 23);
            this.labelStarost.TabIndex = 7;
            this.labelStarost.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 117);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(5);
            this.label8.Size = new System.Drawing.Size(35, 23);
            this.label8.TabIndex = 8;
            this.label8.Text = "Pol:";
            // 
            // labelPol
            // 
            this.labelPol.AutoSize = true;
            this.labelPol.Location = new System.Drawing.Point(107, 117);
            this.labelPol.Name = "labelPol";
            this.labelPol.Padding = new System.Windows.Forms.Padding(5);
            this.labelPol.Size = new System.Drawing.Size(45, 23);
            this.labelPol.TabIndex = 9;
            this.labelPol.Text = "label9";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(241, 9);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5);
            this.label4.Size = new System.Drawing.Size(87, 25);
            this.label4.TabIndex = 10;
            this.label4.Text = "Podaci o Liku:";
            // 
            // labelZdravlje
            // 
            this.labelZdravlje.AutoSize = true;
            this.labelZdravlje.Location = new System.Drawing.Point(337, 125);
            this.labelZdravlje.Name = "labelZdravlje";
            this.labelZdravlje.Padding = new System.Windows.Forms.Padding(5);
            this.labelZdravlje.Size = new System.Drawing.Size(45, 23);
            this.labelZdravlje.TabIndex = 18;
            this.labelZdravlje.Text = "label9";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(243, 125);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(5);
            this.label7.Size = new System.Drawing.Size(58, 23);
            this.label7.TabIndex = 17;
            this.label7.Text = "Zdravlje:";
            // 
            // labelZlato
            // 
            this.labelZlato.AutoSize = true;
            this.labelZlato.Location = new System.Drawing.Point(337, 102);
            this.labelZlato.Name = "labelZlato";
            this.labelZlato.Padding = new System.Windows.Forms.Padding(5);
            this.labelZlato.Size = new System.Drawing.Size(45, 23);
            this.labelZlato.TabIndex = 16;
            this.labelZlato.Text = "label7";
            // 
            // labelIskustvo
            // 
            this.labelIskustvo.AutoSize = true;
            this.labelIskustvo.Location = new System.Drawing.Point(337, 79);
            this.labelIskustvo.Name = "labelIskustvo";
            this.labelIskustvo.Padding = new System.Windows.Forms.Padding(5);
            this.labelIskustvo.Size = new System.Drawing.Size(45, 23);
            this.labelIskustvo.TabIndex = 15;
            this.labelIskustvo.Text = "label6";
            // 
            // labelRasa
            // 
            this.labelRasa.AutoSize = true;
            this.labelRasa.Location = new System.Drawing.Point(337, 56);
            this.labelRasa.Name = "labelRasa";
            this.labelRasa.Padding = new System.Windows.Forms.Padding(5);
            this.labelRasa.Size = new System.Drawing.Size(45, 23);
            this.labelRasa.TabIndex = 14;
            this.labelRasa.Text = "label4";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(242, 102);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(5);
            this.label12.Size = new System.Drawing.Size(44, 23);
            this.label12.TabIndex = 13;
            this.label12.Text = "Zlato:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(242, 79);
            this.label13.Name = "label13";
            this.label13.Padding = new System.Windows.Forms.Padding(5);
            this.label13.Size = new System.Drawing.Size(60, 23);
            this.label13.TabIndex = 12;
            this.label13.Text = "Iskustvo:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(242, 56);
            this.label14.Name = "label14";
            this.label14.Padding = new System.Windows.Forms.Padding(5);
            this.label14.Size = new System.Drawing.Size(45, 23);
            this.label14.TabIndex = 11;
            this.label14.Text = "Rasa:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(243, 148);
            this.label15.Name = "label15";
            this.label15.Padding = new System.Windows.Forms.Padding(5);
            this.label15.Size = new System.Drawing.Size(50, 23);
            this.label15.TabIndex = 19;
            this.label15.Text = "Zamor:";
            // 
            // labelZamor
            // 
            this.labelZamor.AutoSize = true;
            this.labelZamor.Location = new System.Drawing.Point(337, 148);
            this.labelZamor.Name = "labelZamor";
            this.labelZamor.Padding = new System.Windows.Forms.Padding(5);
            this.labelZamor.Size = new System.Drawing.Size(45, 23);
            this.labelZamor.TabIndex = 20;
            this.labelZamor.Text = "label9";
            // 
            // labelSpecijalnost
            // 
            this.labelSpecijalnost.AutoSize = true;
            this.labelSpecijalnost.Location = new System.Drawing.Point(243, 171);
            this.labelSpecijalnost.Name = "labelSpecijalnost";
            this.labelSpecijalnost.Padding = new System.Windows.Forms.Padding(5);
            this.labelSpecijalnost.Size = new System.Drawing.Size(51, 23);
            this.labelSpecijalnost.TabIndex = 21;
            this.labelSpecijalnost.Text = "label11";
            // 
            // labelBonusSpecijalnosti
            // 
            this.labelBonusSpecijalnosti.AutoSize = true;
            this.labelBonusSpecijalnosti.Location = new System.Drawing.Point(337, 171);
            this.labelBonusSpecijalnosti.Name = "labelBonusSpecijalnosti";
            this.labelBonusSpecijalnosti.Padding = new System.Windows.Forms.Padding(5);
            this.labelBonusSpecijalnosti.Size = new System.Drawing.Size(51, 23);
            this.labelBonusSpecijalnosti.TabIndex = 22;
            this.labelBonusSpecijalnosti.Text = "label10";
            // 
            // KreairajteAlijansu
            // 
            this.KreairajteAlijansu.Location = new System.Drawing.Point(451, 56);
            this.KreairajteAlijansu.Name = "KreairajteAlijansu";
            this.KreairajteAlijansu.Size = new System.Drawing.Size(105, 23);
            this.KreairajteAlijansu.TabIndex = 23;
            this.KreairajteAlijansu.Text = "Kreirajte Alijansu";
            this.KreairajteAlijansu.UseVisualStyleBackColor = true;
            this.KreairajteAlijansu.Click += new System.EventHandler(this.KreairajteAlijansu_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(666, 544);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "LOGOUT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttonPridruziteSeAlijansi
            // 
            this.buttonPridruziteSeAlijansi.Location = new System.Drawing.Point(631, 56);
            this.buttonPridruziteSeAlijansi.Name = "buttonPridruziteSeAlijansi";
            this.buttonPridruziteSeAlijansi.Size = new System.Drawing.Size(110, 23);
            this.buttonPridruziteSeAlijansi.TabIndex = 25;
            this.buttonPridruziteSeAlijansi.Text = "Pridružite se Alijansi";
            this.buttonPridruziteSeAlijansi.UseVisualStyleBackColor = true;
            this.buttonPridruziteSeAlijansi.Click += new System.EventHandler(this.buttonPridruziteSeAlijansi_Click);
            // 
            // buttonNapustiteAlijansu
            // 
            this.buttonNapustiteAlijansu.Location = new System.Drawing.Point(631, 89);
            this.buttonNapustiteAlijansu.Name = "buttonNapustiteAlijansu";
            this.buttonNapustiteAlijansu.Size = new System.Drawing.Size(110, 23);
            this.buttonNapustiteAlijansu.TabIndex = 26;
            this.buttonNapustiteAlijansu.Text = "Napustite Alijansu";
            this.buttonNapustiteAlijansu.UseVisualStyleBackColor = true;
            this.buttonNapustiteAlijansu.Click += new System.EventHandler(this.buttonNapustiteAlijansu_Click);
            // 
            // buttonRegrutujSegrta
            // 
            this.buttonRegrutujSegrta.Location = new System.Drawing.Point(241, 544);
            this.buttonRegrutujSegrta.Name = "buttonRegrutujSegrta";
            this.buttonRegrutujSegrta.Size = new System.Drawing.Size(93, 23);
            this.buttonRegrutujSegrta.TabIndex = 27;
            this.buttonRegrutujSegrta.Text = "Regrutuj Šegrta";
            this.buttonRegrutujSegrta.UseVisualStyleBackColor = true;
            this.buttonRegrutujSegrta.Click += new System.EventHandler(this.buttonRegrutujSegrta_Click);
            // 
            // buttonKupiPrevoznoSredstvo
            // 
            this.buttonKupiPrevoznoSredstvo.Location = new System.Drawing.Point(240, 352);
            this.buttonKupiPrevoznoSredstvo.Name = "buttonKupiPrevoznoSredstvo";
            this.buttonKupiPrevoznoSredstvo.Size = new System.Drawing.Size(92, 48);
            this.buttonKupiPrevoznoSredstvo.TabIndex = 28;
            this.buttonKupiPrevoznoSredstvo.Text = "Kupite Prevozno Sredstvo";
            this.buttonKupiPrevoznoSredstvo.UseVisualStyleBackColor = true;
            this.buttonKupiPrevoznoSredstvo.Click += new System.EventHandler(this.buttonKupiPrevoznoSredstvo_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(17, 352);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 48);
            this.button2.TabIndex = 29;
            this.button2.Text = "Izaberite Novi Zadatak";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonPokupiteItem
            // 
            this.buttonPokupiteItem.Location = new System.Drawing.Point(17, 544);
            this.buttonPokupiteItem.Name = "buttonPokupiteItem";
            this.buttonPokupiteItem.Size = new System.Drawing.Size(92, 23);
            this.buttonPokupiteItem.TabIndex = 30;
            this.buttonPokupiteItem.Text = "Pokupite Item";
            this.buttonPokupiteItem.UseVisualStyleBackColor = true;
            this.buttonPokupiteItem.Click += new System.EventHandler(this.buttonPokupiteItem_Click);
            // 
            // buttonIzmeniPodatke
            // 
            this.buttonIzmeniPodatke.Location = new System.Drawing.Point(15, 148);
            this.buttonIzmeniPodatke.Name = "buttonIzmeniPodatke";
            this.buttonIzmeniPodatke.Size = new System.Drawing.Size(142, 23);
            this.buttonIzmeniPodatke.TabIndex = 31;
            this.buttonIzmeniPodatke.Text = "Izmeni podatke";
            this.buttonIzmeniPodatke.UseVisualStyleBackColor = true;
            this.buttonIzmeniPodatke.Click += new System.EventHandler(this.buttonIzmeniPodatke_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(451, 9);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(5);
            this.label6.Size = new System.Drawing.Size(58, 25);
            this.label6.TabIndex = 32;
            this.label6.Text = "Alijansa:";
            // 
            // labelAlijansa
            // 
            this.labelAlijansa.AutoSize = true;
            this.labelAlijansa.Location = new System.Drawing.Point(448, 89);
            this.labelAlijansa.Name = "labelAlijansa";
            this.labelAlijansa.Padding = new System.Windows.Forms.Padding(5);
            this.labelAlijansa.Size = new System.Drawing.Size(51, 23);
            this.labelAlijansa.TabIndex = 33;
            this.labelAlijansa.Text = "label11";
            // 
            // listBoxSegrti
            // 
            this.listBoxSegrti.FormattingEnabled = true;
            this.listBoxSegrti.Location = new System.Drawing.Point(241, 434);
            this.listBoxSegrti.Name = "listBoxSegrti";
            this.listBoxSegrti.Size = new System.Drawing.Size(198, 95);
            this.listBoxSegrti.TabIndex = 34;
            // 
            // buttonOtpustiSegrta
            // 
            this.buttonOtpustiSegrta.Location = new System.Drawing.Point(346, 544);
            this.buttonOtpustiSegrta.Name = "buttonOtpustiSegrta";
            this.buttonOtpustiSegrta.Size = new System.Drawing.Size(93, 23);
            this.buttonOtpustiSegrta.TabIndex = 35;
            this.buttonOtpustiSegrta.Text = "Otpusti Šegrta";
            this.buttonOtpustiSegrta.UseVisualStyleBackColor = true;
            this.buttonOtpustiSegrta.Click += new System.EventHandler(this.buttonOtpustiSegrta_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(239, 408);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(5);
            this.label9.Size = new System.Drawing.Size(73, 23);
            this.label9.TabIndex = 36;
            this.label9.Text = "Vaši Šegrti: ";
            // 
            // listBoxPrevoznaSredstva
            // 
            this.listBoxPrevoznaSredstva.FormattingEnabled = true;
            this.listBoxPrevoznaSredstva.Location = new System.Drawing.Point(241, 242);
            this.listBoxPrevoznaSredstva.Name = "listBoxPrevoznaSredstva";
            this.listBoxPrevoznaSredstva.Size = new System.Drawing.Size(198, 95);
            this.listBoxPrevoznaSredstva.TabIndex = 37;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(237, 216);
            this.label10.Name = "label10";
            this.label10.Padding = new System.Windows.Forms.Padding(5);
            this.label10.Size = new System.Drawing.Size(138, 23);
            this.label10.TabIndex = 38;
            this.label10.Text = "Vaša Prevozna sredstva: ";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(347, 352);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(92, 48);
            this.button3.TabIndex = 39;
            this.button3.Text = "Otpustite Prevozno Sredstvo";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // listBoxZadaci
            // 
            this.listBoxZadaci.FormattingEnabled = true;
            this.listBoxZadaci.Location = new System.Drawing.Point(17, 242);
            this.listBoxZadaci.Name = "listBoxZadaci";
            this.listBoxZadaci.Size = new System.Drawing.Size(198, 95);
            this.listBoxZadaci.TabIndex = 40;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 216);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(5);
            this.label11.Size = new System.Drawing.Size(121, 23);
            this.label11.TabIndex = 41;
            this.label11.Text = "Zadaci na kojima ste: ";
            // 
            // buttonZavrsiteZadatak
            // 
            this.buttonZavrsiteZadatak.Location = new System.Drawing.Point(123, 353);
            this.buttonZavrsiteZadatak.Name = "buttonZavrsiteZadatak";
            this.buttonZavrsiteZadatak.Size = new System.Drawing.Size(92, 48);
            this.buttonZavrsiteZadatak.TabIndex = 42;
            this.buttonZavrsiteZadatak.Text = "Završite Započeti Zadatak";
            this.buttonZavrsiteZadatak.UseVisualStyleBackColor = true;
            this.buttonZavrsiteZadatak.Click += new System.EventHandler(this.buttonZavrsiteZadatak_Click);
            // 
            // listBoxItemi
            // 
            this.listBoxItemi.FormattingEnabled = true;
            this.listBoxItemi.Location = new System.Drawing.Point(17, 434);
            this.listBoxItemi.Name = "listBoxItemi";
            this.listBoxItemi.Size = new System.Drawing.Size(198, 95);
            this.listBoxItemi.TabIndex = 43;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(14, 408);
            this.label16.Name = "label16";
            this.label16.Padding = new System.Windows.Forms.Padding(5);
            this.label16.Size = new System.Drawing.Size(68, 23);
            this.label16.TabIndex = 44;
            this.label16.Text = "Vaši Itemi: ";
            // 
            // buttonBaciteItem
            // 
            this.buttonBaciteItem.Location = new System.Drawing.Point(123, 544);
            this.buttonBaciteItem.Name = "buttonBaciteItem";
            this.buttonBaciteItem.Size = new System.Drawing.Size(92, 23);
            this.buttonBaciteItem.TabIndex = 45;
            this.buttonBaciteItem.Text = "Bacite Item";
            this.buttonBaciteItem.UseVisualStyleBackColor = true;
            this.buttonBaciteItem.Click += new System.EventHandler(this.buttonBaciteItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MMORPG_ver1.Properties.Resources.Zaramin;
            this.pictureBox1.Location = new System.Drawing.Point(467, 143);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(255, 386);
            this.pictureBox1.TabIndex = 46;
            this.pictureBox1.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(202)))), ((int)(((byte)(177)))));
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(515, 443);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(165, 60);
            this.label17.TabIndex = 47;
            this.label17.Text = "Try out our new game \r\n\"ZARAMINS\" \r\nby \"AS/DS\" team!";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 608);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonBaciteItem);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.listBoxItemi);
            this.Controls.Add(this.buttonZavrsiteZadatak);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.listBoxZadaci);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.listBoxPrevoznaSredstva);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.buttonOtpustiSegrta);
            this.Controls.Add(this.listBoxSegrti);
            this.Controls.Add(this.labelAlijansa);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonIzmeniPodatke);
            this.Controls.Add(this.buttonPokupiteItem);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buttonKupiPrevoznoSredstvo);
            this.Controls.Add(this.buttonRegrutujSegrta);
            this.Controls.Add(this.buttonNapustiteAlijansu);
            this.Controls.Add(this.buttonPridruziteSeAlijansi);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.KreairajteAlijansu);
            this.Controls.Add(this.labelBonusSpecijalnosti);
            this.Controls.Add(this.labelSpecijalnost);
            this.Controls.Add(this.labelZamor);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.labelZdravlje);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labelZlato);
            this.Controls.Add(this.labelIskustvo);
            this.Controls.Add(this.labelRasa);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelPol);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.labelStarost);
            this.Controls.Add(this.labelPrezime);
            this.Controls.Add(this.labelIme);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "GameForm";
            this.Text = "GameForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelIme;
        private System.Windows.Forms.Label labelPrezime;
        private System.Windows.Forms.Label labelStarost;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelPol;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelZdravlje;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelZlato;
        private System.Windows.Forms.Label labelIskustvo;
        private System.Windows.Forms.Label labelRasa;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelZamor;
        private System.Windows.Forms.Label labelSpecijalnost;
        private System.Windows.Forms.Label labelBonusSpecijalnosti;
        public System.Windows.Forms.Button KreairajteAlijansu;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button buttonPridruziteSeAlijansi;
        public System.Windows.Forms.Button buttonNapustiteAlijansu;
        private System.Windows.Forms.Button buttonRegrutujSegrta;
        private System.Windows.Forms.Button buttonKupiPrevoznoSredstvo;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonPokupiteItem;
        private System.Windows.Forms.Button buttonIzmeniPodatke;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label labelAlijansa;
        public System.Windows.Forms.ListBox listBoxSegrti;
        private System.Windows.Forms.Button buttonOtpustiSegrta;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.ListBox listBoxPrevoznaSredstva;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.ListBox listBoxZadaci;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonZavrsiteZadatak;
        public System.Windows.Forms.ListBox listBoxItemi;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button buttonBaciteItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label17;

    }
}