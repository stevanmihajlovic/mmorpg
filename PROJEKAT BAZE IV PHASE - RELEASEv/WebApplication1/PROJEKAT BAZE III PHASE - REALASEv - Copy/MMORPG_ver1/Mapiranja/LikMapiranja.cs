﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMORPG_ver1.Entiteti;
using FluentNHibernate.Mapping;

namespace MMORPG_ver1.Mapiranja
{
    class LikMapiranja : ClassMap<Lik>
    {
        public LikMapiranja()
        {
            Table("LIK");                                                //mapiranje tabele

            DiscriminateSubClassesOnColumn("RASA");                      //mapiranje podklasa za RASA

            Id(x => x.IdLik, "ID_LIK").GeneratedBy.TriggerIdentity();    //mapiranje primaryID
            
            Map(x => x.StepenZamora, "ZAMOR");                           //mapiranje atributa
            Map(x => x.Iskustvo, "ISKUSTVO");   
            Map(x => x.Zdravlje, "ZDRAVLJE");
            Map(x => x.KolicinaZlata, "ZLATO");
            Map(x => x.Ime, "IME");
            Map(x => x.BonusSkrivanjaSegrta, "SEGRTOV_BONUS_U_SKRIVANJU"); 
            

            HasOne(x => x.IdIgraca).PropertyRef(x => x.IdLika);                         //mapiranje 1:1 za igrac-lik vezu, 
            //u tabeli lik postoji kolona preko koje on Referencira Lika, medjutim kako lik nema tu kolonu on ce potraziti u IdLika kod Igraca

            HasMany(x => x.Segrti).KeyColumn("ID_GOSPODARA").LazyLoad().Cascade.All();     //mapiranje 1:N rekurzivne veze za je segrt
            //dozvolicemo samo brisanje igraca, on ce da povuce brisanje lika a lik ce obrisati i sve segrte
            
            References(x => x.GospodarSegrta).Column("ID_GOSPODARA").LazyLoad();           //PROVERITI STA KOGA REFERENCIRA
            //istovremeno imamo preko iste kolone HasMany vezu i referencu ka nekom Liku, kod Likova je 0 a kod segrta <>0
        }

    }
    class LikVilenjakMapiranja : SubclassMap<LikVilenjak>   
    {
        public LikVilenjakMapiranja()
        {
            DiscriminatorValue("Vilenjak");
            Map(x => x.Energija, "ENERGIJA");
        }
        
    }
    class LikDemonMapiranja : SubclassMap<LikDemon>
    {
        public LikDemonMapiranja()
        {
            DiscriminatorValue("Demon");
            Map(x => x.Energija, "ENERGIJA");
        }

    }
    class LikOrkMapiranja : SubclassMap<LikOrk>
    {
        public LikOrkMapiranja()
        {
            DiscriminatorValue("Ork");
            Map(x => x.SpecijalizacijaOruzja, "SPECIJALIZACIJA_ORUZJA");
        }

    }
    class LikPatuljakMapiranja : SubclassMap<LikPatuljak>
    {
        public LikPatuljakMapiranja()
        {
            DiscriminatorValue("Patuljak");
            Map(x => x.SpecijalizacijaOruzja, "SPECIJALIZACIJA_ORUZJA");
        }

    }
    class LikCovekMapiranja : SubclassMap<LikCovek>
    {
        public LikCovekMapiranja()
        {
            DiscriminatorValue("Covek");
            Map(x => x.BonusSkrivanja, "BONUS_U_SKRIVANJU");
        }

    }
}
