﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMORPG_ver1.Entiteti;
using FluentNHibernate.Mapping;

namespace MMORPG_ver1.Mapiranja
{
    class PrevoznoSredstvoMapiranja : ClassMap<PrevoznoSredstvo>
    {
        public PrevoznoSredstvoMapiranja()
        {
            Table("PREVOZNO_SREDSTVO");                                                         //mapiranje tabele

            DiscriminateSubClassesOnColumn("TIP_VOZILA");                                       //mapiranje podklasa za TIP_VOZILA

            Id(x => x.IdPrevoznoSredstvo, "ID_PREVOZNO_SREDSTVO").GeneratedBy.TriggerIdentity();//mapiranje primaryID
            
            Map(x => x.Naziv, "NAZIV");     //mapiranje atributa
            Map(x => x.Brzina, "BRZINA");   //mapiranje atributa
            Map(x => x.Cena, "CENA");

            References(x => x.PripadaIgracu).Column("ID_IGRACA").LazyLoad();    //mapiranje 1:N za igrac-prevoznosredstvo vezu
            
        }
    }
    class PrevoznoSredstvoZmajMapiranja : SubclassMap<PrevoznoSredstvoZmaj>
    {
        public PrevoznoSredstvoZmajMapiranja()
        {
            DiscriminatorValue("Zmaj");
        }
    }

    class PrevoznoSredstvoZivotinjaMapiranja : SubclassMap<PrevoznoSredstvoZivotinja>
    {
        public PrevoznoSredstvoZivotinjaMapiranja()
        {
            DiscriminatorValue("Zivotinja");
        }
    }

    class PrevoznoSredstvoMehanickiMapiranja : SubclassMap<PrevoznoSredstvoMehanicki>
    {
        public PrevoznoSredstvoMehanickiMapiranja()
        {
            DiscriminatorValue("Mehanicki");
        }
    }

    
}
