﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMORPG_ver1.Entiteti;
using FluentNHibernate.Mapping;

namespace MMORPG_ver1.Mapiranja
{
    class SesijaMapiranja : ClassMap<Sesija>
    {
        public SesijaMapiranja()
        {
            Table("SESIJA");                                                //mapiranje tabele

            Id(x => x.IdSesija, "ID_SESIJA").GeneratedBy.TriggerIdentity(); //mapiranje primaryID

            Map(x => x.VremePocetka, "VREME_POCETKA");                      //mapiranje atributa
            Map(x => x.VremeKraja, "VREME_KRAJA");   
            Map(x => x.Iskustvo, "ISKUSTVO");
            Map(x => x.Zlato, "ZLATO");

            References(x => x.VezanaZaIgraca).Column("ID_IGRACA").LazyLoad(); //mapiranje 1:N za igrac-sesija vezu
        }
    }
}
