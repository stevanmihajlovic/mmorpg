﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMORPG_ver1.Entiteti;
using FluentNHibernate.Mapping;

namespace MMORPG_ver1.Mapiranja
{
    class AlijansaMapiranja : ClassMap<Alijansa>
    {
        public AlijansaMapiranja()
        {
            Table("ALIJANSA");                                                     //Mapiranje tabele
            
            Id(x => x.IdAlijansa, "ID_ALIJANSA").GeneratedBy.TriggerIdentity();    //mapiranje primaryID

            Map(x => x.Naziv, "NAZIV");                                            //mapiranje svojstava.
            Map(x => x.Min, "MIN");
            Map(x => x.Max, "MAX");
            Map(x => x.BonusIskustvo, "BONUS_ISKUSTVO");
            Map(x => x.BonusZdravlje, "BONUS_ZDRAVLJE");



            HasMany(x => x.Igraci).KeyColumn("ID_ALIJANSE").LazyLoad().Cascade.SaveUpdate(); //mapiranje 1:N veze alijansa-igraci
            //kao sto u igracu imamo za prevozna sredstva tako i ovde, brisanjem alijanse ne brisemo igrace
            //vec samo nulliramo njihov pointer na alijansu
            
            HasManyToMany(x => x.Zadaci)                                                        //mapiranje M:N veze alijanse-zadaci
                .Table("GRUPNI_ZADATAK")
                .ParentKeyColumn("ID_ALIJANSE")
                .ChildKeyColumn("ID_ZADATKA")
                .Cascade.SaveUpdate();  //isto kao i kod igraca radimo

            HasManyToMany(x => x.USavezuSa)                                                     //mapiranje M:N rekurzivne veze SAVEZ
                .Table("SAVEZ")                                                                 //mora dve sa obe strane
                .ParentKeyColumn("ID_ALIJANSE1")
                .ChildKeyColumn("ID_ALIJANSE2");
               // .Cascade.SaveUpdate();         //smisli sta ces sa ovim zbog saveza                
                //cascade smo izbacili jer je pravio ponekad problem prilikom dodavanja novih saveza jer pokusa
                // da sacuva neki savez koji je vec lockovan...
           
        }
    }
}
