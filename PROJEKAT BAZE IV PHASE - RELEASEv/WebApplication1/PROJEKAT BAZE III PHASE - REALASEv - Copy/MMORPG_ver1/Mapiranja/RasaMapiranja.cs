﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMORPG_ver1.Entiteti;
using FluentNHibernate.Mapping;

namespace MMORPG_ver1.Mapiranja
{
    class RasaMapiranja : ClassMap<Rasa>
    {
        public RasaMapiranja()
        {
            Table("RASA");
            Id(x => x.IdRasa, "ID_RASA").GeneratedBy.TriggerIdentity();
            Map(x => x.TipRase, "RASA");

            References(x => x.IdItema).Column("ID_ITEMA").LazyLoad();
        }
    }
}
