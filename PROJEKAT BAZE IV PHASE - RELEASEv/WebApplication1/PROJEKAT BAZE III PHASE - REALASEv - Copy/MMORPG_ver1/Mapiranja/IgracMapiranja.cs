﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMORPG_ver1.Entiteti;
using FluentNHibernate.Mapping;

namespace MMORPG_ver1.Mapiranja
{
    class IgracMapiranja : ClassMap<Igrac>
    {
        public IgracMapiranja()
        {
            Table("IGRAC");                                                 //mapiranje tabele

            Id(x => x.IdIgrac, "ID_IGRAC").GeneratedBy.TriggerIdentity();   //mapiranje primaryID

            Map(x => x.Nadimak, "NADIMAK");                                 //mapiranje svojstava.
            Map(x => x.Lozinka, "LOZINKA");
            Map(x => x.Ime, "IME");
            Map(x => x.Prezime, "PREZIME");
            Map(x => x.Pol, "POL");
            Map(x => x.Starost, "STAROST");
                          
            
            HasMany(x => x.PrevoznaSredstva).KeyColumn("ID_IGRACA").LazyLoad().Cascade.SaveUpdate();     //mapiranje veze 1:N igrac-prevoznosredstvo
            //kada brisemo igraca, prevozna sredstva ce dobiti null za vlasnika i time opet mogu da se unajme. kad pamtimo, mozemo samo igraca da pamtimo ovako
            
            HasMany(x => x.Sesije).KeyColumn("ID_IGRACA").LazyLoad().Cascade.All();               //mapiranje veze 1:N igrac-sesija
            //kada brisemo igraca, brisu se sve sesije jer nema smisla da ih cuvamo
            
            References(x => x.PripadaAlijansi).Column("ID_ALIJANSE").LazyLoad().Cascade.SaveUpdate();                            //mapiranje N:1 igrac-alijansa
            //verovatno nepotreban cascade ali neka stoji za svaki slucaj, kada pravimo alijansu mozemo samo igraca da sacuvamo
            //izgleda da nam treba ipak cascade kada izlazimo iz alijanse
            References(x => x.IdLika).Column("ID_LIKA").Cascade.All();                                                    //mapiranje veze 1:1 igrac-lik
            //kada brisemo igraca, brisacemo i njegovog lika
            
            HasManyToMany(x => x.Zadaci)         //mapiranje vezez M:N igrac-zadaci za SOLO_ZADATAK, 
                .Table("SOLO_ZADATAK")
                .ParentKeyColumn("ID_IGRACA")
                .ChildKeyColumn("ID_ZADATKA")
                .Cascade.SaveUpdate(); //Izbacili smo odavde inverse da bismo pri zavrsavanju zadatka mogli samo
                //.Inverse();  //da sacuvamo igraca i da se iz tabele poveznicu takodje izbaci ta vrsta      
                //cascade je verovatno skroz nepotreban jer ce inverse kod zadataka za to da se pobrine
            
            HasManyToMany(x => x.Itemi)         //mapiranje vezez M:N igrac-alijansa M:N za POSEDUJE,  
                .Table("POSEDUJE")
                .ParentKeyColumn("ID_IGRACA")
                .ChildKeyColumn("ID_ITEMA")
                .Cascade.SaveUpdate();
                //.Inverse()    //ista fora kao i gore
        }
    }
}
