﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMORPG_ver1.Entiteti;
using FluentNHibernate.Mapping;

namespace MMORPG_ver1.Mapiranja
{
    class ItemMapiranja : ClassMap<Item>
    {
        public ItemMapiranja()
        {
            Table("ITEM");
            Id(x => x.IdItem, "ID_ITEM").GeneratedBy.TriggerIdentity();
            DiscriminateSubClassesOnColumn("TIP");  //imamo dole sve kombinacije tipova
            HasManyToMany(x => x.Igraci)         //VEZA M:N za POSEDUJE,  
                .Table("POSEDUJE")
                .ParentKeyColumn("ID_ITEMA")    //parent je ID ove klase u kojoj smo...
                .ChildKeyColumn("ID_IGRACA")
                .Inverse(); //moramo da bi mogla da se napravi M:N veza ovde da stavimo, kada sacuvamo igraca ovime cuvamo i item
                //.Cascade.All();
        }
    }
    class BonusItemMapiranja : SubclassMap<BonusItem>
    {
        public BonusItemMapiranja()
        {
            Map(x => x.Iskustvo, "ISKUSTVO");
            HasMany(x => x.Rase).KeyColumn("ID_ITEMA").LazyLoad().Cascade.All().Inverse();  
        }
    }
    class KljucniItemMapiranja : SubclassMap<KljucniItem>
    {
        public KljucniItemMapiranja()
        {
            Map(x => x.Ime, "IME");
            Map(x => x.Opis, "OPIS");
        }
    }
    class OruzjeKljucniItemMapiranja : SubclassMap<OruzjeKljucniItem>
    {
        public OruzjeKljucniItemMapiranja()
        {
            DiscriminatorValue("OruzjeKljucni");

        }
    }
    class PredmetKljucniItemMapiranja : SubclassMap<PredmetKljucniItem>
    {
        public PredmetKljucniItemMapiranja()
        {
            DiscriminatorValue("PredmetKljucni");
            
        }
    }
    class OruzjeBonusItemMapiranja : SubclassMap<OruzjeBonusItem>
    {
        public OruzjeBonusItemMapiranja()
        {
            DiscriminatorValue("OruzjeBonus");

        }
    }
    class PredmetBonusItemMapiranja : SubclassMap<PredmetBonusItem>
    {
        public PredmetBonusItemMapiranja()
        {
            DiscriminatorValue("PredmetBonus");

        }
    }
}
