﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMORPG_ver1.Entiteti;
using FluentNHibernate.Mapping;

namespace MMORPG_ver1.Mapiranja
{
    class ZadatakMapiranja : ClassMap<Zadatak>
    {
        public ZadatakMapiranja()
        {
            Table("ZADATAK");                                                   //mapiranje tabele

            Id(x => x.IdZadatak, "ID_ZADATAK").GeneratedBy.TriggerIdentity();   //mapiranje primary ID

            DiscriminateSubClassesOnColumn("BROJNOST");     

            Map(x => x.VremePocetka, "VREME_POCETKA");                          //mapiranje atributa
            Map(x => x.VremeKraja, "VREME_KRAJA");   
            Map(x => x.BonusIskustvo, "BONUS");
                
        }
        

    }
    class SoloZadatakMapiranja : SubclassMap<SoloZadatak>
    {
        public SoloZadatakMapiranja()
        {
            DiscriminatorValue("Solo");

            HasManyToMany(x => x.Solo)                                          //VEZA M:N za igraci-solo zadataci  
                .Table("SOLO_ZADATAK")
                .ParentKeyColumn("ID_ZADATKA")                                  //parent je ID ove klase u kojoj smo
                .ChildKeyColumn("ID_IGRACA")                                  //a child one druge kod svake M:N veze
                .Inverse();  //moramo da ubacimo ovde da bi uspelo pravljenje M:N
                //.Cascade.All();  
        }
    }
    class GrupniZadatakMapiranja : SubclassMap<GrupniZadatak>
    {
        public GrupniZadatakMapiranja()
        {
            DiscriminatorValue("Grupni");

            HasManyToMany(x => x.Grupni)                                        //VEZA M:N za alijanse-grupni zadataci  
                .Table("GRUPNI_ZADATAK")
                .ParentKeyColumn("ID_ZADATKA")
                .ChildKeyColumn("ID_ALIJANSE")
                .Inverse();
                //.Cascade.All(); 
        }
    }
}
