﻿namespace MMORPG_ver1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DodajIgraca = new System.Windows.Forms.Button();
            this.DodajZadatak = new System.Windows.Forms.Button();
            this.DodajAlijansu = new System.Windows.Forms.Button();
            this.DodajGrupni = new System.Windows.Forms.Button();
            this.UbaciPrevoz = new System.Windows.Forms.Button();
            this.DodajSesiju = new System.Windows.Forms.Button();
            this.DodajItem = new System.Windows.Forms.Button();
            this.UpdateIgrac = new System.Windows.Forms.Button();
            this.UpdateZadatak = new System.Windows.Forms.Button();
            this.UpdateAlijansa = new System.Windows.Forms.Button();
            this.IzbrisiIgraca = new System.Windows.Forms.Button();
            this.IzbrisiZadatak = new System.Windows.Forms.Button();
            this.IstaknutoEXIT = new System.Windows.Forms.Button();
            this.IzbrisiAlijansu = new System.Windows.Forms.Button();
            this.IzbrisiGrupni = new System.Windows.Forms.Button();
            this.UpdateGrupni = new System.Windows.Forms.Button();
            this.UpdatePrevoz = new System.Windows.Forms.Button();
            this.UpdateSesiju = new System.Windows.Forms.Button();
            this.UpdateItem = new System.Windows.Forms.Button();
            this.IzbrisiPrevoz = new System.Windows.Forms.Button();
            this.IzbrisiSesiju = new System.Windows.Forms.Button();
            this.IzbrisiItem = new System.Windows.Forms.Button();
            this.RegisrujNovogIgraca = new System.Windows.Forms.Button();
            this.UbaciItemUBazu = new System.Windows.Forms.Button();
            this.UbaciPrevoznoSredstvoUBazu = new System.Windows.Forms.Button();
            this.UbaciAlijansuUBazu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DodajIgraca
            // 
            this.DodajIgraca.Location = new System.Drawing.Point(12, 12);
            this.DodajIgraca.Name = "DodajIgraca";
            this.DodajIgraca.Size = new System.Drawing.Size(260, 23);
            this.DodajIgraca.TabIndex = 0;
            this.DodajIgraca.Text = "DodajIgraca";
            this.DodajIgraca.UseVisualStyleBackColor = true;
            this.DodajIgraca.Click += new System.EventHandler(this.DodajIgraca_Click);
            // 
            // DodajZadatak
            // 
            this.DodajZadatak.Location = new System.Drawing.Point(12, 42);
            this.DodajZadatak.Name = "DodajZadatak";
            this.DodajZadatak.Size = new System.Drawing.Size(260, 23);
            this.DodajZadatak.TabIndex = 1;
            this.DodajZadatak.Text = "DodajZadatak";
            this.DodajZadatak.UseVisualStyleBackColor = true;
            this.DodajZadatak.Click += new System.EventHandler(this.DodajZadatak_Click);
            // 
            // DodajAlijansu
            // 
            this.DodajAlijansu.Location = new System.Drawing.Point(13, 72);
            this.DodajAlijansu.Name = "DodajAlijansu";
            this.DodajAlijansu.Size = new System.Drawing.Size(259, 23);
            this.DodajAlijansu.TabIndex = 2;
            this.DodajAlijansu.Text = "DodajAlijansu";
            this.DodajAlijansu.UseVisualStyleBackColor = true;
            this.DodajAlijansu.Click += new System.EventHandler(this.DodajAlijansu_Click);
            // 
            // DodajGrupni
            // 
            this.DodajGrupni.Location = new System.Drawing.Point(13, 102);
            this.DodajGrupni.Name = "DodajGrupni";
            this.DodajGrupni.Size = new System.Drawing.Size(259, 23);
            this.DodajGrupni.TabIndex = 3;
            this.DodajGrupni.Text = "DodajGrupni";
            this.DodajGrupni.UseVisualStyleBackColor = true;
            this.DodajGrupni.Click += new System.EventHandler(this.button1_Click);
            // 
            // UbaciPrevoz
            // 
            this.UbaciPrevoz.Location = new System.Drawing.Point(13, 132);
            this.UbaciPrevoz.Name = "UbaciPrevoz";
            this.UbaciPrevoz.Size = new System.Drawing.Size(259, 23);
            this.UbaciPrevoz.TabIndex = 4;
            this.UbaciPrevoz.Text = "UbaciPrevoz";
            this.UbaciPrevoz.UseVisualStyleBackColor = true;
            this.UbaciPrevoz.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // DodajSesiju
            // 
            this.DodajSesiju.Location = new System.Drawing.Point(13, 161);
            this.DodajSesiju.Name = "DodajSesiju";
            this.DodajSesiju.Size = new System.Drawing.Size(259, 23);
            this.DodajSesiju.TabIndex = 5;
            this.DodajSesiju.Text = "DodajSesiju";
            this.DodajSesiju.UseVisualStyleBackColor = true;
            this.DodajSesiju.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // DodajItem
            // 
            this.DodajItem.Location = new System.Drawing.Point(13, 191);
            this.DodajItem.Name = "DodajItem";
            this.DodajItem.Size = new System.Drawing.Size(259, 23);
            this.DodajItem.TabIndex = 6;
            this.DodajItem.Text = "DodajItem";
            this.DodajItem.UseVisualStyleBackColor = true;
            this.DodajItem.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // UpdateIgrac
            // 
            this.UpdateIgrac.Location = new System.Drawing.Point(278, 12);
            this.UpdateIgrac.Name = "UpdateIgrac";
            this.UpdateIgrac.Size = new System.Drawing.Size(259, 23);
            this.UpdateIgrac.TabIndex = 7;
            this.UpdateIgrac.Text = "UpdateIgrac";
            this.UpdateIgrac.UseVisualStyleBackColor = true;
            this.UpdateIgrac.Click += new System.EventHandler(this.UpdateIgrac_Click);
            // 
            // UpdateZadatak
            // 
            this.UpdateZadatak.Location = new System.Drawing.Point(278, 42);
            this.UpdateZadatak.Name = "UpdateZadatak";
            this.UpdateZadatak.Size = new System.Drawing.Size(259, 23);
            this.UpdateZadatak.TabIndex = 0;
            this.UpdateZadatak.Text = "UpdateZadatak";
            this.UpdateZadatak.Click += new System.EventHandler(this.UpdateZadatak_Click);
            // 
            // UpdateAlijansa
            // 
            this.UpdateAlijansa.Location = new System.Drawing.Point(278, 71);
            this.UpdateAlijansa.Name = "UpdateAlijansa";
            this.UpdateAlijansa.Size = new System.Drawing.Size(259, 23);
            this.UpdateAlijansa.TabIndex = 8;
            this.UpdateAlijansa.Text = "UpdateAlijansa";
            this.UpdateAlijansa.UseVisualStyleBackColor = true;
            this.UpdateAlijansa.Click += new System.EventHandler(this.button1_Click_4);
            // 
            // IzbrisiIgraca
            // 
            this.IzbrisiIgraca.Location = new System.Drawing.Point(545, 12);
            this.IzbrisiIgraca.Name = "IzbrisiIgraca";
            this.IzbrisiIgraca.Size = new System.Drawing.Size(259, 23);
            this.IzbrisiIgraca.TabIndex = 9;
            this.IzbrisiIgraca.Text = "IzbrisiIgraca";
            this.IzbrisiIgraca.UseVisualStyleBackColor = true;
            this.IzbrisiIgraca.Click += new System.EventHandler(this.IzbrisiIgraca_Click);
            // 
            // IzbrisiZadatak
            // 
            this.IzbrisiZadatak.Location = new System.Drawing.Point(545, 42);
            this.IzbrisiZadatak.Name = "IzbrisiZadatak";
            this.IzbrisiZadatak.Size = new System.Drawing.Size(259, 23);
            this.IzbrisiZadatak.TabIndex = 10;
            this.IzbrisiZadatak.Text = "IzbrisiZadatak";
            this.IzbrisiZadatak.UseVisualStyleBackColor = true;
            this.IzbrisiZadatak.Click += new System.EventHandler(this.IzbrisiZadatak_Click);
            // 
            // IstaknutoEXIT
            // 
            this.IstaknutoEXIT.Location = new System.Drawing.Point(678, 231);
            this.IstaknutoEXIT.Name = "IstaknutoEXIT";
            this.IstaknutoEXIT.Size = new System.Drawing.Size(124, 23);
            this.IstaknutoEXIT.TabIndex = 12;
            this.IstaknutoEXIT.Text = "IstaknutoEXIT";
            this.IstaknutoEXIT.UseVisualStyleBackColor = true;
            this.IstaknutoEXIT.Click += new System.EventHandler(this.IstaknutoEXIT_Click);
            // 
            // IzbrisiAlijansu
            // 
            this.IzbrisiAlijansu.Location = new System.Drawing.Point(545, 71);
            this.IzbrisiAlijansu.Name = "IzbrisiAlijansu";
            this.IzbrisiAlijansu.Size = new System.Drawing.Size(259, 23);
            this.IzbrisiAlijansu.TabIndex = 14;
            this.IzbrisiAlijansu.Text = "IzbrisiAlijansu";
            this.IzbrisiAlijansu.UseVisualStyleBackColor = true;
            this.IzbrisiAlijansu.Click += new System.EventHandler(this.IzbrisiAlijansu_Click);
            // 
            // IzbrisiGrupni
            // 
            this.IzbrisiGrupni.Location = new System.Drawing.Point(545, 100);
            this.IzbrisiGrupni.Name = "IzbrisiGrupni";
            this.IzbrisiGrupni.Size = new System.Drawing.Size(259, 23);
            this.IzbrisiGrupni.TabIndex = 15;
            this.IzbrisiGrupni.Text = "IzbrisiGrupni";
            this.IzbrisiGrupni.UseVisualStyleBackColor = true;
            this.IzbrisiGrupni.Click += new System.EventHandler(this.button1_Click_5);
            // 
            // UpdateGrupni
            // 
            this.UpdateGrupni.Location = new System.Drawing.Point(278, 100);
            this.UpdateGrupni.Name = "UpdateGrupni";
            this.UpdateGrupni.Size = new System.Drawing.Size(259, 23);
            this.UpdateGrupni.TabIndex = 16;
            this.UpdateGrupni.Text = "UpdateGrupni";
            this.UpdateGrupni.UseVisualStyleBackColor = true;
            this.UpdateGrupni.Click += new System.EventHandler(this.UpdateGrupni_Click);
            // 
            // UpdatePrevoz
            // 
            this.UpdatePrevoz.Location = new System.Drawing.Point(278, 132);
            this.UpdatePrevoz.Name = "UpdatePrevoz";
            this.UpdatePrevoz.Size = new System.Drawing.Size(259, 23);
            this.UpdatePrevoz.TabIndex = 17;
            this.UpdatePrevoz.Text = "UpdatePrevoz";
            this.UpdatePrevoz.UseVisualStyleBackColor = true;
            this.UpdatePrevoz.Click += new System.EventHandler(this.UpdatePrevoz_Click);
            // 
            // UpdateSesiju
            // 
            this.UpdateSesiju.Location = new System.Drawing.Point(278, 162);
            this.UpdateSesiju.Name = "UpdateSesiju";
            this.UpdateSesiju.Size = new System.Drawing.Size(259, 23);
            this.UpdateSesiju.TabIndex = 18;
            this.UpdateSesiju.Text = "UpdateSesiju";
            this.UpdateSesiju.UseVisualStyleBackColor = true;
            this.UpdateSesiju.Click += new System.EventHandler(this.UpdateSesiju_Click);
            // 
            // UpdateItem
            // 
            this.UpdateItem.Location = new System.Drawing.Point(278, 190);
            this.UpdateItem.Name = "UpdateItem";
            this.UpdateItem.Size = new System.Drawing.Size(259, 23);
            this.UpdateItem.TabIndex = 19;
            this.UpdateItem.Text = "UpdateItem";
            this.UpdateItem.UseVisualStyleBackColor = true;
            this.UpdateItem.Click += new System.EventHandler(this.UpdateItem_Click);
            // 
            // IzbrisiPrevoz
            // 
            this.IzbrisiPrevoz.Location = new System.Drawing.Point(545, 130);
            this.IzbrisiPrevoz.Name = "IzbrisiPrevoz";
            this.IzbrisiPrevoz.Size = new System.Drawing.Size(257, 23);
            this.IzbrisiPrevoz.TabIndex = 20;
            this.IzbrisiPrevoz.Text = "IzbrisiPrevoz";
            this.IzbrisiPrevoz.UseVisualStyleBackColor = true;
            this.IzbrisiPrevoz.Click += new System.EventHandler(this.IzbrisiPrevoz_Click);
            // 
            // IzbrisiSesiju
            // 
            this.IzbrisiSesiju.Location = new System.Drawing.Point(545, 160);
            this.IzbrisiSesiju.Name = "IzbrisiSesiju";
            this.IzbrisiSesiju.Size = new System.Drawing.Size(257, 23);
            this.IzbrisiSesiju.TabIndex = 21;
            this.IzbrisiSesiju.Text = "IzbrisiSesiju";
            this.IzbrisiSesiju.UseVisualStyleBackColor = true;
            this.IzbrisiSesiju.Click += new System.EventHandler(this.IzbrisiSesiju_Click);
            // 
            // IzbrisiItem
            // 
            this.IzbrisiItem.Location = new System.Drawing.Point(545, 190);
            this.IzbrisiItem.Name = "IzbrisiItem";
            this.IzbrisiItem.Size = new System.Drawing.Size(257, 23);
            this.IzbrisiItem.TabIndex = 22;
            this.IzbrisiItem.Text = "IzbrisiItem";
            this.IzbrisiItem.UseVisualStyleBackColor = true;
            this.IzbrisiItem.Click += new System.EventHandler(this.IzbrisiItem_Click);
            // 
            // RegisrujNovogIgraca
            // 
            this.RegisrujNovogIgraca.Location = new System.Drawing.Point(13, 255);
            this.RegisrujNovogIgraca.Name = "RegisrujNovogIgraca";
            this.RegisrujNovogIgraca.Size = new System.Drawing.Size(259, 23);
            this.RegisrujNovogIgraca.TabIndex = 23;
            this.RegisrujNovogIgraca.Text = "RegisrujNovogIgraca";
            this.RegisrujNovogIgraca.UseVisualStyleBackColor = true;
            this.RegisrujNovogIgraca.Click += new System.EventHandler(this.RegisrujNovogIgraca_Click);
            // 
            // UbaciItemUBazu
            // 
            this.UbaciItemUBazu.Location = new System.Drawing.Point(13, 284);
            this.UbaciItemUBazu.Name = "UbaciItemUBazu";
            this.UbaciItemUBazu.Size = new System.Drawing.Size(259, 23);
            this.UbaciItemUBazu.TabIndex = 24;
            this.UbaciItemUBazu.Text = "UbaciItemUBazu";
            this.UbaciItemUBazu.UseVisualStyleBackColor = true;
            this.UbaciItemUBazu.Click += new System.EventHandler(this.UbaciItemUBazu_Click);
            // 
            // UbaciPrevoznoSredstvoUBazu
            // 
            this.UbaciPrevoznoSredstvoUBazu.Location = new System.Drawing.Point(13, 314);
            this.UbaciPrevoznoSredstvoUBazu.Name = "UbaciPrevoznoSredstvoUBazu";
            this.UbaciPrevoznoSredstvoUBazu.Size = new System.Drawing.Size(259, 23);
            this.UbaciPrevoznoSredstvoUBazu.TabIndex = 25;
            this.UbaciPrevoznoSredstvoUBazu.Text = "UbaciPrevoznoSredstvoUBazu";
            this.UbaciPrevoznoSredstvoUBazu.UseVisualStyleBackColor = true;
            this.UbaciPrevoznoSredstvoUBazu.Click += new System.EventHandler(this.UbaciPrevoznoSredstvoUBazu_Click);
            // 
            // UbaciAlijansuUBazu
            // 
            this.UbaciAlijansuUBazu.Location = new System.Drawing.Point(13, 344);
            this.UbaciAlijansuUBazu.Name = "UbaciAlijansuUBazu";
            this.UbaciAlijansuUBazu.Size = new System.Drawing.Size(259, 23);
            this.UbaciAlijansuUBazu.TabIndex = 26;
            this.UbaciAlijansuUBazu.Text = "UbaciAlijansuUBazu";
            this.UbaciAlijansuUBazu.UseVisualStyleBackColor = true;
            this.UbaciAlijansuUBazu.Click += new System.EventHandler(this.UbaciAlijansuUBazu_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 389);
            this.Controls.Add(this.UbaciAlijansuUBazu);
            this.Controls.Add(this.UbaciPrevoznoSredstvoUBazu);
            this.Controls.Add(this.UbaciItemUBazu);
            this.Controls.Add(this.RegisrujNovogIgraca);
            this.Controls.Add(this.IzbrisiItem);
            this.Controls.Add(this.IzbrisiSesiju);
            this.Controls.Add(this.IzbrisiPrevoz);
            this.Controls.Add(this.UpdateItem);
            this.Controls.Add(this.UpdateSesiju);
            this.Controls.Add(this.UpdatePrevoz);
            this.Controls.Add(this.UpdateGrupni);
            this.Controls.Add(this.IzbrisiGrupni);
            this.Controls.Add(this.IzbrisiAlijansu);
            this.Controls.Add(this.IstaknutoEXIT);
            this.Controls.Add(this.IzbrisiZadatak);
            this.Controls.Add(this.IzbrisiIgraca);
            this.Controls.Add(this.UpdateAlijansa);
            this.Controls.Add(this.UpdateZadatak);
            this.Controls.Add(this.UpdateIgrac);
            this.Controls.Add(this.DodajItem);
            this.Controls.Add(this.DodajSesiju);
            this.Controls.Add(this.UbaciPrevoz);
            this.Controls.Add(this.DodajGrupni);
            this.Controls.Add(this.DodajAlijansu);
            this.Controls.Add(this.DodajZadatak);
            this.Controls.Add(this.DodajIgraca);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button DodajIgraca;
        private System.Windows.Forms.Button DodajZadatak;
        private System.Windows.Forms.Button DodajAlijansu;
        private System.Windows.Forms.Button DodajGrupni;
        private System.Windows.Forms.Button UbaciPrevoz;
        private System.Windows.Forms.Button DodajSesiju;
        private System.Windows.Forms.Button DodajItem;
        private System.Windows.Forms.Button UpdateIgrac;
        private System.Windows.Forms.Button UpdateZadatak;
        private System.Windows.Forms.Button UpdateAlijansa;
        private System.Windows.Forms.Button IzbrisiIgraca;
        private System.Windows.Forms.Button IzbrisiZadatak;
        private System.Windows.Forms.Button IstaknutoEXIT;
        private System.Windows.Forms.Button IzbrisiAlijansu;
        private System.Windows.Forms.Button IzbrisiGrupni;
        private System.Windows.Forms.Button UpdateGrupni;
        private System.Windows.Forms.Button UpdatePrevoz;
        private System.Windows.Forms.Button UpdateSesiju;
        private System.Windows.Forms.Button UpdateItem;
        private System.Windows.Forms.Button IzbrisiPrevoz;
        private System.Windows.Forms.Button IzbrisiSesiju;
        private System.Windows.Forms.Button IzbrisiItem;
        private System.Windows.Forms.Button RegisrujNovogIgraca;
        private System.Windows.Forms.Button UbaciItemUBazu;
        private System.Windows.Forms.Button UbaciPrevoznoSredstvoUBazu;
        private System.Windows.Forms.Button UbaciAlijansuUBazu;
    }
}

