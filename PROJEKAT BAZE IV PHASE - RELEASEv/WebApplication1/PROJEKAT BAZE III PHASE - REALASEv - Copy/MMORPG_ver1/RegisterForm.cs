﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;


namespace MMORPG_ver1
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 4;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxIme.BackColor=Color.White;
            textBoxLozinka.BackColor=Color.White;
            textBoxStarost.BackColor = Color.White;
            if (textBoxNick.TextLength == 0)
            {
                MessageBox.Show("Unesite nadimak");
                textBoxNick.BackColor = Color.IndianRed;
            }
            else
                if (textBoxLozinka.TextLength == 0)
                {
                    MessageBox.Show("Unesite lozinku");
                    textBoxLozinka.BackColor = Color.IndianRed;
                }
                else
                {


                    try
                    {
                        ISession s = DataLayer.GetSession();


                        Entiteti.Igrac p = new Entiteti.Igrac();

                        p.Ime = textBoxIme.Text;
                        p.Prezime = textBoxPrezime.Text;
                        if (radioButtonMuski.Checked)
                            p.Pol = 'M';
                        else
                            p.Pol = 'Z';
                        if (textBoxStarost.TextLength != 0)
                            p.Starost = Convert.ToInt32(textBoxStarost.Text);
                        else
                            p.Starost = 0;
                        p.Nadimak = textBoxNick.Text;
                        p.Lozinka = textBoxLozinka.Text;
                        Entiteti.Lik c;

                        
                        switch (comboBox1.SelectedIndex)
                        {
                            case 0:
                                {
                                    c = new Entiteti.LikVilenjak();
                                    ((Entiteti.LikVilenjak)c).Energija = 100;
                                    break;
                                }
                            case 1:
                                {
                                    c = new Entiteti.LikDemon();
                                    ((Entiteti.LikDemon)c).Energija = 100;
                                    break;
                                }
                            case 2:
                                {
                                    c = new Entiteti.LikOrk();
                                    ((Entiteti.LikOrk)c).SpecijalizacijaOruzja = 10;
                                    break;
                                }
                            case 3:
                                {
                                    c = new Entiteti.LikPatuljak();
                                    ((Entiteti.LikPatuljak)c).SpecijalizacijaOruzja = 10;
                                    break;
                                }
                            default:
                                {
                                    c = new Entiteti.LikCovek();
                                    ((Entiteti.LikCovek)c).BonusSkrivanja = 5;
                                    break;
                                }
                        }

                        c.IdIgraca = p;

                        c.Iskustvo = 0;
                        c.Zdravlje = 100;
                        c.StepenZamora = 0;
                        c.KolicinaZlata = 0;

                        p.IdLika = c;

                        
                        s.SaveOrUpdate(p);  //igrac povlaci save za lika

                        s.Flush();
                        s.Close();

                        MessageBox.Show("Uspešno ste se registrovali");
                        this.Close();
                    }
                    catch (System.FormatException ec)
                    {
                        MessageBox.Show("Unesite za starost broj od 12 do 100");//kako je proslo kod igraca
                        textBoxStarost.BackColor = Color.IndianRed;
                    }
                    catch (Exception ec)
                    {
                        
                        if (ec.InnerException.Message.Contains("unique"))
                        {
                            MessageBox.Show("Uneti nadimak je zauzet");
                            textBoxNick.BackColor = Color.IndianRed;
                        }
                        else
                        {
                            MessageBox.Show("Neobradjeni prekid, presli ste nas");
                            this.Close();
                        }
                       
                    }
                   
                }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBoxNick_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
