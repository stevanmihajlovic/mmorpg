﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class DeleteQuestForm : Form
    {
        public IList<Zadatak> zadaci;

        public DeleteQuestForm()
        {
            InitializeComponent();

            try
            {
                ISession s = DataLayer.GetSession();

                zadaci = (from a in s.Query<Entiteti.Zadatak>()
                          select a).ToList<Zadatak>();

                foreach (Zadatak a in zadaci)
                    listBox1.Items.Add(a.IdZadatak);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Selektujte item");
            else
            {
                try
                {
                    ISession s = DataLayer.GetSession();
                    Zadatak selektovan = zadaci[listBox1.SelectedIndex];
                    s.Lock(selektovan, LockMode.None);
                    Zadatak selektovan1 = (Zadatak)s.GetSessionImplementation().PersistenceContext.Unproxy(selektovan);


                    if (selektovan1.GetType() == typeof(SoloZadatak))
                        foreach (Igrac i in ((SoloZadatak)selektovan).Solo)
                        {
                            i.Zadaci.Remove((SoloZadatak)selektovan);
                            s.SaveOrUpdate(i);
                        }
                    
                    if (selektovan1.GetType() == typeof(GrupniZadatak))
                        foreach (Alijansa a in ((GrupniZadatak)selektovan).Grupni)
                        {
                            a.Zadaci.Remove((GrupniZadatak)selektovan);
                            s.SaveOrUpdate(a);
                        }
                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                    s.Delete(selektovan);

                    s.Flush();
                    s.Close();
                    MessageBox.Show("Uspesno ste izbacili zadatak");
                    this.Close();
                }
                catch (Exception ec)
                {
                    // if (ec.Message.Contains("already") || ec.InnerException.Message.Contains("insert"))
                    //   MessageBox.Show("Igrac vec poseduje item");
                    // else
                    MessageBox.Show(ec.Message);
                }
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            InsertQuestForm i = new InsertQuestForm(zadaci[listBox1.SelectedIndex].IdZadatak);
            i.ShowDialog();
        }
    }
}