﻿namespace MMORPG_ver1
{
    partial class InsertMountForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonZivotinja = new System.Windows.Forms.RadioButton();
            this.radioButtonZmaj = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButtonMehanicki = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxNaziv = new System.Windows.Forms.TextBox();
            this.textBoxBrzina = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxCena = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(159, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Podaci o prevoznom sredstvu";
            // 
            // radioButtonZivotinja
            // 
            this.radioButtonZivotinja.AutoSize = true;
            this.radioButtonZivotinja.Location = new System.Drawing.Point(116, 162);
            this.radioButtonZivotinja.Name = "radioButtonZivotinja";
            this.radioButtonZivotinja.Size = new System.Drawing.Size(65, 17);
            this.radioButtonZivotinja.TabIndex = 6;
            this.radioButtonZivotinja.Text = "Životinja";
            this.radioButtonZivotinja.UseVisualStyleBackColor = true;
            // 
            // radioButtonZmaj
            // 
            this.radioButtonZmaj.AutoSize = true;
            this.radioButtonZmaj.Checked = true;
            this.radioButtonZmaj.Location = new System.Drawing.Point(24, 162);
            this.radioButtonZmaj.Name = "radioButtonZmaj";
            this.radioButtonZmaj.Size = new System.Drawing.Size(48, 17);
            this.radioButtonZmaj.TabIndex = 5;
            this.radioButtonZmaj.TabStop = true;
            this.radioButtonZmaj.Text = "Zmaj";
            this.radioButtonZmaj.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 133);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(170, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Izaberite tip prevoznog sredstva:";
            // 
            // radioButtonMehanicki
            // 
            this.radioButtonMehanicki.AutoSize = true;
            this.radioButtonMehanicki.Location = new System.Drawing.Point(217, 162);
            this.radioButtonMehanicki.Name = "radioButtonMehanicki";
            this.radioButtonMehanicki.Size = new System.Drawing.Size(74, 17);
            this.radioButtonMehanicki.TabIndex = 7;
            this.radioButtonMehanicki.Text = "Mehanički";
            this.radioButtonMehanicki.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 50);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(93, 23);
            this.label3.TabIndex = 8;
            this.label3.Text = "Unesite Naziv:* ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 73);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5);
            this.label4.Size = new System.Drawing.Size(91, 23);
            this.label4.TabIndex = 9;
            this.label4.Text = "Unesite Brzinu: ";
            // 
            // textBoxNaziv
            // 
            this.textBoxNaziv.Location = new System.Drawing.Point(108, 52);
            this.textBoxNaziv.Name = "textBoxNaziv";
            this.textBoxNaziv.Size = new System.Drawing.Size(164, 20);
            this.textBoxNaziv.TabIndex = 10;
            // 
            // textBoxBrzina
            // 
            this.textBoxBrzina.Location = new System.Drawing.Point(108, 76);
            this.textBoxBrzina.Name = "textBoxBrzina";
            this.textBoxBrzina.Size = new System.Drawing.Size(63, 20);
            this.textBoxBrzina.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(136, 199);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Ubaci prevozno sredstvo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 235);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(5);
            this.label9.Size = new System.Drawing.Size(214, 23);
            this.label9.TabIndex = 17;
            this.label9.Text = "Polja označena sa * moraju biti popunjena";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 96);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5);
            this.label5.Size = new System.Drawing.Size(84, 23);
            this.label5.TabIndex = 18;
            this.label5.Text = "Unesite Cenu:";
            // 
            // textBoxCena
            // 
            this.textBoxCena.Location = new System.Drawing.Point(108, 99);
            this.textBoxCena.Name = "textBoxCena";
            this.textBoxCena.Size = new System.Drawing.Size(63, 20);
            this.textBoxCena.TabIndex = 19;
            // 
            // InsertMountForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 269);
            this.Controls.Add(this.textBoxCena);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxBrzina);
            this.Controls.Add(this.textBoxNaziv);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.radioButtonMehanicki);
            this.Controls.Add(this.radioButtonZivotinja);
            this.Controls.Add(this.radioButtonZmaj);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "InsertMountForm";
            this.Text = "InsertMountForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonZivotinja;
        private System.Windows.Forms.RadioButton radioButtonZmaj;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioButtonMehanicki;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxNaziv;
        private System.Windows.Forms.TextBox textBoxBrzina;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxCena;
    }
}