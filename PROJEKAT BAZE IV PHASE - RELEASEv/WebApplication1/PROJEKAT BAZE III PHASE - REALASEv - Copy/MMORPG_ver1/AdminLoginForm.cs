﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMORPG_ver1
{
    public partial class AdminLoginForm : Form
    {
        public AdminLoginForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Equals("admin"))
            {
                this.Close();
                AdminForm nova = new AdminForm();
                nova.ShowDialog();
            }
            else
                MessageBox.Show("Uneli ste pogrešnu lozinku");
        }
    }
}
