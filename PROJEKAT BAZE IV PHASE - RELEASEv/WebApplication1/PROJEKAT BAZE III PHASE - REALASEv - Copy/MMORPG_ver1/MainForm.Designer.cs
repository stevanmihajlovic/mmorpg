﻿namespace MMORPG_ver1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AdministratorskiPristup = new System.Windows.Forms.Button();
            this.KorisnickiPristup = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AdministratorskiPristup
            // 
            this.AdministratorskiPristup.Location = new System.Drawing.Point(45, 66);
            this.AdministratorskiPristup.Name = "AdministratorskiPristup";
            this.AdministratorskiPristup.Size = new System.Drawing.Size(161, 23);
            this.AdministratorskiPristup.TabIndex = 0;
            this.AdministratorskiPristup.Text = "Administratorski Pristup";
            this.AdministratorskiPristup.UseVisualStyleBackColor = true;
            this.AdministratorskiPristup.Click += new System.EventHandler(this.AdministratorskiPristup_Click);
            // 
            // KorisnickiPristup
            // 
            this.KorisnickiPristup.Location = new System.Drawing.Point(45, 95);
            this.KorisnickiPristup.Name = "KorisnickiPristup";
            this.KorisnickiPristup.Size = new System.Drawing.Size(161, 23);
            this.KorisnickiPristup.TabIndex = 1;
            this.KorisnickiPristup.Text = "Korisnički pristup";
            this.KorisnickiPristup.UseVisualStyleBackColor = true;
            this.KorisnickiPristup.Click += new System.EventHandler(this.KorisnickiPristup_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 9);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(10);
            this.label1.Size = new System.Drawing.Size(141, 33);
            this.label1.TabIndex = 2;
            this.label1.Text = "MMO RPG APLIKACIJA";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 141);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.KorisnickiPristup);
            this.Controls.Add(this.AdministratorskiPristup);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AdministratorskiPristup;
        private System.Windows.Forms.Button KorisnickiPristup;
        private System.Windows.Forms.Label label1;
    }
}