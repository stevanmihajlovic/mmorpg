﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace MMORPG_ver1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void AdministratorskiPristup_Click(object sender, EventArgs e)
        {
            AdminLoginForm nova = new AdminLoginForm();
            nova.ShowDialog();
        }

        private void KorisnickiPristup_Click(object sender, EventArgs e)
        {
            PlayerForm nova = new PlayerForm();
            nova.ShowDialog();
        }
    }
}
