﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMORPG_ver1
{
    public class LikIgrac
    {
        public virtual string Nadimak { get; set; }
        public virtual string Lozinka { get; set; }
        public virtual string Ime { get; set; }
        public virtual string Prezime { get; set; }
        public virtual char Pol { get; set; }
        public virtual int Starost { get; set; }
        public virtual int StepenZamora { get; set; }
        public virtual int Iskustvo { get; set; }
        public virtual int Zdravlje { get; set; }
        public virtual int KolicinaZlata { get; set; }
        public virtual string Rasa { get; set; }
        public LikIgrac()
        {

        }
    }
}
