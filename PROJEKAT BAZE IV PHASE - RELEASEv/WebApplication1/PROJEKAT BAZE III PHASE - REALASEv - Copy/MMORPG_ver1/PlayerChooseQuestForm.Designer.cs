﻿namespace MMORPG_ver1
{
    partial class PlayerChooseQuestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buttonIzaberite = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 51);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(205, 95);
            this.listBox1.TabIndex = 5;
            // 
            // buttonIzaberite
            // 
            this.buttonIzaberite.Location = new System.Drawing.Point(142, 167);
            this.buttonIzaberite.Name = "buttonIzaberite";
            this.buttonIzaberite.Size = new System.Drawing.Size(75, 23);
            this.buttonIzaberite.TabIndex = 6;
            this.buttonIzaberite.Text = "Izaberite";
            this.buttonIzaberite.UseVisualStyleBackColor = true;
            this.buttonIzaberite.Click += new System.EventHandler(this.buttonIzaberite_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(108, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "Izaberite Zadatak: ";
            // 
            // PlayerChooseQuestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 219);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonIzaberite);
            this.Controls.Add(this.listBox1);
            this.Name = "PlayerChooseQuestForm";
            this.Text = "PlayerChooseQuestForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button buttonIzaberite;
        private System.Windows.Forms.Label label1;
    }
}