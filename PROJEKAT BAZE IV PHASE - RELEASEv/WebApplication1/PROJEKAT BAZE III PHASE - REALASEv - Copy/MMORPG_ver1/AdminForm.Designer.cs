﻿namespace MMORPG_ver1
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UbaciPrevoznoSredstvoUBazu = new System.Windows.Forms.Button();
            this.UbaciItemUBazu = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonUbaciZadatakUBazu = new System.Windows.Forms.Button();
            this.buttonPregledSesija = new System.Windows.Forms.Button();
            this.buttonKreirajSavez = new System.Windows.Forms.Button();
            this.buttonStaviAljansuNaZadatak = new System.Windows.Forms.Button();
            this.buttonListaIgracaZaKljucniItem = new System.Windows.Forms.Button();
            this.buttonIzbaciItemIzBaze = new System.Windows.Forms.Button();
            this.buttonIzbaciPrevoznoSredstvoIzBaze = new System.Windows.Forms.Button();
            this.buttonIzbaciZadatakIzBaze = new System.Windows.Forms.Button();
            this.buttonBanujIgraca = new System.Windows.Forms.Button();
            this.buttonBrisiAlijansu = new System.Windows.Forms.Button();
            this.buttonIzlaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // UbaciPrevoznoSredstvoUBazu
            // 
            this.UbaciPrevoznoSredstvoUBazu.Location = new System.Drawing.Point(13, 96);
            this.UbaciPrevoznoSredstvoUBazu.Name = "UbaciPrevoznoSredstvoUBazu";
            this.UbaciPrevoznoSredstvoUBazu.Size = new System.Drawing.Size(187, 23);
            this.UbaciPrevoznoSredstvoUBazu.TabIndex = 27;
            this.UbaciPrevoznoSredstvoUBazu.Text = "Ubaci Prevozno Sredstvo U Bazu";
            this.UbaciPrevoznoSredstvoUBazu.UseVisualStyleBackColor = true;
            this.UbaciPrevoznoSredstvoUBazu.Click += new System.EventHandler(this.UbaciPrevoznoSredstvoUBazu_Click);
            // 
            // UbaciItemUBazu
            // 
            this.UbaciItemUBazu.Location = new System.Drawing.Point(13, 67);
            this.UbaciItemUBazu.Name = "UbaciItemUBazu";
            this.UbaciItemUBazu.Size = new System.Drawing.Size(187, 23);
            this.UbaciItemUBazu.TabIndex = 26;
            this.UbaciItemUBazu.Text = "Ubaci Item U Bazu";
            this.UbaciItemUBazu.UseVisualStyleBackColor = true;
            this.UbaciItemUBazu.Click += new System.EventHandler(this.UbaciItemUBazu_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(10);
            this.label1.Size = new System.Drawing.Size(143, 35);
            this.label1.TabIndex = 28;
            this.label1.Text = "Administratorske opcije: ";
            // 
            // buttonUbaciZadatakUBazu
            // 
            this.buttonUbaciZadatakUBazu.Location = new System.Drawing.Point(13, 125);
            this.buttonUbaciZadatakUBazu.Name = "buttonUbaciZadatakUBazu";
            this.buttonUbaciZadatakUBazu.Size = new System.Drawing.Size(187, 23);
            this.buttonUbaciZadatakUBazu.TabIndex = 29;
            this.buttonUbaciZadatakUBazu.Text = "Ubaci Zadatak U Bazu";
            this.buttonUbaciZadatakUBazu.UseVisualStyleBackColor = true;
            this.buttonUbaciZadatakUBazu.Click += new System.EventHandler(this.buttonUbaciZadatakUBazu_Click);
            // 
            // buttonPregledSesija
            // 
            this.buttonPregledSesija.Location = new System.Drawing.Point(226, 178);
            this.buttonPregledSesija.Name = "buttonPregledSesija";
            this.buttonPregledSesija.Size = new System.Drawing.Size(187, 23);
            this.buttonPregledSesija.TabIndex = 30;
            this.buttonPregledSesija.Text = "Pregled Sesija";
            this.buttonPregledSesija.UseVisualStyleBackColor = true;
            this.buttonPregledSesija.Click += new System.EventHandler(this.buttonPregledSesija_Click);
            // 
            // buttonKreirajSavez
            // 
            this.buttonKreirajSavez.Location = new System.Drawing.Point(13, 226);
            this.buttonKreirajSavez.Name = "buttonKreirajSavez";
            this.buttonKreirajSavez.Size = new System.Drawing.Size(187, 23);
            this.buttonKreirajSavez.TabIndex = 31;
            this.buttonKreirajSavez.Text = "Rad sa Savezima";
            this.buttonKreirajSavez.UseVisualStyleBackColor = true;
            this.buttonKreirajSavez.Click += new System.EventHandler(this.buttonKreirajSavez_Click);
            // 
            // buttonStaviAljansuNaZadatak
            // 
            this.buttonStaviAljansuNaZadatak.Location = new System.Drawing.Point(226, 226);
            this.buttonStaviAljansuNaZadatak.Name = "buttonStaviAljansuNaZadatak";
            this.buttonStaviAljansuNaZadatak.Size = new System.Drawing.Size(187, 23);
            this.buttonStaviAljansuNaZadatak.TabIndex = 32;
            this.buttonStaviAljansuNaZadatak.Text = "Rad sa Alijansama i Zadacima";
            this.buttonStaviAljansuNaZadatak.UseVisualStyleBackColor = true;
            this.buttonStaviAljansuNaZadatak.Click += new System.EventHandler(this.buttonStaviAljansuNaZadatak_Click);
            // 
            // buttonListaIgracaZaKljucniItem
            // 
            this.buttonListaIgracaZaKljucniItem.Location = new System.Drawing.Point(13, 178);
            this.buttonListaIgracaZaKljucniItem.Name = "buttonListaIgracaZaKljucniItem";
            this.buttonListaIgracaZaKljucniItem.Size = new System.Drawing.Size(187, 23);
            this.buttonListaIgracaZaKljucniItem.TabIndex = 33;
            this.buttonListaIgracaZaKljucniItem.Text = "Lista Igraca Za Kljucni Item";
            this.buttonListaIgracaZaKljucniItem.UseVisualStyleBackColor = true;
            this.buttonListaIgracaZaKljucniItem.Click += new System.EventHandler(this.buttonListaIgracaZaKljucniItem_Click);
            // 
            // buttonIzbaciItemIzBaze
            // 
            this.buttonIzbaciItemIzBaze.Location = new System.Drawing.Point(226, 67);
            this.buttonIzbaciItemIzBaze.Name = "buttonIzbaciItemIzBaze";
            this.buttonIzbaciItemIzBaze.Size = new System.Drawing.Size(187, 23);
            this.buttonIzbaciItemIzBaze.TabIndex = 34;
            this.buttonIzbaciItemIzBaze.Text = "Izbaci/Update Item";
            this.buttonIzbaciItemIzBaze.UseVisualStyleBackColor = true;
            this.buttonIzbaciItemIzBaze.Click += new System.EventHandler(this.buttonIzbaciItemIzBaze_Click);
            // 
            // buttonIzbaciPrevoznoSredstvoIzBaze
            // 
            this.buttonIzbaciPrevoznoSredstvoIzBaze.Location = new System.Drawing.Point(226, 96);
            this.buttonIzbaciPrevoznoSredstvoIzBaze.Name = "buttonIzbaciPrevoznoSredstvoIzBaze";
            this.buttonIzbaciPrevoznoSredstvoIzBaze.Size = new System.Drawing.Size(187, 23);
            this.buttonIzbaciPrevoznoSredstvoIzBaze.TabIndex = 35;
            this.buttonIzbaciPrevoznoSredstvoIzBaze.Text = "Izbaci/Update Prevozno Sredstvo";
            this.buttonIzbaciPrevoznoSredstvoIzBaze.UseVisualStyleBackColor = true;
            this.buttonIzbaciPrevoznoSredstvoIzBaze.Click += new System.EventHandler(this.buttonIzbaciPrevoznoSredstvoIzBaze_Click);
            // 
            // buttonIzbaciZadatakIzBaze
            // 
            this.buttonIzbaciZadatakIzBaze.Location = new System.Drawing.Point(226, 125);
            this.buttonIzbaciZadatakIzBaze.Name = "buttonIzbaciZadatakIzBaze";
            this.buttonIzbaciZadatakIzBaze.Size = new System.Drawing.Size(187, 23);
            this.buttonIzbaciZadatakIzBaze.TabIndex = 36;
            this.buttonIzbaciZadatakIzBaze.Text = "Izbaci/Update Zadatak";
            this.buttonIzbaciZadatakIzBaze.UseVisualStyleBackColor = true;
            this.buttonIzbaciZadatakIzBaze.Click += new System.EventHandler(this.buttonIzbaciZadatakIzBaze_Click);
            // 
            // buttonBanujIgraca
            // 
            this.buttonBanujIgraca.Location = new System.Drawing.Point(442, 178);
            this.buttonBanujIgraca.Name = "buttonBanujIgraca";
            this.buttonBanujIgraca.Size = new System.Drawing.Size(187, 23);
            this.buttonBanujIgraca.TabIndex = 37;
            this.buttonBanujIgraca.Text = "BanujIgraca";
            this.buttonBanujIgraca.UseVisualStyleBackColor = true;
            this.buttonBanujIgraca.Click += new System.EventHandler(this.buttonBanujIgraca_Click);
            // 
            // buttonBrisiAlijansu
            // 
            this.buttonBrisiAlijansu.Location = new System.Drawing.Point(442, 226);
            this.buttonBrisiAlijansu.Name = "buttonBrisiAlijansu";
            this.buttonBrisiAlijansu.Size = new System.Drawing.Size(187, 23);
            this.buttonBrisiAlijansu.TabIndex = 38;
            this.buttonBrisiAlijansu.Text = "Brisi Alijansu";
            this.buttonBrisiAlijansu.UseVisualStyleBackColor = true;
            this.buttonBrisiAlijansu.Click += new System.EventHandler(this.buttonBrisiAlijansu_Click);
            // 
            // buttonIzlaz
            // 
            this.buttonIzlaz.Location = new System.Drawing.Point(536, 264);
            this.buttonIzlaz.Name = "buttonIzlaz";
            this.buttonIzlaz.Size = new System.Drawing.Size(93, 23);
            this.buttonIzlaz.TabIndex = 39;
            this.buttonIzlaz.Text = "Izlaz";
            this.buttonIzlaz.UseVisualStyleBackColor = true;
            this.buttonIzlaz.Click += new System.EventHandler(this.buttonIzlaz_Click);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 299);
            this.Controls.Add(this.buttonIzlaz);
            this.Controls.Add(this.buttonBrisiAlijansu);
            this.Controls.Add(this.buttonBanujIgraca);
            this.Controls.Add(this.buttonIzbaciZadatakIzBaze);
            this.Controls.Add(this.buttonIzbaciPrevoznoSredstvoIzBaze);
            this.Controls.Add(this.buttonIzbaciItemIzBaze);
            this.Controls.Add(this.buttonListaIgracaZaKljucniItem);
            this.Controls.Add(this.buttonStaviAljansuNaZadatak);
            this.Controls.Add(this.buttonKreirajSavez);
            this.Controls.Add(this.buttonPregledSesija);
            this.Controls.Add(this.buttonUbaciZadatakUBazu);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.UbaciPrevoznoSredstvoUBazu);
            this.Controls.Add(this.UbaciItemUBazu);
            this.Name = "AdminForm";
            this.Text = "AdminForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UbaciPrevoznoSredstvoUBazu;
        private System.Windows.Forms.Button UbaciItemUBazu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonUbaciZadatakUBazu;
        private System.Windows.Forms.Button buttonPregledSesija;
        private System.Windows.Forms.Button buttonKreirajSavez;
        private System.Windows.Forms.Button buttonStaviAljansuNaZadatak;
        private System.Windows.Forms.Button buttonListaIgracaZaKljucniItem;
        private System.Windows.Forms.Button buttonIzbaciItemIzBaze;
        private System.Windows.Forms.Button buttonIzbaciPrevoznoSredstvoIzBaze;
        private System.Windows.Forms.Button buttonIzbaciZadatakIzBaze;
        private System.Windows.Forms.Button buttonBanujIgraca;
        private System.Windows.Forms.Button buttonBrisiAlijansu;
        private System.Windows.Forms.Button buttonIzlaz;
    }
}