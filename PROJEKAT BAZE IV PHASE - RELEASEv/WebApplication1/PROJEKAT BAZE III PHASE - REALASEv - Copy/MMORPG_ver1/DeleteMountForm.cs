﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class DeleteMountForm : Form
    {
        public IList<PrevoznoSredstvo> ps;
        
        public DeleteMountForm()
        {
            InitializeComponent();

            try
            {
                ISession s = DataLayer.GetSession();

                ps = (from a in s.Query<Entiteti.PrevoznoSredstvo>()
                         select a).ToList<PrevoznoSredstvo>();

                foreach (PrevoznoSredstvo a in ps)
                    listBox1.Items.Add(a.IdPrevoznoSredstvo);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Selektujte item");
            else
            {
                try
                {
                    ISession s = DataLayer.GetSession();
                    PrevoznoSredstvo selektovano = s.Load<PrevoznoSredstvo>(ps[listBox1.SelectedIndex].IdPrevoznoSredstvo);
                    selektovano.PripadaIgracu.PrevoznaSredstva.Remove(selektovano);
                    s.SaveOrUpdate(selektovano.PripadaIgracu);
                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                    s.Delete(selektovano);
                    
                   
                    s.Flush();
                    s.Close();
                    MessageBox.Show("Uspesno ste izbacili prevozno sredstvo iz baze");
                    this.Close();

                }
                catch (Exception ec)
                {
                    // if (ec.Message.Contains("already") || ec.InnerException.Message.Contains("insert"))
                    //   MessageBox.Show("Igrac vec poseduje item");
                    // else
                    MessageBox.Show(ec.Message);
                }
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            InsertMountForm i = new InsertMountForm(ps[listBox1.SelectedIndex].IdPrevoznoSredstvo);
            i.ShowDialog();
        }
    }
}
