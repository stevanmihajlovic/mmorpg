﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public class DataProvider
    {
        public IList<Igrac> GetIgraci()
        {
                ISession s= DataLayer.GetSession();
                IList<Igrac> igraci = s.Query<Igrac>().ToList();
                return igraci;
        }
        public Igrac GetIgrac (int id)
        {
            ISession s = DataLayer.GetSession();
            Igrac i = s.Get<Igrac>(id);
            //Igrac i = s.Query<Igrac>().Where(igrac => (igrac.IdIgrac == id)).Select(igrac => igrac).FirstOrDefault();
            return i;
        }
        public int DeleteIgrac(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Igrac i = s.Load<Igrac>(id);
                s.Delete(i);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdateIgrac(int id, Igrac i)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Igrac selektovan = s.Load<Igrac>(id);
                selektovan.Ime = i.Ime;
                selektovan.Lozinka = i.Lozinka;
                selektovan.Nadimak = i.Nadimak;
                selektovan.Pol = i.Pol;
                selektovan.Prezime = i.Prezime;
                selektovan.Starost = i.Starost;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int AddLikIgrac(LikIgrac i)    //mora sa LIkom da se sinhronizuje
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Igrac igrac = new Igrac();
                Lik lik;
                switch (i.Rasa)
                {
                    case "Vilenjak": lik = new LikVilenjak(); ((LikVilenjak)lik).Energija = 100; break;
                    case "Demon": lik = new LikDemon(); ((LikDemon)lik).Energija = 100; break;
                    case "Ork": lik = new LikOrk(); ((LikOrk)lik).SpecijalizacijaOruzja = 100; break;
                    case "Patuljak": lik = new LikPatuljak(); ((LikPatuljak)lik).SpecijalizacijaOruzja = 100; break;
                    default: lik = new LikCovek(); ((LikCovek)lik).BonusSkrivanja = 100; break;
                }
                igrac.Ime = i.Ime;
                igrac.Lozinka = i.Lozinka;
                igrac.Nadimak = i.Nadimak;
                igrac.Pol = i.Pol;
                igrac.Prezime = i.Prezime;
                igrac.Starost = i.Starost;
                lik.Iskustvo = i.Iskustvo;
                lik.KolicinaZlata = i.KolicinaZlata;
                lik.StepenZamora = i.StepenZamora;
                lik.Zdravlje = i.Zdravlje;
                lik.IdIgraca = igrac;
                igrac.IdLika = lik;
                s.Save(lik);
                s.Save(igrac);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<Lik> GetLikovi()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Lik> lik = s.Query<Lik>().ToList();
            return lik;
        }
        public Lik GetLik(int id)
        {
            ISession s = DataLayer.GetSession();
            Lik i = s.Query<Lik>().Where(lik => (lik.IdLik == id)).Select(lik => lik).FirstOrDefault();
            return i;
        }
        public int AddLik(Lik l)
        {
            try
            {

                ISession s = DataLayer.GetSession();
                
                if (l.GospodarSegrta != null)   //mozemo samo segrte da ubacujemo, lik ide zajedno sa igracem
                {
                    s.SaveOrUpdate(l);
                    s.Flush();
                    s.Close();
                    return 1;
                }
                else
                {
                    s.Flush();
                    s.Close();
                    return -1;
                }
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdateLik(int id, Lik l)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Lik selektovan = s.Load<Lik>(id);
                if (l.GospodarSegrta!=null)
                {
                    selektovan.Ime = l.Ime;
                    selektovan.BonusSkrivanjaSegrta = l.BonusSkrivanjaSegrta;
                }
                else
                {
                    selektovan.Iskustvo = l.Iskustvo;
                    selektovan.KolicinaZlata = l.KolicinaZlata;
                    selektovan.StepenZamora = l.StepenZamora;
                    selektovan.Zdravlje = l.Zdravlje;
                }

                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int DeleteLik(int id)
        {
            try
            {
                
                    ISession s = DataLayer.GetSession();
                    Lik brisi = s.Load<Lik>(id);
                    if (brisi.GospodarSegrta == null)
                    {
                         //ako nema gospodara segrta, onda je to lik pa je logicno da brisemo celog igraca
                        s.Flush();
                        s.Close();
                        return DeleteIgrac(id);
                    }
                    else
                    {
                        brisi.GospodarSegrta.Segrti.Remove(brisi);
                        s.SaveOrUpdate(brisi.GospodarSegrta);
                        s.Delete(brisi);
                        s.Flush();
                        s.Close();
                        return 1;
                    }
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<Alijansa> GetAlijanse()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Alijansa> alijansa = s.Query<Alijansa>().ToList();
            return alijansa;
        }
        public Alijansa GetAlijansa (int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<Alijansa>(id);
        }
        public int AddAlijansa(Alijansa a)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(a);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdateAlijansa(int id, Alijansa a)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Alijansa selektovana = s.Load<Alijansa>(id);
                selektovana.BonusIskustvo = a.BonusIskustvo;
                selektovana.BonusZdravlje = a.BonusZdravlje;
                selektovana.Max = a.Max;
                selektovana.Min = a.Min;
                selektovana.Naziv = a.Naziv;
                
                s.SaveOrUpdate(selektovana);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int DeleteAlijansa(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Alijansa selektovana = s.Load<Alijansa>(id);
                
                foreach (Alijansa a in selektovana.USavezuSa)
                    a.USavezuSa.Remove(selektovana);
                
                s.Delete(selektovana);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<Sesija> GetSesije()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Sesija> sesija = s.Query<Sesija>().ToList();
            return sesija;
        }
        public Sesija GetSesija(int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<Sesija>(id);
        }
        public int AddSesija(Sesija s1)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(s1);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdateSesija(int id, Sesija s1)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Sesija selektovana = s.Load<Sesija>(id);
                selektovana.Iskustvo = s1.Iskustvo;
                selektovana.VremeKraja = s1.VremeKraja;
                selektovana.VremePocetka = s1.VremePocetka;
                selektovana.Zlato = s1.Zlato;
                s.SaveOrUpdate(selektovana);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int DeleteSesija(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Sesija selektovan = s.Load<Sesija>(id);

                selektovan.VezanaZaIgraca.Sesije.Remove(selektovan);
                s.SaveOrUpdate(selektovan.VezanaZaIgraca);

                s.Delete(selektovan);

                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        //nema smisla brisati,dodavati,updatovati sesije jer se kreiraju cim se uloguje igrac. za njih imamo samo pregled al evo ubacili smo
        public IEnumerable<PrevoznoSredstvo> GetPrevoznaSredstva()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<PrevoznoSredstvo> prevoznaSredstva = s.Query<PrevoznoSredstvo>().ToList();
            return prevoznaSredstva;
        }

        public PrevoznoSredstvo GetPrevoznoSredstvo(int id)
        {
            ISession s = DataLayer.GetSession();
            PrevoznoSredstvo i = s.Query<PrevoznoSredstvo>().Where(ps => (ps.IdPrevoznoSredstvo == id)).Select(ps => ps).FirstOrDefault();
            return i;
        }
        public int DeletePrevoznoSredstvo(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                PrevoznoSredstvo selektovano = s.Load<PrevoznoSredstvo>(id);
                selektovano.PripadaIgracu.PrevoznaSredstva.Remove(selektovano);
                s.SaveOrUpdate(selektovano.PripadaIgracu);
                
                s.Delete(selektovano);
                
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<PrevoznoSredstvoZmaj> GetPrevoznaSredstvaZmaj()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<PrevoznoSredstvoZmaj> prevoznaSredstva = s.Query<PrevoznoSredstvoZmaj>().ToList();
            return prevoznaSredstva;
        }

        public PrevoznoSredstvoZmaj GetPrevoznoSredstvoZmaj(int id)
        {
            ISession s = DataLayer.GetSession();
            PrevoznoSredstvoZmaj i = s.Query<PrevoznoSredstvoZmaj>().Where(ps => (ps.IdPrevoznoSredstvo == id)).Select(ps => ps).FirstOrDefault();
            return i;
        }
        public int AddPrevoznoSredstvoZmaj(PrevoznoSredstvoZmaj ps)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(ps);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdatePrevoznoSredstvoZmaj(int id, PrevoznoSredstvoZmaj ps)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                PrevoznoSredstvoZmaj selektovan = s.Load<PrevoznoSredstvoZmaj>(id);
                selektovan.Brzina = ps.Brzina;
                selektovan.Cena = ps.Cena;
                selektovan.Naziv = ps.Naziv;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int DeletePrevoznoSredstvoZmaj(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                PrevoznoSredstvoZmaj selektovano = s.Load<PrevoznoSredstvoZmaj>(id);
                if (selektovano.PripadaIgracu != null)
                {
                    selektovano.PripadaIgracu.PrevoznaSredstva.Remove(selektovano);
                    s.SaveOrUpdate(selektovano.PripadaIgracu);
                }

                s.Delete(selektovano);

                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<PrevoznoSredstvoZivotinja> GetPrevoznaSredstvaZivotinja()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<PrevoznoSredstvoZivotinja> prevoznaSredstva = s.Query<PrevoznoSredstvoZivotinja>().ToList();
            return prevoznaSredstva;
        }

        public PrevoznoSredstvoZivotinja GetPrevoznoSredstvoZivotinja(int id)
        {
            ISession s = DataLayer.GetSession();
            PrevoznoSredstvoZivotinja i = s.Query<PrevoznoSredstvoZivotinja>().Where(ps => (ps.IdPrevoznoSredstvo == id)).Select(ps => ps).FirstOrDefault();
            return i;
        }
        public int AddPrevoznoSredstvoZivotinja(PrevoznoSredstvoZivotinja ps)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(ps);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdatePrevoznoSredstvoZivotinja(int id, PrevoznoSredstvoZivotinja ps)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                PrevoznoSredstvoZivotinja selektovan = s.Load<PrevoznoSredstvoZivotinja>(id);
                selektovan.Brzina = ps.Brzina;
                selektovan.Cena = ps.Cena;
                selektovan.Naziv = ps.Naziv;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int DeletePrevoznoSredstvoZivotinja(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                PrevoznoSredstvoZivotinja selektovano = s.Load<PrevoznoSredstvoZivotinja>(id);
                if (selektovano.PripadaIgracu != null)
                {
                    selektovano.PripadaIgracu.PrevoznaSredstva.Remove(selektovano);
                    s.SaveOrUpdate(selektovano.PripadaIgracu);
                }

                s.Delete(selektovano);

                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<PrevoznoSredstvoMehanicki> GetPrevoznaSredstvaMehanicki()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<PrevoznoSredstvoMehanicki> prevoznaSredstva = s.Query<PrevoznoSredstvoMehanicki>().ToList();
            return prevoznaSredstva;
        }

        public PrevoznoSredstvoMehanicki GetPrevoznoSredstvoMehanicki(int id)
        {
            ISession s = DataLayer.GetSession();
            PrevoznoSredstvoMehanicki i = s.Query<PrevoznoSredstvoMehanicki>().Where(ps => (ps.IdPrevoznoSredstvo == id)).Select(ps => ps).FirstOrDefault();
            return i;
        }
        public int AddPrevoznoSredstvoMehanicki(PrevoznoSredstvoMehanicki ps)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(ps);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdatePrevoznoSredstvoMehanicki(int id, PrevoznoSredstvoMehanicki ps)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                PrevoznoSredstvoMehanicki selektovan = s.Load<PrevoznoSredstvoMehanicki>(id);
                selektovan.Brzina = ps.Brzina;
                selektovan.Cena = ps.Cena;
                selektovan.Naziv = ps.Naziv;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int DeletePrevoznoSredstvoMehanicki(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                PrevoznoSredstvoMehanicki selektovano = s.Load<PrevoznoSredstvoMehanicki>(id);
                if (selektovano.PripadaIgracu != null)
                {
                    selektovano.PripadaIgracu.PrevoznaSredstva.Remove(selektovano);
                    s.SaveOrUpdate(selektovano.PripadaIgracu);
                }

                s.Delete(selektovano);

                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<Item> GetItemi()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Item> itemi = s.Query<Item>().ToList();
            return itemi;
        }

        public Item GetItem(int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<Item>(id);
        }
        public int DeleteItem(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Item selektovan = s.Load<Item>(id);
                
                foreach (Igrac i in selektovan.Igraci)
                {
                    i.Itemi.Remove(selektovan);
                    s.SaveOrUpdate(i);  //mozda i ne mora zbog cascade al me mrzi da gledam
                }
                                
                s.Delete(selektovan);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<OruzjeKljucniItem> GetOruzjeKljucniItemi()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<OruzjeKljucniItem> itemi = s.Query<OruzjeKljucniItem>().ToList();
            return itemi;
        }

        public OruzjeKljucniItem GetOruzjeKljucniItem(int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<OruzjeKljucniItem>(id);
        }
        public int DeleteOruzjeKljucniItem(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                OruzjeKljucniItem selektovan = s.Load<OruzjeKljucniItem>(id);

                foreach (Igrac i in selektovan.Igraci)
                {
                    i.Itemi.Remove(selektovan);
                    s.SaveOrUpdate(i);  //mozda i ne mora zbog cascade al me mrzi da gledam
                }

                s.Delete(selektovan);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int AddOruzjeKljucniItem(OruzjeKljucniItem i)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(i);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdateOruzjeKljucniItem(int id, OruzjeKljucniItem i)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                OruzjeKljucniItem selektovan = s.Load<OruzjeKljucniItem>(id);
                selektovan.Ime = i.Ime;
                selektovan.Opis = i.Opis;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<PredmetKljucniItem> GetPredmetKljucniItemi()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<PredmetKljucniItem> itemi = s.Query<PredmetKljucniItem>().ToList();
            return itemi;
        }

        public PredmetKljucniItem GetPredmetKljucniItem(int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<PredmetKljucniItem>(id);
        }
        public int DeletePredmetKljucniItem(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PredmetKljucniItem selektovan = s.Load<PredmetKljucniItem>(id);

                foreach (Igrac i in selektovan.Igraci)
                {
                    i.Itemi.Remove(selektovan);
                    s.SaveOrUpdate(i);  //mozda i ne mora zbog cascade al me mrzi da gledam
                }

                s.Delete(selektovan);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int AddPredmetKljucniItem(PredmetKljucniItem i)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(i);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdatePredmetKljucniItem(int id, PredmetKljucniItem i)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                PredmetKljucniItem selektovan = s.Load<PredmetKljucniItem>(id);
                selektovan.Ime = i.Ime;
                selektovan.Opis = i.Opis;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<PredmetBonusItem> GetPredmetBonusItemi()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<PredmetBonusItem> itemi = s.Query<PredmetBonusItem>().ToList();
            return itemi;
        }

        public PredmetBonusItem GetPredmetBonusItem(int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<PredmetBonusItem>(id);
        }
        public int DeletePredmetBonusItem(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PredmetBonusItem selektovan = s.Load<PredmetBonusItem>(id);

                foreach (Igrac i in selektovan.Igraci)
                {
                    i.Itemi.Remove(selektovan);
                    s.SaveOrUpdate(i);  //mozda i ne mora zbog cascade al me mrzi da gledam
                }

                s.Delete(selektovan);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int AddPredmetBonusItem(PredmetBonusItem i)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(i);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdatePredmetBonusItem(int id, PredmetBonusItem i)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                PredmetBonusItem selektovan = s.Load<PredmetBonusItem>(id);
                selektovan.Iskustvo = i.Iskustvo;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<OruzjeBonusItem> GetOruzjeBonusItemi()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<OruzjeBonusItem> itemi = s.Query<OruzjeBonusItem>().ToList();
            return itemi;
        }

        public OruzjeBonusItem GetOruzjeBonusItem(int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<OruzjeBonusItem>(id);
        }
        public int DeleteOruzjeBonusItem(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                OruzjeBonusItem selektovan = s.Load<OruzjeBonusItem>(id);

                foreach (Igrac i in selektovan.Igraci)
                {
                    i.Itemi.Remove(selektovan);
                    s.SaveOrUpdate(i);  //mozda i ne mora zbog cascade al me mrzi da gledam
                }

                s.Delete(selektovan);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int AddOruzjeBonusItem(OruzjeBonusItem i)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(i);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdateOruzjeBonusItem(int id, OruzjeBonusItem i)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                OruzjeBonusItem selektovan = s.Load<OruzjeBonusItem>(id);
                selektovan.Iskustvo = i.Iskustvo;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public IEnumerable<Zadatak> GetZadaci()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Zadatak> zadaci = s.Query<Zadatak>().ToList();
            return zadaci;
        }

        public Zadatak GetZadatak(int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<Zadatak>(id);
        }
        public int DeleteZadatak(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Zadatak selektovan = s.Load<Zadatak>(id);
                Zadatak selektovan1 = (Zadatak)s.GetSessionImplementation().PersistenceContext.Unproxy(selektovan);
                
                if (selektovan1.GetType() == typeof(SoloZadatak))
                    foreach (Igrac i in ((SoloZadatak)selektovan).Solo)
                    {
                        i.Zadaci.Remove((SoloZadatak)selektovan);
                        s.SaveOrUpdate(i);
                    }

                if (selektovan1.GetType() == typeof(GrupniZadatak))
                    foreach (Alijansa a in ((GrupniZadatak)selektovan).Grupni)
                    {
                        a.Zadaci.Remove((GrupniZadatak)selektovan);
                        s.SaveOrUpdate(a);
                    }
                
                s.Delete(selektovan);

                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdateZadatak(int id, Zadatak z)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Zadatak selektovan = s.Load<Zadatak>(id);
                selektovan.BonusIskustvo = z.BonusIskustvo;
                selektovan.VremeKraja = z.VremeKraja;
                selektovan.VremePocetka = z.VremePocetka;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<GrupniZadatak> GetGrupniZadaci()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<GrupniZadatak> zadaci = s.Query<GrupniZadatak>().ToList();
            return zadaci;
        }

        public GrupniZadatak GetGrupniZadatak(int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<GrupniZadatak>(id);
        }
        public int DeleteGrupniZadatak(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                GrupniZadatak selektovan = s.Load<GrupniZadatak>(id);
                
                    foreach (Alijansa a in selektovan.Grupni)
                    {
                        a.Zadaci.Remove(selektovan);
                        s.SaveOrUpdate(a);
                    }

                s.Delete(selektovan);

                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int AddGrupniZadatak(GrupniZadatak z)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(z);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdateGrupniZadatak(int id, GrupniZadatak z)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                GrupniZadatak selektovan = s.Load<GrupniZadatak>(id);
                selektovan.BonusIskustvo = z.BonusIskustvo;
                selektovan.VremeKraja = z.VremeKraja;
                selektovan.VremePocetka = z.VremePocetka;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<SoloZadatak> GetSoloZadaci()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<SoloZadatak> zadaci = s.Query<SoloZadatak>().ToList();
            return zadaci;
        }

        public SoloZadatak GetSoloZadatak(int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<SoloZadatak>(id);
        }
        public int DeleteSoloZadatak(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                SoloZadatak selektovan = s.Load<SoloZadatak>(id);

                foreach (Igrac a in selektovan.Solo)
                {
                    a.Zadaci.Remove(selektovan);
                    s.SaveOrUpdate(a);
                }

                s.Delete(selektovan);

                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int AddSoloZadatak(SoloZadatak z)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(z);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public int UpdateSoloZadatak(int id, SoloZadatak z)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                SoloZadatak selektovan = s.Load<SoloZadatak>(id);
                selektovan.BonusIskustvo = z.BonusIskustvo;
                selektovan.VremeKraja = z.VremeKraja;
                selektovan.VremePocetka = z.VremePocetka;
                s.SaveOrUpdate(selektovan);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }
        public IEnumerable<Rasa> GetRase()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Rasa> rase = s.Query<Rasa>().ToList();
            return rase;
        }

        public Rasa GetRasa(int id)
        {
            ISession s = DataLayer.GetSession();
            return s.Get<Rasa>(id);
        }
        //nema smisla brisati ovo...
    }
}
