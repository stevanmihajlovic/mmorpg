﻿namespace MMORPG_ver1
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNick = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxLozinka = new System.Windows.Forms.TextBox();
            this.PrijaviteSe = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 55);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(81, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Unesite Nick:";
            // 
            // textBoxNick
            // 
            this.textBoxNick.Location = new System.Drawing.Point(132, 58);
            this.textBoxNick.Name = "textBoxNick";
            this.textBoxNick.Size = new System.Drawing.Size(121, 20);
            this.textBoxNick.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 81);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(96, 23);
            this.label2.TabIndex = 12;
            this.label2.Text = "Unesite Lozinku:";
            // 
            // textBoxLozinka
            // 
            this.textBoxLozinka.Location = new System.Drawing.Point(132, 84);
            this.textBoxLozinka.Name = "textBoxLozinka";
            this.textBoxLozinka.PasswordChar = '*';
            this.textBoxLozinka.Size = new System.Drawing.Size(121, 20);
            this.textBoxLozinka.TabIndex = 13;
            // 
            // PrijaviteSe
            // 
            this.PrijaviteSe.Location = new System.Drawing.Point(163, 135);
            this.PrijaviteSe.Name = "PrijaviteSe";
            this.PrijaviteSe.Size = new System.Drawing.Size(89, 23);
            this.PrijaviteSe.TabIndex = 14;
            this.PrijaviteSe.Text = "Prijavite se";
            this.PrijaviteSe.UseVisualStyleBackColor = true;
            this.PrijaviteSe.Click += new System.EventHandler(this.PrijaviteSe_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(79, 9);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(10);
            this.label3.Size = new System.Drawing.Size(112, 33);
            this.label3.TabIndex = 15;
            this.label3.Text = "PRIJAVLJIVANJE";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 187);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PrijaviteSe);
            this.Controls.Add(this.textBoxLozinka);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxNick);
            this.Controls.Add(this.label1);
            this.Name = "LoginForm";
            this.Text = "LoginForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNick;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxLozinka;
        private System.Windows.Forms.Button PrijaviteSe;
        private System.Windows.Forms.Label label3;
    }
}