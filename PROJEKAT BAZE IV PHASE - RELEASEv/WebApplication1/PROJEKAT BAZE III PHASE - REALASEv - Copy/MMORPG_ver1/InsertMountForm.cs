﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;

namespace MMORPG_ver1
{
    public partial class InsertMountForm : Form
    {
        public int idprevoznogsredstvazaupdate = 0;

        public Entiteti.PrevoznoSredstvo p;

        public InsertMountForm()
        {
            InitializeComponent();
        }
        public InsertMountForm(int a)
        {
            InitializeComponent();
            idprevoznogsredstvazaupdate = a;
            try
            {
                ISession s = DataLayer.GetSession();
                p = s.Get<PrevoznoSredstvo>(idprevoznogsredstvazaupdate); //ZBOG PROXYA!
                if (p.GetType() == typeof(PrevoznoSredstvoMehanicki))
                {
                    radioButtonMehanicki.Checked = true;
                    radioButtonZivotinja.Checked = false;
                    radioButtonZmaj.Checked = false;
                }
                if (p.GetType() == typeof(PrevoznoSredstvoZivotinja))
                {
                    radioButtonMehanicki.Checked = false;
                    radioButtonZivotinja.Checked = true;
                    radioButtonZmaj.Checked = false;
                }
                if (p.GetType() == typeof(PrevoznoSredstvoZmaj))
                {
                    radioButtonMehanicki.Checked = false;
                    radioButtonZivotinja.Checked = false;
                    radioButtonZmaj.Checked = true;
                }
                radioButtonMehanicki.Visible = false;
                radioButtonZivotinja.Visible = false;
                radioButtonZmaj.Visible = false;
                label2.Visible = false;
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxNaziv.BackColor = Color.White;
            textBoxBrzina.BackColor = Color.White;
            if (textBoxNaziv.TextLength > 0)
            {
                try
                {
                    ISession s = DataLayer.GetSession();

                    
                    int pom = 0;

                    if (idprevoznogsredstvazaupdate == 0)
                    {
                        if (radioButtonMehanicki.Checked)
                            pom = 1;
                        if (radioButtonZivotinja.Checked)
                            pom = 2;
                        if (radioButtonZmaj.Checked)
                            pom = 3;

                        switch (pom)
                        {
                            case 1:
                                {
                                    p = new PrevoznoSredstvoMehanicki();
                                    break;
                                }
                            case 2:
                                {
                                    p = new PrevoznoSredstvoZivotinja();
                                    break;
                                }
                            default:
                                {
                                    p = new PrevoznoSredstvoZmaj();
                                    break;
                                }
                        }
                    }

                    p.Naziv = textBoxNaziv.Text;
                    p.Brzina = Convert.ToInt32(textBoxBrzina.Text);
                    p.Cena = Convert.ToInt32(textBoxCena.Text);

                    s.SaveOrUpdate(p);

                    s.Flush();
                    s.Close();

                    if (idprevoznogsredstvazaupdate == 0)
                        MessageBox.Show("Uspešno ste ubacili prevozno sredstvo u bazu");
                    else
                        MessageBox.Show("Uspešno ste updatovali prevozno sredstvo");
                    this.Close();


                }
                catch (System.FormatException ec)
                {
                    MessageBox.Show("Unesite za brzinu broj od 0 do 100, a za cenu broj od 0 do 1000");
                    textBoxBrzina.BackColor = Color.IndianRed;
                    textBoxCena.BackColor = Color.IndianRed;
                }
                catch (Exception ec)
                {
                    MessageBox.Show("Neobradjeni prekid, presli ste nas");
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Unesite naziv prevoznog sredstva");
                textBoxNaziv.BackColor = Color.IndianRed;
            }

        }
    }
}
