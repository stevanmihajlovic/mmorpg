﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;

namespace MMORPG_ver1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void DodajIgraca_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.Igrac p = new Entiteti.Igrac();
                Entiteti.LikDemon c = new Entiteti.LikDemon();

                //p = s.Load<Entiteti.Prodavnica>(81);

                p.Ime = "Nikola";
                p.Prezime = "Mihajlovic";
                p.Pol = 'M';
                p.Starost = 17;
                p.Nadimak = "Nikolica";
                p.Lozinka = "Nikola123";


             //   c.BonusSkrivanjaSegrta = 0;
                c.Energija = 100;
                c.IdIgraca = p;
            //    c.Ime = "Ashbringer";
                c.Iskustvo = 1000;
                c.Zdravlje = 100;
                c.StepenZamora = 0;
                
                c.KolicinaZlata = 10000;
               // p.IdLika = c;



                p.IdLika = c;


                //s.Save(p);
                s.SaveOrUpdate(c);
                s.SaveOrUpdate(p);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void DodajZadatak_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.SoloZadatak q = new Entiteti.SoloZadatak();
               
                q.BonusIskustvo = 100;
                q.VremePocetka = DateTime.Now;
                q.VremeKraja = DateTime.Now;
                s.SaveOrUpdate(q);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }

        }

        private void DodajAlijansu_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Alijansa a = new Entiteti.Alijansa();
                a.Min = 2;
                a.Max = 10;
                a.Naziv = "Pure";
                a.BonusIskustvo = 100;
                //KAKO UBACITI IGRACE I SAVEZE?

                s.SaveOrUpdate(a);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.GrupniZadatak q = new Entiteti.GrupniZadatak();

                q.BonusIskustvo = 100;
                q.VremePocetka = DateTime.Now;
                q.VremeKraja = DateTime.Now;
                s.SaveOrUpdate(q);
                
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.PrevoznoSredstvoZmaj m = new Entiteti.PrevoznoSredstvoZmaj();

                m.Brzina = 10;
                m.Naziv = "Zmajcek";
                
                
                s.SaveOrUpdate(m);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Sesija S = new Entiteti.Sesija();

                S.Iskustvo = 50;
                S.VremePocetka = DateTime.Now;
                S.Zlato = 100;


                s.SaveOrUpdate(S);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.OruzjeKljucniItem i1 = new Entiteti.OruzjeKljucniItem();

                i1.Ime = "Toljaga";
                i1.Opis = "Obican napad";
                
                


                s.SaveOrUpdate(i1);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void UpdateIgrac_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Igrac i = s.Load<Igrac>(22);

                s.Close();
                               
                i.Nadimak = "CheryKiss";
                i.Lozinka = "Nova123";

                ISession s1 = DataLayer.GetSession();

                s1.Update(i);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void UpdateZadatak_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.SoloZadatak z = s.Load<SoloZadatak>(23);
                s.Close();

                z.BonusIskustvo = 150;
                
                
                ISession s1 = DataLayer.GetSession();

                s1.Update(z);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button1_Click_4(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Alijansa a = s.Load<Alijansa>(22);

                s.Close();
                
                a.Min = 4;
                a.Max = 15;
             
                
                
                ISession s1 = DataLayer.GetSession();

                s1.Update(a);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void IzbrisiIgraca_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Igrac i = s.Load<Igrac>(22); 

                s.Delete(i);

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void IzbrisiZadatak_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.SoloZadatak z = s.Load<SoloZadatak>(23); 

                s.Delete(z);

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void IzbrisiAlijansu_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Alijansa a = s.Load<Alijansa>(22);  

                s.Delete(a);

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void IstaknutoEXIT_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click_5(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.GrupniZadatak i = s.Load<GrupniZadatak>(22);

                s.Delete(i);

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void UpdatePrevoz_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.PrevoznoSredstvoZmaj z = s.Load<PrevoznoSredstvoZmaj>(21);
                s.Close();

                z.Brzina = 80;


                ISession s1 = DataLayer.GetSession();

                s1.Update(z);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void UpdateGrupni_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.GrupniZadatak z = s.Load<GrupniZadatak>(22);
                s.Close();

                z.BonusIskustvo = 25;


                ISession s1 = DataLayer.GetSession();

                s1.Update(z);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void UpdateSesiju_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Sesija z = s.Load<Sesija>(21);
                s.Close();

                z.Iskustvo = 15;


                ISession s1 = DataLayer.GetSession();

                s1.Update(z);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void UpdateItem_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.OruzjeKljucniItem z = s.Load<OruzjeKljucniItem>(21);
                s.Close();

                z.Ime = "FrostMourne";


                ISession s1 = DataLayer.GetSession();

                s1.Update(z);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void IzbrisiPrevoz_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.PrevoznoSredstvoZmaj i = s.Load<PrevoznoSredstvoZmaj>(21);

                s.Delete(i);

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void IzbrisiSesiju_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Sesija i = s.Load<Sesija>(21);

                s.Delete(i);

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void IzbrisiItem_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.OruzjeKljucniItem i = s.Load<OruzjeKljucniItem>(21);

                s.Delete(i);

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void RegisrujNovogIgraca_Click(object sender, EventArgs e)
        {
            RegisterForm nova = new RegisterForm();
            nova.Show();
        }

        private void UbaciItemUBazu_Click(object sender, EventArgs e)
        {
            InsertItemForm nova = new InsertItemForm();
            nova.Show();
        }

        private void UbaciPrevoznoSredstvoUBazu_Click(object sender, EventArgs e)
        {
            InsertMountForm nova = new InsertMountForm();
            nova.Show();
        }

        private void UbaciAlijansuUBazu_Click(object sender, EventArgs e)
        {
            CreateAlianceForm nova = new CreateAlianceForm();
            nova.Show();
        }

        
    }
}
