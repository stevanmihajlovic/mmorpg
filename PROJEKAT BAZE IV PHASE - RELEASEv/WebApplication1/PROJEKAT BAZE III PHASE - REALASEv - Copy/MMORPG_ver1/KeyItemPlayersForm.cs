﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class KeyItemPlayersForm : Form
    {
        public IList<Item> itemi;
        public IList<Igrac> igraci;
        public KeyItemPlayersForm()
        {
            InitializeComponent();
            try
            {
                ISession s = DataLayer.GetSession();

                itemi = (from a in s.Query<Entiteti.Item>()
                          select a).ToList<Item>();
                if (itemi.Count > 0)
                {
                    foreach (Item a in itemi)
                    {
                        if (a.GetType() == typeof(OruzjeKljucniItem) || a.GetType() == typeof(PredmetKljucniItem))
                            listBox1.Items.Add(a.IdItem);
                    }
                }
                else
                    MessageBox.Show("Nema igraca");
                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show("Neobradjena greska");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            try
            {
                ISession s = DataLayer.GetSession();
                s.Lock(itemi[listBox1.SelectedIndex], LockMode.None);
                igraci = (from a in s.Query<Entiteti.Igrac>()
                          where (a.Itemi.Contains(itemi[listBox1.SelectedIndex]))
                          select a).ToList<Igrac>();
                if (igraci.Count > 0)
                {
                    foreach (Igrac a in igraci)
                    {
                        listBox2.Items.Add(a.Nadimak);
                    }
                }
                else
                    MessageBox.Show("Nema igraca");
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show("Neobradjena greska");
            }
        }
    }
}
