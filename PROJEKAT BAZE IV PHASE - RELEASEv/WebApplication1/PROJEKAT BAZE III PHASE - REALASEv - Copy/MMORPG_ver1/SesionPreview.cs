﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class SesionPreview : Form
    {
        public IList<Igrac> igraci;
        public IList<Sesija> sesije;
        public SesionPreview()
        {
            
            InitializeComponent();
            try
            {
                ISession s = DataLayer.GetSession();

                igraci = (from a in s.Query<Entiteti.Igrac>()
                            where (a.Sesije.Count > 0)
                            select a).ToList<Igrac>();
                if (igraci.Count > 0)
                {
                    foreach (Igrac a in igraci)
                    {
                        listBox1.Items.Add(a.Nadimak);
                    }
                }
                else
                    MessageBox.Show("Nema igraca");
                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show("Neobradjena greska");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            try
            {
                ISession s = DataLayer.GetSession();
                s.Lock(igraci[listBox1.SelectedIndex], LockMode.None);
                sesije = (from a in s.Query<Entiteti.Sesija>()
                          where (a.VezanaZaIgraca == igraci[listBox1.SelectedIndex])
                          select a).ToList<Sesija>();
                if (sesije.Count > 0)
                {
                    foreach (Sesija a in sesije)
                    {
                        listBox2.Items.Add(a.IdSesija.ToString() + "\t VP: " + a.VremePocetka.ToString() + "\t VK: " + a.VremeKraja.ToString());
                    }
                }
                else
                    MessageBox.Show("Nema sesija");
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show("Neobradjena greska");
            }
        }
    }
}
