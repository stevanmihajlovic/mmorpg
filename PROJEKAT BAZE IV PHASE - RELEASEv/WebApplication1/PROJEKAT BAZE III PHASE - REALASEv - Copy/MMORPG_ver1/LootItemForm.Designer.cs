﻿namespace MMORPG_ver1
{
    partial class LootItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonIzlaz = new System.Windows.Forms.Button();
            this.buttonPokupiteItem = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 42);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(199, 147);
            this.listBox1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(44, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Itemi:";
            // 
            // buttonIzlaz
            // 
            this.buttonIzlaz.Location = new System.Drawing.Point(136, 247);
            this.buttonIzlaz.Name = "buttonIzlaz";
            this.buttonIzlaz.Size = new System.Drawing.Size(75, 23);
            this.buttonIzlaz.TabIndex = 7;
            this.buttonIzlaz.Text = "Izlaz";
            this.buttonIzlaz.UseVisualStyleBackColor = true;
            this.buttonIzlaz.Click += new System.EventHandler(this.buttonIzlaz_Click);
            // 
            // buttonPokupiteItem
            // 
            this.buttonPokupiteItem.Location = new System.Drawing.Point(12, 205);
            this.buttonPokupiteItem.Name = "buttonPokupiteItem";
            this.buttonPokupiteItem.Size = new System.Drawing.Size(199, 23);
            this.buttonPokupiteItem.TabIndex = 6;
            this.buttonPokupiteItem.Text = "Pokupite Item";
            this.buttonPokupiteItem.UseVisualStyleBackColor = true;
            this.buttonPokupiteItem.Click += new System.EventHandler(this.buttonPokupiteItem_Click);
            // 
            // LootItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(229, 302);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonIzlaz);
            this.Controls.Add(this.buttonPokupiteItem);
            this.Controls.Add(this.listBox1);
            this.Name = "LootItemForm";
            this.Text = "LootItemForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonIzlaz;
        private System.Windows.Forms.Button buttonPokupiteItem;
    }
}