﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class AlianceChooseQuestForm : Form
    {
        public IList<GrupniZadatak> zadaci;
        public IList<Alijansa> alijanse;
        public IList<GrupniZadatak> listazadatakanakojimaNIJEalijansa;

        public AlianceChooseQuestForm()
        {
            InitializeComponent();
            try
            {
                ISession s = DataLayer.GetSession();

                alijanse = (from a in s.Query<Entiteti.Alijansa>()
                            select a).ToList<Alijansa>();
                zadaci = (from a in s.Query<Entiteti.GrupniZadatak>()
                          select a).ToList<GrupniZadatak>();
                if (alijanse.Count > 0)
                {
                    foreach (Alijansa a in alijanse)
                    {
                        listBox1.Items.Add(a.Naziv);
                    }
                }
                else
                {
                    MessageBox.Show("Nema alijansi");
                    
                }

                

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);    //verovatno ce da baci exception ako vec postoji taj par alijansa-zadatak
            }
        }

        private void buttonStaviNaZadatak_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex == -1)
                MessageBox.Show("Izaberite Alijansu");
            else
            {
                try
                {
                    ISession s = DataLayer.GetSession();

                    s.Lock(alijanse[listBox1.SelectedIndex], LockMode.None);
                    s.Lock(listazadatakanakojimaNIJEalijansa[listBox2.SelectedIndex], LockMode.None);

                    alijanse[listBox1.SelectedIndex].Zadaci.Add(listazadatakanakojimaNIJEalijansa[listBox2.SelectedIndex]);
                    (listazadatakanakojimaNIJEalijansa[listBox2.SelectedIndex]).Grupni.Add(alijanse[listBox1.SelectedIndex]);

                    s.SaveOrUpdate(alijanse[listBox1.SelectedIndex]);   //zadatak ima inverse i nepotreno je sacuvati ga

                    s.Flush();
                    s.Close();
                    MessageBox.Show("Alijansa je sada na zadatku");
                    this.Close();
                }
                catch (Exception ec)
                {
                    if (ec.InnerException.Message.Contains("insert"))
                        MessageBox.Show("Alijansa je vec na zadatku");
                    else
                        MessageBox.Show(ec.Message);
                }
            }
        }

        private void buttonIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            try
            {
                ISession s = DataLayer.GetSession();
                
                Alijansa selektovana = alijanse[listBox1.SelectedIndex];
                s.Lock(selektovana, LockMode.None);

                foreach (GrupniZadatak g in selektovana.Zadaci)
                {
                    listBox3.Items.Add(g.IdZadatak);
                }
                
                IList<int> IdzadatakanakojimaJEalijansa = selektovana.Zadaci.Select(x => x.IdZadatak).ToList();

                listazadatakanakojimaNIJEalijansa = zadaci.Where(x => !IdzadatakanakojimaJEalijansa.Contains(x.IdZadatak)).ToList();

                if (listazadatakanakojimaNIJEalijansa.Count > 0)
                {
                    foreach (GrupniZadatak a in listazadatakanakojimaNIJEalijansa)   //kako proveriti da vec nije na tom zadatku?
                    {
                        listBox2.Items.Add(a.IdZadatak);
                    }
                }
                else
                    MessageBox.Show("Nema grupnih zadataka koji su dostupni alijansi");
                
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void buttonZavrsiZadatak_Click(object sender, EventArgs e)
        {
            if (listBox3.SelectedIndex == -1)
                MessageBox.Show("Izaberite zadatak koji želite da završite");
            else
            {
                try
                {
                    ISession s = DataLayer.GetSession();
                    Alijansa selektovana = alijanse[listBox1.SelectedIndex];
                    s.Lock(selektovana, LockMode.None);

                    selektovana.Zadaci[listBox3.SelectedIndex].VremeKraja = DateTime.Now;
                    foreach (Igrac i in selektovana.Igraci) //kad alijansa zavrsi zadatak, svi clanovi dobijaju bonus
                    {
                        i.IdLika.KolicinaZlata += 100;
                        i.IdLika.Iskustvo += selektovana.Zadaci[listBox3.SelectedIndex].BonusIskustvo;
                    }
                    selektovana.Zadaci.RemoveAt(listBox3.SelectedIndex);

                    s.SaveOrUpdate(selektovana);
                    s.Flush();
                    s.Close();
                    MessageBox.Show("Uspesno ste zavrsili zadatak");
                    this.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }
            }
        }
    }
}
