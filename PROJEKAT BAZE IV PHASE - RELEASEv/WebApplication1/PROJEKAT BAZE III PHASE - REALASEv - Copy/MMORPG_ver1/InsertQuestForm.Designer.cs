﻿namespace MMORPG_ver1
{
    partial class InsertQuestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonGrupni = new System.Windows.Forms.RadioButton();
            this.radioButtonSolo = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxIskustvo = new System.Windows.Forms.TextBox();
            this.buttonUbaciZadatak = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // radioButtonGrupni
            // 
            this.radioButtonGrupni.AutoSize = true;
            this.radioButtonGrupni.Checked = true;
            this.radioButtonGrupni.Location = new System.Drawing.Point(150, 29);
            this.radioButtonGrupni.Name = "radioButtonGrupni";
            this.radioButtonGrupni.Size = new System.Drawing.Size(56, 17);
            this.radioButtonGrupni.TabIndex = 0;
            this.radioButtonGrupni.TabStop = true;
            this.radioButtonGrupni.Text = "Grupni";
            this.radioButtonGrupni.UseVisualStyleBackColor = true;
            // 
            // radioButtonSolo
            // 
            this.radioButtonSolo.AutoSize = true;
            this.radioButtonSolo.Location = new System.Drawing.Point(212, 29);
            this.radioButtonSolo.Name = "radioButtonSolo";
            this.radioButtonSolo.Size = new System.Drawing.Size(46, 17);
            this.radioButtonSolo.TabIndex = 1;
            this.radioButtonSolo.Text = "Solo";
            this.radioButtonSolo.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Izaberite Tip Zadatka: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Unesite Bonus za Iskustvo: ";
            // 
            // textBoxIskustvo
            // 
            this.textBoxIskustvo.Location = new System.Drawing.Point(158, 57);
            this.textBoxIskustvo.Name = "textBoxIskustvo";
            this.textBoxIskustvo.Size = new System.Drawing.Size(100, 20);
            this.textBoxIskustvo.TabIndex = 4;
            // 
            // buttonUbaciZadatak
            // 
            this.buttonUbaciZadatak.Location = new System.Drawing.Point(158, 97);
            this.buttonUbaciZadatak.Name = "buttonUbaciZadatak";
            this.buttonUbaciZadatak.Size = new System.Drawing.Size(100, 23);
            this.buttonUbaciZadatak.TabIndex = 5;
            this.buttonUbaciZadatak.Text = "Ubaci Zadatak";
            this.buttonUbaciZadatak.UseVisualStyleBackColor = true;
            this.buttonUbaciZadatak.Click += new System.EventHandler(this.buttonUbaciZadatak_Click);
            // 
            // InsertQuestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 132);
            this.Controls.Add(this.buttonUbaciZadatak);
            this.Controls.Add(this.textBoxIskustvo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButtonSolo);
            this.Controls.Add(this.radioButtonGrupni);
            this.Name = "InsertQuestForm";
            this.Text = "InsertQuestForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonGrupni;
        private System.Windows.Forms.RadioButton radioButtonSolo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxIskustvo;
        private System.Windows.Forms.Button buttonUbaciZadatak;
    }
}