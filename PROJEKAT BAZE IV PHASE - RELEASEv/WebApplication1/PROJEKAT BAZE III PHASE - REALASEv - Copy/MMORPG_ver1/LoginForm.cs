﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using MMORPG_ver1.Entiteti;

namespace MMORPG_ver1
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void PrijaviteSe_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
               
                Igrac igrac = (from i in s.Query<Entiteti.Igrac>()
                              where (i.Nadimak.Equals(textBoxNick.Text) && i.Lozinka.Equals(textBoxLozinka.Text))
                              select i).First();
                Lik lik = (from l in s.Query<Entiteti.Lik>()
                           where (l==igrac.IdLika)
                           select l).First();
                Igrac ig = (Igrac)s.GetSessionImplementation().PersistenceContext.Unproxy(igrac);
                Lik lk = (Lik)s.GetSessionImplementation().PersistenceContext.Unproxy(lik);
                s.Close();
                textBoxLozinka.BackColor = (Color.White);
                textBoxNick.BackColor = (Color.White);
                textBoxNick.Clear();
                textBoxLozinka.Clear();
                GameForm nova = new GameForm(ig, lk);
                nova.ShowDialog();
            }
            catch (Exception ec)
            {
                MessageBox.Show("Uneli ste pogrešni nadimak ili lozinku!");
                textBoxLozinka.BackColor=(Color.IndianRed);
                textBoxNick.BackColor = (Color.IndianRed);
            }

        }
    }
}
