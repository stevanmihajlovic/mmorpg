﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMORPG_ver1
{
    public partial class AdminForm : Form
    {
        public AdminForm()
        {
            InitializeComponent();
        }

        private void UbaciItemUBazu_Click(object sender, EventArgs e)
        {
            InsertItemForm nova = new InsertItemForm();
            nova.ShowDialog();
        }

        private void UbaciPrevoznoSredstvoUBazu_Click(object sender, EventArgs e)
        {
            InsertMountForm nova = new InsertMountForm();
            nova.ShowDialog();
        }

        private void buttonPregledSesija_Click(object sender, EventArgs e)
        {
            SesionPreview nova = new SesionPreview();
            nova.ShowDialog();
        }

        private void buttonUbaciZadatakUBazu_Click(object sender, EventArgs e)
        {
            InsertQuestForm nova = new InsertQuestForm();
            nova.ShowDialog();
        }

        private void buttonKreirajSavez_Click(object sender, EventArgs e)
        {
            CreateUnion nova = new CreateUnion();
            nova.ShowDialog();
        }

        private void buttonStaviAljansuNaZadatak_Click(object sender, EventArgs e)
        {
            AlianceChooseQuestForm nova = new AlianceChooseQuestForm();
            nova.ShowDialog();
        }

        private void buttonListaIgracaZaKljucniItem_Click(object sender, EventArgs e)
        {
            KeyItemPlayersForm nova = new KeyItemPlayersForm();
            nova.ShowDialog();
        }

        private void buttonIzbaciItemIzBaze_Click(object sender, EventArgs e)
        {
            DeleteItemForm nova = new DeleteItemForm();
            nova.ShowDialog();

        }

        private void buttonIzbaciPrevoznoSredstvoIzBaze_Click(object sender, EventArgs e)
        {
            DeleteMountForm nova = new DeleteMountForm();
            nova.ShowDialog();
        }

        private void buttonIzbaciZadatakIzBaze_Click(object sender, EventArgs e)
        {
            DeleteQuestForm nova = new DeleteQuestForm();
            nova.ShowDialog();
        }

        private void buttonBanujIgraca_Click(object sender, EventArgs e)
        {
            BanPlayerForm nova = new BanPlayerForm();
            nova.ShowDialog();
        }

        private void buttonBrisiAlijansu_Click(object sender, EventArgs e)
        {
            DeleteAlianceForm nova = new DeleteAlianceForm();
            nova.ShowDialog();
        }

        private void buttonIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
