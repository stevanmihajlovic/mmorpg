﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class DeleteAlianceForm : Form
    {
        public IList<Alijansa> alijanse;
        public DeleteAlianceForm()
        {
            InitializeComponent();

            try
            {
                ISession s = DataLayer.GetSession();

                alijanse = (from a in s.Query<Entiteti.Alijansa>()
                         select a).ToList<Alijansa>();

                foreach (Alijansa a in alijanse)
                    listBox1.Items.Add(a.Naziv);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Selektujte Alijansu");
            else
            {
                try
                {
                    ISession s = DataLayer.GetSession();

                    Alijansa selektovana = s.Load<Alijansa>(alijanse[listBox1.SelectedIndex].IdAlijansa);
                    //s.Lock(selektovan, LockMode.None);
                    //Item selektovan = itemi[listBox1.SelectedIndex];
                    //s.Delete(Convert.ToInt32(listBox1.Items[listBox1.SelectedIndex].ToString()));

                    //foreach (Igrac i in selektovana.Igraci)
                    //{
                    //    i.Itemi.Remove(selektovana);
                    //    s.SaveOrUpdate(i);
                    //}

                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                    foreach (Alijansa a in selektovana.USavezuSa)
                    {
                        //selektovana.USavezuSa.Remove(a);
                        a.USavezuSa.Remove(selektovana);
                    }
                    s.Delete(selektovana);

                    s.Flush();
                    s.Close();

                    MessageBox.Show("Uspesno ste izbacili Alijansu iz baze");
                    this.Close();
                }
                catch (Exception ec)
                {
                    // if (ec.Message.Contains("already") || ec.InnerException.Message.Contains("insert"))
                    //   MessageBox.Show("Igrac vec poseduje item");
                    // else
                    MessageBox.Show(ec.Message);
                }
            }
        }
    }
}
