﻿namespace MMORPG_ver1
{
    partial class InsertItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButtonPredmet = new System.Windows.Forms.RadioButton();
            this.radioButtonOruzje = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButtonKljucni = new System.Windows.Forms.RadioButton();
            this.radioButtonBonus = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBoxKljucni = new System.Windows.Forms.GroupBox();
            this.textBoxOpis = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxIme = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxBonus = new System.Windows.Forms.GroupBox();
            this.checkBoxOrk = new System.Windows.Forms.CheckBox();
            this.checkBoxDemon = new System.Windows.Forms.CheckBox();
            this.checkBoxCovek = new System.Windows.Forms.CheckBox();
            this.checkBoxPatuljak = new System.Windows.Forms.CheckBox();
            this.checkBoxVilenjak = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxIskustvo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonUbaci = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBoxKljucni.SuspendLayout();
            this.groupBoxBonus.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(92, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Podaci o itemu ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 63);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(102, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Izaberite tip itema:";
            // 
            // radioButtonPredmet
            // 
            this.radioButtonPredmet.AutoSize = true;
            this.radioButtonPredmet.Checked = true;
            this.radioButtonPredmet.Location = new System.Drawing.Point(143, 63);
            this.radioButtonPredmet.Name = "radioButtonPredmet";
            this.radioButtonPredmet.Size = new System.Drawing.Size(64, 17);
            this.radioButtonPredmet.TabIndex = 2;
            this.radioButtonPredmet.TabStop = true;
            this.radioButtonPredmet.Text = "Predmet";
            this.radioButtonPredmet.UseVisualStyleBackColor = true;
            // 
            // radioButtonOruzje
            // 
            this.radioButtonOruzje.AutoSize = true;
            this.radioButtonOruzje.Location = new System.Drawing.Point(207, 63);
            this.radioButtonOruzje.Name = "radioButtonOruzje";
            this.radioButtonOruzje.Size = new System.Drawing.Size(55, 17);
            this.radioButtonOruzje.TabIndex = 3;
            this.radioButtonOruzje.TabStop = true;
            this.radioButtonOruzje.Text = "Oružje";
            this.radioButtonOruzje.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 97);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(114, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Izaberite vrstu itema:";
            // 
            // radioButtonKljucni
            // 
            this.radioButtonKljucni.AutoSize = true;
            this.radioButtonKljucni.Checked = true;
            this.radioButtonKljucni.Location = new System.Drawing.Point(6, 17);
            this.radioButtonKljucni.Name = "radioButtonKljucni";
            this.radioButtonKljucni.Size = new System.Drawing.Size(56, 17);
            this.radioButtonKljucni.TabIndex = 5;
            this.radioButtonKljucni.TabStop = true;
            this.radioButtonKljucni.Text = "Ključni";
            this.radioButtonKljucni.UseVisualStyleBackColor = true;
            this.radioButtonKljucni.CheckedChanged += new System.EventHandler(this.radioButtonKljucni_CheckedChanged);
            // 
            // radioButtonBonus
            // 
            this.radioButtonBonus.AutoSize = true;
            this.radioButtonBonus.Location = new System.Drawing.Point(70, 17);
            this.radioButtonBonus.Name = "radioButtonBonus";
            this.radioButtonBonus.Size = new System.Drawing.Size(55, 17);
            this.radioButtonBonus.TabIndex = 6;
            this.radioButtonBonus.TabStop = true;
            this.radioButtonBonus.Text = "Bonus";
            this.radioButtonBonus.UseVisualStyleBackColor = true;
            this.radioButtonBonus.CheckedChanged += new System.EventHandler(this.radioButtonBonus_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonKljucni);
            this.groupBox1.Controls.Add(this.radioButtonBonus);
            this.groupBox1.Location = new System.Drawing.Point(137, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(135, 40);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // groupBoxKljucni
            // 
            this.groupBoxKljucni.Controls.Add(this.textBoxOpis);
            this.groupBoxKljucni.Controls.Add(this.label5);
            this.groupBoxKljucni.Controls.Add(this.textBoxIme);
            this.groupBoxKljucni.Controls.Add(this.label4);
            this.groupBoxKljucni.Location = new System.Drawing.Point(16, 132);
            this.groupBoxKljucni.Name = "groupBoxKljucni";
            this.groupBoxKljucni.Size = new System.Drawing.Size(256, 117);
            this.groupBoxKljucni.TabIndex = 8;
            this.groupBoxKljucni.TabStop = false;
            this.groupBoxKljucni.Text = "Dodatne informacije";
            // 
            // textBoxOpis
            // 
            this.textBoxOpis.Location = new System.Drawing.Point(9, 91);
            this.textBoxOpis.Name = "textBoxOpis";
            this.textBoxOpis.Size = new System.Drawing.Size(231, 20);
            this.textBoxOpis.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Opis:";
            // 
            // textBoxIme
            // 
            this.textBoxIme.Location = new System.Drawing.Point(9, 45);
            this.textBoxIme.Name = "textBoxIme";
            this.textBoxIme.Size = new System.Drawing.Size(231, 20);
            this.textBoxIme.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Ime:";
            // 
            // groupBoxBonus
            // 
            this.groupBoxBonus.Controls.Add(this.checkBoxOrk);
            this.groupBoxBonus.Controls.Add(this.checkBoxDemon);
            this.groupBoxBonus.Controls.Add(this.checkBoxCovek);
            this.groupBoxBonus.Controls.Add(this.checkBoxPatuljak);
            this.groupBoxBonus.Controls.Add(this.checkBoxVilenjak);
            this.groupBoxBonus.Controls.Add(this.label6);
            this.groupBoxBonus.Controls.Add(this.textBoxIskustvo);
            this.groupBoxBonus.Controls.Add(this.label7);
            this.groupBoxBonus.Location = new System.Drawing.Point(16, 132);
            this.groupBoxBonus.Name = "groupBoxBonus";
            this.groupBoxBonus.Size = new System.Drawing.Size(256, 117);
            this.groupBoxBonus.TabIndex = 9;
            this.groupBoxBonus.TabStop = false;
            this.groupBoxBonus.Text = "Dodatne informacije";
            this.groupBoxBonus.Visible = false;
            // 
            // checkBoxOrk
            // 
            this.checkBoxOrk.AutoSize = true;
            this.checkBoxOrk.Location = new System.Drawing.Point(165, 67);
            this.checkBoxOrk.Name = "checkBoxOrk";
            this.checkBoxOrk.Size = new System.Drawing.Size(43, 17);
            this.checkBoxOrk.TabIndex = 7;
            this.checkBoxOrk.Text = "Ork";
            this.checkBoxOrk.UseVisualStyleBackColor = true;
            // 
            // checkBoxDemon
            // 
            this.checkBoxDemon.AutoSize = true;
            this.checkBoxDemon.Location = new System.Drawing.Point(92, 89);
            this.checkBoxDemon.Name = "checkBoxDemon";
            this.checkBoxDemon.Size = new System.Drawing.Size(60, 17);
            this.checkBoxDemon.TabIndex = 6;
            this.checkBoxDemon.Text = "Demon";
            this.checkBoxDemon.UseVisualStyleBackColor = true;
            // 
            // checkBoxCovek
            // 
            this.checkBoxCovek.AutoSize = true;
            this.checkBoxCovek.Location = new System.Drawing.Point(92, 67);
            this.checkBoxCovek.Name = "checkBoxCovek";
            this.checkBoxCovek.Size = new System.Drawing.Size(57, 17);
            this.checkBoxCovek.TabIndex = 5;
            this.checkBoxCovek.Text = "Čovek";
            this.checkBoxCovek.UseVisualStyleBackColor = true;
            // 
            // checkBoxPatuljak
            // 
            this.checkBoxPatuljak.AutoSize = true;
            this.checkBoxPatuljak.Location = new System.Drawing.Point(9, 90);
            this.checkBoxPatuljak.Name = "checkBoxPatuljak";
            this.checkBoxPatuljak.Size = new System.Drawing.Size(64, 17);
            this.checkBoxPatuljak.TabIndex = 4;
            this.checkBoxPatuljak.Text = "Patuljak";
            this.checkBoxPatuljak.UseVisualStyleBackColor = true;
            // 
            // checkBoxVilenjak
            // 
            this.checkBoxVilenjak.AutoSize = true;
            this.checkBoxVilenjak.Location = new System.Drawing.Point(9, 67);
            this.checkBoxVilenjak.Name = "checkBoxVilenjak";
            this.checkBoxVilenjak.Size = new System.Drawing.Size(63, 17);
            this.checkBoxVilenjak.TabIndex = 3;
            this.checkBoxVilenjak.Text = "Vilenjak";
            this.checkBoxVilenjak.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Rase koje mogu nositi item:";
            // 
            // textBoxIskustvo
            // 
            this.textBoxIskustvo.Location = new System.Drawing.Point(62, 19);
            this.textBoxIskustvo.Name = "textBoxIskustvo";
            this.textBoxIskustvo.Size = new System.Drawing.Size(51, 20);
            this.textBoxIskustvo.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Iskustvo:";
            // 
            // buttonUbaci
            // 
            this.buttonUbaci.Location = new System.Drawing.Point(197, 266);
            this.buttonUbaci.Name = "buttonUbaci";
            this.buttonUbaci.Size = new System.Drawing.Size(75, 23);
            this.buttonUbaci.TabIndex = 10;
            this.buttonUbaci.Text = "Ubaci Item";
            this.buttonUbaci.UseVisualStyleBackColor = true;
            this.buttonUbaci.Click += new System.EventHandler(this.buttonUbaci_Click);
            // 
            // InsertItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 301);
            this.Controls.Add(this.buttonUbaci);
            this.Controls.Add(this.groupBoxKljucni);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.radioButtonOruzje);
            this.Controls.Add(this.radioButtonPredmet);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBoxBonus);
            this.Name = "InsertItemForm";
            this.Text = "InsertItemForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxKljucni.ResumeLayout(false);
            this.groupBoxKljucni.PerformLayout();
            this.groupBoxBonus.ResumeLayout(false);
            this.groupBoxBonus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioButtonPredmet;
        private System.Windows.Forms.RadioButton radioButtonOruzje;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButtonKljucni;
        private System.Windows.Forms.RadioButton radioButtonBonus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxKljucni;
        private System.Windows.Forms.TextBox textBoxOpis;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxIme;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBoxBonus;
        private System.Windows.Forms.CheckBox checkBoxOrk;
        private System.Windows.Forms.CheckBox checkBoxDemon;
        private System.Windows.Forms.CheckBox checkBoxCovek;
        private System.Windows.Forms.CheckBox checkBoxPatuljak;
        private System.Windows.Forms.CheckBox checkBoxVilenjak;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxIskustvo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonUbaci;
    }
}