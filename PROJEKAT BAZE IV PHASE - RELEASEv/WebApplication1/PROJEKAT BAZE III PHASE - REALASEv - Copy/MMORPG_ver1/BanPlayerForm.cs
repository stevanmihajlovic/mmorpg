﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using MMORPG_ver1.Entiteti;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MMORPG_ver1
{
    public partial class BanPlayerForm : Form
    {
        public IList<Igrac> igraci;
        public BanPlayerForm()
        {
            InitializeComponent();
            try
            {
                ISession s = DataLayer.GetSession();

                igraci = (from a in s.Query<Entiteti.Igrac>()
                         select a).ToList<Igrac>();

                foreach (Igrac a in igraci)
                    listBox1.Items.Add(a.Nadimak);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void buttonBanuj_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Izaberite igraca");
            else
            {
                try
                {
                    ISession s = DataLayer.GetSession();
                    Igrac i = s.Load<Igrac>(igraci[listBox1.SelectedIndex].IdIgrac);
                    s.Delete(i);
                    s.Flush();
                    s.Close();
                    MessageBox.Show("Uspesno ste izbrisali igraca");
                    this.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.Message);
                }
            }
        }
    }
}
