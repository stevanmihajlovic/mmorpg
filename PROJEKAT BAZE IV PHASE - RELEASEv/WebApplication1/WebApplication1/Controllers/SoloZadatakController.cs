﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class SoloZadatakController : ApiController
    {
        // GET: api/SoloZadatak
        public IEnumerable<SoloZadatak> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<SoloZadatak> zadaci = p.GetSoloZadaci();
            foreach (SoloZadatak z in zadaci)
                z.Solo = null;
            return zadaci;
        }

        // GET: api/SoloZadatak/5
        public SoloZadatak Get(int id)
        {
            DataProvider p = new DataProvider();
            SoloZadatak z = p.GetSoloZadatak(id);
            if (z != null)
                z.Solo = null;
            return z;
        }

        // POST: api/SoloZadatak
        public int Post([FromBody]SoloZadatak value)
        {
            DataProvider p = new DataProvider();
            return p.AddSoloZadatak(value);
        }

        // PUT: api/SoloZadatak/5
        public int Put(int id, [FromBody]SoloZadatak value)
        {
            DataProvider p = new DataProvider();
            return p.UpdateSoloZadatak(id, value);
        }

        // DELETE: api/SoloZadatak/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeleteSoloZadatak(id);
        }
    }
}
