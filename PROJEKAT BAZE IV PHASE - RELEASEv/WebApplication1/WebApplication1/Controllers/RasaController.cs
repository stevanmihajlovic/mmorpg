﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;
namespace WebApplication1.Controllers
{
    public class RasaController : ApiController
    {
        // GET: api/Rasa
        public IEnumerable<Rasa> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<Rasa> rase = p.GetRase();
            foreach (Rasa i in rase)
                i.IdItema = null;   //Bez ovog kad trazimo sve rase, lista itema je cudna
            return rase;
        }

        public Rasa Get(int id)
        {
            DataProvider p = new DataProvider();
            Rasa rasa = p.GetRasa(id);
            if (rasa!=null)
                rasa.IdItema = null;
            return rasa;
        }

        // POST: api/Rasa
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Rasa/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Rasa/5
        public void Delete(int id)
        {
        }
    }
}
