﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;
namespace WebApplication1.Controllers
{
    public class PrevoznoSredstvoController : ApiController
    {
        // GET: api/PrevoznaSredstva
        public IEnumerable<PrevoznoSredstvo> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<PrevoznoSredstvo> ps = p.GetPrevoznaSredstva();
            foreach (PrevoznoSredstvo p1 in ps)
                p1.PripadaIgracu = null;    //nece da prikaze igraca...
            return ps;
        }

        // GET: api/PrevoznaSredstva/5
        public PrevoznoSredstvo Get(int id)
        {
            DataProvider p = new DataProvider();
            PrevoznoSredstvo p1 = p.GetPrevoznoSredstvo(id);
            if (p1 != null)
            {
                p1.PripadaIgracu = null;
            }
            return p1;
        }

        // POST: api/PrevoznaSredstva
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/PrevoznaSredstva/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/PrevoznaSredstva/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeletePrevoznoSredstvo(id);
        }
    }
}
