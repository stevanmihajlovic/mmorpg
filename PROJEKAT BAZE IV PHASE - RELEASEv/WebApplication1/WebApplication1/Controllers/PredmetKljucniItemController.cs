﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class PredmetKljucniItemController : ApiController
    {
        // GET: api/PredmetKljucniItem
        public IEnumerable<PredmetKljucniItem> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<PredmetKljucniItem> itemi = p.GetPredmetKljucniItemi();
            foreach (PredmetKljucniItem i in itemi)
                i.Igraci = null;    //Bez ovoga dobijemo sve podatke o igracu, a to nam ne treba
            return itemi;
        }

        // GET: api/PredmetKljucniItem/5
        public PredmetKljucniItem Get(int id)
        {
            DataProvider p = new DataProvider();
            PredmetKljucniItem item = p.GetPredmetKljucniItem(id);
            if (item != null)
            {
                item.Igraci = null;
            }
            return item;
        }

        // POST: api/PredmetKljucniItem
        public int Post([FromBody]PredmetKljucniItem value)
        {
            DataProvider p = new DataProvider();
            return p.AddPredmetKljucniItem(value);
        }

        // PUT: api/PredmetKljucniItem/5
        public int Put(int id, [FromBody]PredmetKljucniItem value)
        {
            DataProvider p = new DataProvider();
            return p.UpdatePredmetKljucniItem(id, value);
        }

        // DELETE: api/PredmetKljucniItem/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeletePredmetKljucniItem(id);
        }
    }
}
