﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class PrevoznoSredstvoMehanickiController : ApiController
    {
        // GET: api/PrevoznoSredstvoMehanicki
        public IEnumerable<PrevoznoSredstvoMehanicki> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<PrevoznoSredstvoMehanicki> ps = p.GetPrevoznaSredstvaMehanicki();
            foreach (PrevoznoSredstvoMehanicki p1 in ps)
                p1.PripadaIgracu = null;    //nece da prikaze igraca...
            return ps;
        }

        // GET: api/PrevoznoSredstvoMehanicki/5
        public PrevoznoSredstvoMehanicki Get(int id)
        {
            DataProvider p = new DataProvider();
            PrevoznoSredstvoMehanicki p1 = p.GetPrevoznoSredstvoMehanicki(id);
            if (p1 != null)
            {
                p1.PripadaIgracu = null;
            }
            return p1;
        }

        // POST: api/PrevoznoSredstvoMehanicki
        public int Post([FromBody]PrevoznoSredstvoMehanicki value)
        {
            DataProvider p = new DataProvider();
            return p.AddPrevoznoSredstvoMehanicki(value);
        }

        // PUT: api/PrevoznoSredstvoMehanicki/5
        public int Put(int id, [FromBody]PrevoznoSredstvoMehanicki value)
        {
            DataProvider p = new DataProvider();
            return p.UpdatePrevoznoSredstvoMehanicki(id, value);
        }

        // DELETE: api/PrevoznoSredstvoMehanicki/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeletePrevoznoSredstvoMehanicki(id);
        }
    }
}
