﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class LikIgracController : ApiController
    {
        // GET: api/LikIgrac
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/LikIgrac/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/LikIgrac
        public int Post([FromBody]LikIgrac value)
        {
            DataProvider p = new DataProvider();
            return p.AddLikIgrac(value);
        }

        // PUT: api/LikIgrac/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/LikIgrac/5
        public void Delete(int id)
        {
        }
    }
}
