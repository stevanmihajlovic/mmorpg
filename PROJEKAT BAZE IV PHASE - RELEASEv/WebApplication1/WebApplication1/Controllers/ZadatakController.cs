﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;
namespace WebApplication1.Controllers
{
    public class ZadatakController : ApiController
    {
        // GET: api/Zadatak
        public IEnumerable<Zadatak> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<Zadatak> zadaci = p.GetZadaci();
            foreach (Zadatak z in zadaci)
                if (z.GetType() == typeof(SoloZadatak))
                    ((SoloZadatak)z).Solo = null;
                else
                    ((GrupniZadatak)z).Grupni = null;
            return zadaci;
        }

        // GET: api/Zadatak/5
        public Zadatak Get(int id)
        {
            DataProvider p = new DataProvider();
            Zadatak z = p.GetZadatak(id);
            if (z.GetType() == typeof(SoloZadatak))
                ((SoloZadatak)z).Solo = null;
            else
                ((GrupniZadatak)z).Grupni = null;
            return z;
        }

        // POST: api/Zadatak
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Zadatak/5
        public int Put(int id, [FromBody]Zadatak value)
        {
            DataProvider p = new DataProvider();
            return p.UpdateZadatak(id, value);
        }

        // DELETE: api/Zadatak/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeleteZadatak(id);
        }
    }
}
