﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class GrupniZadatakController : ApiController
    {
        // GET: api/GrupniZadatak
        public IEnumerable<GrupniZadatak> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<GrupniZadatak> zadaci = p.GetGrupniZadaci();
            foreach (GrupniZadatak z in zadaci)
                    z.Grupni = null;
            return zadaci;
        }

        // GET: api/GrupniZadatak/5
        public GrupniZadatak Get(int id)
        {
            DataProvider p = new DataProvider();
            GrupniZadatak z = p.GetGrupniZadatak(id);
            if (z!=null)
                z.Grupni = null;
            return z;
        }

        // POST: api/GrupniZadatak
        public int Post([FromBody]GrupniZadatak value)
        {
            DataProvider p = new DataProvider();
            return p.AddGrupniZadatak(value);
        }

        // PUT: api/GrupniZadatak/5
        public int Put(int id, [FromBody]GrupniZadatak value)
        {
            DataProvider p = new DataProvider();
            return p.UpdateGrupniZadatak(id, value);
        }

        // DELETE: api/GrupniZadatak/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeleteGrupniZadatak(id);
        }
    }
}
