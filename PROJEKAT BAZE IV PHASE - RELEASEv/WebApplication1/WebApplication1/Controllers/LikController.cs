﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class LikController : ApiController
    {
        // GET: api/Lik
        public IEnumerable<Lik> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<Lik> likovi = p.GetLikovi();
            foreach (Lik l in likovi)
            {
                l.IdIgraca = null;
                l.GospodarSegrta = null;  //stavili smo ovo jer kad neki ima vise gospodara opet se ispisuju svi njegovi segrti bespotrebno
                l.Segrti = null; //u JSON moze ovo da se iskomentarise zbog onog mog koda
            }
            return likovi;
        }

        // GET: api/Lik/5
        public Lik Get(int id)
        {
            DataProvider p = new DataProvider();
            Lik lik = p.GetLik(id);
            if (lik != null)
            {
                lik.GospodarSegrta = null;
                lik.IdIgraca = null;
                lik.Segrti = null;
            }
            return lik; 
        }

        // POST: api/Lik
        public int Post([FromBody]Lik value)
        {
            DataProvider p = new DataProvider();
            return p.AddLik(value);
        }

        // PUT: api/Lik/5
        public int Put(int id, [FromBody]Lik value)
        {
            DataProvider p = new DataProvider();
            return p.UpdateLik(id, value);
        }

        // DELETE: api/Lik/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeleteLik(id);
        }
    }
}
