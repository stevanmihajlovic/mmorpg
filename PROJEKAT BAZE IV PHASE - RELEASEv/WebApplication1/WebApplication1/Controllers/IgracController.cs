﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class IgracController : ApiController
    {
        
        // GET: api/Igrac
        public IList<Igrac> Get()
        {
            DataProvider p=new DataProvider();
            IList<Igrac> test = p.GetIgraci();

            foreach (Igrac i in test)
            {
                i.IdLika = null;
                i.PripadaAlijansi = null;
                i.Sesije = null;    //u JSON moze ovo da se iskomentarise zbog onog mog koda
                i.Zadaci = null;
                i.Itemi = null;
                i.PrevoznaSredstva = null; //u JSON moze ovo da se iskomentarise zbog onog mog koda
            }

            return test;
        }

        // GET: api/Igrac/5
        public Igrac Get(int id)
        {
            DataProvider p = new DataProvider();
            Igrac i = p.GetIgrac(id);
            if (i != null)
            {
                i.IdLika = null;
                i.PripadaAlijansi = null;
                i.Sesije = null;
                i.Zadaci = null;
                i.Itemi = null;
                i.PrevoznaSredstva = null;
            }
            
            return i;
        }

        // POST: api/Igrac
        public void Post([FromBody]Igrac i)  //mora sa likom da se sinhronizuje
        {
            
        }

        // PUT: api/Igrac/5
        public int Put(int id, [FromBody]Igrac value)
        {
            DataProvider p = new DataProvider();
            return p.UpdateIgrac(id, value);
        }

        // DELETE: api/Igrac/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeleteIgrac(id);
        }
    }
}
