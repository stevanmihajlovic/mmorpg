﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class PrevoznoSredstvoZmajController : ApiController
    {
        // GET: api/PrevoznoSredstvoZmaj
        public IEnumerable<PrevoznoSredstvoZmaj> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<PrevoznoSredstvoZmaj> ps = p.GetPrevoznaSredstvaZmaj();
            foreach (PrevoznoSredstvoZmaj p1 in ps)
                p1.PripadaIgracu = null;    //nece da prikaze igraca...
            return ps;
        }

        // GET: api/PrevoznoSredstvoZmaj/5
        public PrevoznoSredstvoZmaj Get(int id)
        {
            DataProvider p = new DataProvider();
            PrevoznoSredstvoZmaj p1 = p.GetPrevoznoSredstvoZmaj(id);
            if (p1 != null)
            {
                p1.PripadaIgracu = null;
            }
            return p1;
        }

        // POST: api/PrevoznoSredstvoZmaj
        public int Post([FromBody]PrevoznoSredstvoZmaj value)
        {
            DataProvider p = new DataProvider();
            return p.AddPrevoznoSredstvoZmaj(value);
        }

        // PUT: api/PrevoznoSredstvoZmaj/5
        public int Put(int id, [FromBody]PrevoznoSredstvoZmaj value)
        {
            DataProvider p = new DataProvider();
            return p.UpdatePrevoznoSredstvoZmaj(id,value);
        }

        // DELETE: api/PrevoznoSredstvoZmaj/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeletePrevoznoSredstvoZmaj(id);
        }
    }
}
