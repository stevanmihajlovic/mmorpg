﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class SesijaController : ApiController
    {
        // GET: api/Sesija
        public IEnumerable<Sesija> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<Sesija> sesije = p.GetSesije();
            foreach (Sesija i in sesije)
                i.VezanaZaIgraca = null;
            return sesije;
        }

        public Sesija Get(int id)
        {
            DataProvider p = new DataProvider();
            Sesija sesija = p.GetSesija(id);
            if (sesija!=null)
                sesija.VezanaZaIgraca = null;
            return sesija;
        }

        // POST: api/Sesija
        public int Post([FromBody]Sesija value)
        {
            DataProvider p = new DataProvider();
            return p.AddSesija(value);
        }

        // PUT: api/Sesija/5
        public int Put(int id, [FromBody]Sesija value)
        {
            DataProvider p = new DataProvider();
            return p.UpdateSesija(id, value);
        }

        // DELETE: api/Sesija/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeleteSesija(id);
        }
    }
}
