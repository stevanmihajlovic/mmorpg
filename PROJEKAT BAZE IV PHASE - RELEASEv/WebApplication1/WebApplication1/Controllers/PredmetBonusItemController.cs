﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class PredmetBonusItemController : ApiController
    {
        // GET: api/PredmetBonusItem
        public IEnumerable<PredmetBonusItem> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<PredmetBonusItem> itemi = p.GetPredmetBonusItemi();
            foreach (PredmetBonusItem i in itemi)
                i.Igraci = null;    //Bez ovoga dobijemo sve podatke o igracu, a to nam ne treba
            return itemi;
        }

        // GET: api/PredmetBonusItem/5
        public PredmetBonusItem Get(int id)
        {
            DataProvider p = new DataProvider();
            PredmetBonusItem item = p.GetPredmetBonusItem(id);
            if (item != null)
            {
                item.Igraci = null;
            }
            return item;
        }

        // POST: api/PredmetBonusItem
        public int Post([FromBody]PredmetBonusItem value)
        {
            DataProvider p = new DataProvider();
            return p.AddPredmetBonusItem(value);
        }

        // PUT: api/PredmetBonusItem/5
        public int Put(int id, [FromBody]PredmetBonusItem value)
        {
            DataProvider p = new DataProvider();
            return p.UpdatePredmetBonusItem(id, value);
        }

        // DELETE: api/PredmetBonusItem/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeletePredmetBonusItem(id);
        }
    }
}
