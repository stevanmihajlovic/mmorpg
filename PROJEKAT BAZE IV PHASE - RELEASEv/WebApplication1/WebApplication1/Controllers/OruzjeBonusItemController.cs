﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class OruzjeBonusItemController : ApiController
    {
        // GET: api/OruzjeBonusItem
        public IEnumerable<OruzjeBonusItem> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<OruzjeBonusItem> itemi = p.GetOruzjeBonusItemi();
            foreach (OruzjeBonusItem i in itemi)
                i.Igraci = null;    //Bez ovoga dobijemo sve podatke o igracu, a to nam ne treba
            return itemi;
        }

        // GET: api/OruzjeBonusItem/5
        public OruzjeBonusItem Get(int id)
        {
            DataProvider p = new DataProvider();
            OruzjeBonusItem item = p.GetOruzjeBonusItem(id);
            if (item != null)
            {
                item.Igraci = null;
            }
            return item;
        }

        // POST: api/OruzjeBonusItem
        public int Post([FromBody]OruzjeBonusItem value)
        {
            DataProvider p = new DataProvider();
            return p.AddOruzjeBonusItem(value);
        }

        // PUT: api/OruzjeBonusItem/5
        public int Put(int id, [FromBody]OruzjeBonusItem value)
        {
            DataProvider p = new DataProvider();
            return p.UpdateOruzjeBonusItem(id, value);
        }

        // DELETE: api/OruzjeBonusItem/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeleteOruzjeBonusItem(id);
        }
    }
}
