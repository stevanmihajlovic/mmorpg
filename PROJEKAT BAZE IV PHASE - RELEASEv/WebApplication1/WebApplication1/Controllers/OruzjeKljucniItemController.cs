﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class OruzjeKljucniItemController : ApiController
    {
        // GET: api/OruzjeKljucniItem
        public IEnumerable<OruzjeKljucniItem> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<OruzjeKljucniItem> itemi = p.GetOruzjeKljucniItemi();
            foreach (OruzjeKljucniItem i in itemi)
                i.Igraci = null;    //Bez ovoga dobijemo sve podatke o igracu, a to nam ne treba
            return itemi;
        }

        // GET: api/OruzjeKljucniItem/5
        public OruzjeKljucniItem Get(int id)
        {
            DataProvider p = new DataProvider();
            OruzjeKljucniItem item = p.GetOruzjeKljucniItem(id);
            if (item != null)
            {
                item.Igraci = null;
            }
            return item;
        }

        // POST: api/OruzjeKljucniItem
        public int Post([FromBody]OruzjeKljucniItem value)
        {
            DataProvider p = new DataProvider();
            return p.AddOruzjeKljucniItem(value);
        }

        // PUT: api/OruzjeKljucniItem/5
        public int Put(int id, [FromBody]OruzjeKljucniItem value)
        {
            DataProvider p = new DataProvider();
            return p.UpdateOruzjeKljucniItem(id, value);
        }

        // DELETE: api/OruzjeKljucniItem/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeleteOruzjeKljucniItem(id);
        }
    }
}
