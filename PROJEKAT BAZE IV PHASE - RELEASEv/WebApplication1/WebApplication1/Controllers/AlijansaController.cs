﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class AlijansaController : ApiController
    {
        // GET: api/Alijansa
        public IEnumerable<Alijansa> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<Alijansa> alijanse = p.GetAlijanse();
            foreach (Alijansa a in alijanse)
            {
                a.Igraci = null;
                a.Zadaci = null;    //isto ce prenatrpano da bude...
                a.USavezuSa = null; //ako ima vise saveza opet ce da bude kao za gospodara...
            }
            return alijanse;
        }

        // GET: api/Alijansa/5
        public Alijansa Get(int id)
        {
            DataProvider p = new DataProvider();
            Alijansa a = p.GetAlijansa(id);
            if (a != null)
            {
                a.USavezuSa = null;
                a.Igraci = null;
                a.Zadaci = null;
            }
            return a;
        }

        // POST: api/Alijansa
        public int Post([FromBody]Alijansa value)
        {
            DataProvider p = new DataProvider();
            return p.AddAlijansa(value);
        }

        // PUT: api/Alijansa/5
        public int Put(int id, [FromBody]Alijansa value)
        {
            DataProvider p = new DataProvider();
            return p.UpdateAlijansa(id, value);
        }

        // DELETE: api/Alijansa/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeleteAlijansa(id);
        }
    }
}
