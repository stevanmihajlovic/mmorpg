﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class ItemController : ApiController
    {
        // GET: api/Item
        public IEnumerable<Item> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<Item> itemi = p.GetItemi();
            foreach (Item i in itemi)
                i.Igraci = null;    //Bez ovoga dobijemo sve podatke o igracu, a to nam ne treba
            return itemi;
        }

        // GET: api/Item/5
        public Item Get(int id)
        {
            DataProvider p = new DataProvider();
            Item item = p.GetItem(id);
            if (item != null)
            {
                item.Igraci = null;
            }
            return item;
        }

        // POST: api/Item
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Item/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Item/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeleteItem(id);
        }
    }
}
