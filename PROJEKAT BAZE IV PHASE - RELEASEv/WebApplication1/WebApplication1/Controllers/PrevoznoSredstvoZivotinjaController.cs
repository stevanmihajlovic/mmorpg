﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using MMORPG_ver1.Entiteti;
using MMORPG_ver1;

namespace WebApplication1.Controllers
{
    public class PrevoznoSredstvoZivotinjaController : ApiController
    {
        // GET: api/PrevoznoSredstvoZivotinja
        public IEnumerable<PrevoznoSredstvoZivotinja> Get()
        {
            DataProvider p = new DataProvider();
            IEnumerable<PrevoznoSredstvoZivotinja> ps = p.GetPrevoznaSredstvaZivotinja();
            foreach (PrevoznoSredstvoZivotinja p1 in ps)
                p1.PripadaIgracu = null;    //nece da prikaze igraca...
            return ps;
        }

        // GET: api/PrevoznoSredstvoZivotinja/5
        public PrevoznoSredstvoZivotinja Get(int id)
        {
            DataProvider p = new DataProvider();
            PrevoznoSredstvoZivotinja p1 = p.GetPrevoznoSredstvoZivotinja(id);
            if (p1 != null)
            {
                p1.PripadaIgracu = null;
            }
            return p1;
        }

        // POST: api/PrevoznoSredstvoZivotinja
        public int Post([FromBody]PrevoznoSredstvoZivotinja value)
        {
            DataProvider p = new DataProvider();
            return p.AddPrevoznoSredstvoZivotinja(value);
        }

        // PUT: api/PrevoznoSredstvoZivotinja/5
        public int Put(int id, [FromBody]PrevoznoSredstvoZivotinja value)
        {
            DataProvider p = new DataProvider();
            return p.UpdatePrevoznoSredstvoZivotinja(id, value);
        }

        // DELETE: api/PrevoznoSredstvoZivotinja/5
        public int Delete(int id)
        {
            DataProvider p = new DataProvider();
            return p.DeletePrevoznoSredstvoZivotinja(id);
        }
    }
}
